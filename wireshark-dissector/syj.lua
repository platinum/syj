do
	local opcode = {
		[0x01] = "RRQ",
		[0x02] = "WRQ",
		[0x03] = "DATA",
		[0x04] = "ACK",
		[0x05] = "ERROR"
	}

	local cmd = {
		[0x01] = "信息验证",
		[0x02] = "全局参数",
		[0x03] = "小票信息",
		[0x04] = "商家信息",
		[0x05] = "销售价格定制信息",
		[0x06] = "仓库信息",
		[0x07] = "员工信息",
		[0x08] = "客户信息",
		[0x09] = "供应商信息",
		[0x0a] = "商品基本信息",
		[0x0b] = "商品单位信息",
		[0x0c] = "商品批次信息",
		[0x0d] = "待办任务信息取得",
		[0x0e] = "系统消息",
		[0x0f] = "商品类别信息",
		[0x10] = "数据字典",
		[0x11] = "商品类别/证件编号名称对照",
		[0x12] = "差异化同步请求",
		[0x13] = "待办任务信息处理",
		[0x14] = "索取商家证请求",
		[0x15] = "索取产品证请求",
		[0x16] = "进货单生成",
		[0x17] = "销售单生成",
		[0x18] = "退市单生成",
		[0x19] = "退市处理单生成",
		[0x1a] = "商品基本信息管理",
		[0x1b] = "商品单位信息管理",
		[0x1c] = "商品档案信息获取",
		[0x1d] = "取消销售单商品信息请求",
		[0x1e] = "获取升级包信息请求",
		[0x1f] = "软件升级请求",
		[0x20] = "员工管理请求",
		[0x21] = "供应商管理请求",
		[0x22] = "客户管理请求"
	}

	local result = {
		[0x00] = "不通过",
		[0x01] = "通过"
	}

	local yesno = {
		[0x00] = "未知",
		[0x01] = "是",
		[0x02] = "否"
	}

	local order = {
		[0x00] = "未知",
		[0x01] = "升序",
		[0x02] = "降序"
	}

	local p_syj			= Proto("SYJ", "SYJ Debug Messages")
	local f_rexen		= ProtoField.string("syj.rexen",		"标示")
	local f_simid		= ProtoField.string("syj.simid",		"SIM 卡号")
	local f_opcode		= ProtoField.uint8("syj.opcode",		"报文类型", base.HEX)
	local f_blocknum	= ProtoField.uint8("syj.blocknum",		"数据块编号", base.HEX_DEC)
	local f_cmd			= ProtoField.uint8("syj.cmd",			"信令", base.HEX)
	local f_hardware	= ProtoField.string("syj.hardware",		"硬件串号")
	local f_ret			= ProtoField.string("syj.ret",			"返回码")
	local f_dept_id		= ProtoField.uint64("syj.dept_id",		"商家 ID")
	local f_num			= ProtoField.uint16("syj.num",			"收银机编号")
	local f_last_update	= ProtoField.string("syj.last_update",	"最后更新时间")
	local f_use_shelf	= ProtoField.uint8("syj.use_shelf",		"是否启用在售货架", base.HEX)
	local f_direct_out	= ProtoField.uint8("syj.direct_out",	"是否直接出库", base.HEX)
	local f_direct_in	= ProtoField.uint8("syj.direct_in",		"是否直接入库", base.HEX)
	local f_batch		= ProtoField.uint8("syj.batch",			"是否选择批次", base.HEX)
	local f_batch_order	= ProtoField.uint8("syj.batch_order",	"批次号排序规则", base.HEX)
	local f_auto		= ProtoField.uint8("syj.auto",			"是否自动生成进货单", base.HEX)

	p_syj.fields = { 
		f_rexen, f_simid, f_opcode, f_blocknum, f_cmd, f_hardware, f_ret, f_dept_id,
		f_num, f_last_update, f_use_shelf, f_direct_in, f_direct_out, f_batch,
		f_batch_order, f_auto
	}
	
	local data_dis = Dissector.get("data")
	
	local function syj_dissector(buf, pinfo, tree)
		local buf_len = buf:len();

		if buf_len < 19 then
			return false
		end
	
		local v_rexen 		= buf(0,   2)
		local v_simid		= buf(2,  15)
		local v_opcode		= buf(17,  1)
		local v_blocknum	= buf(18,  1)

		if v_rexen:string() ~= "RX" then
			return false
		end

		-- column
		pinfo.cols.protocol = "SYJ"
		local info = string.format("Len: %d, SIMID: %s, %s, BLOCKNUM: %s",
								   buf_len, v_simid:string(),
								   opcode[v_opcode:uint()], v_blocknum:uint())
		pinfo.cols.info = info

		local t = tree:add(p_syj, buf(0))
		t:add(f_simid, v_simid)
		t:add(f_opcode, v_opcode, v_opcode:uint(), nil, "(" .. opcode[v_opcode:uint()] .. ")")
		t:add(f_blocknum, v_blocknum)

		if buf_len > 19 then
			local v_cmd = buf(19, 1)
			t:add(f_cmd, v_cmd, v_cmd:uint(), nil, "(" .. cmd[v_cmd:uint()] .. ")")
			if v_cmd:uint() >= 0x01 and v_cmd:uint() <= 22 then
				if v_cmd:uint() == 0x01 then
					if buf_len == 55 then
						local v_simid = buf(20, 15)
						local v_hardware = buf(35, 20)
						t:add(f_simid, v_simid)
						t:add(f_hardware, v_hardware)
					elseif buf_len == 31 then
						local v_ret = buf(20, 1)
						local v_dept_id = buf(21, 8)
						local v_num = buf(29, 2)
						t:add(f_ret, v_ret, v_ret:uint(), nil, "(" .. result[v_ret:uint()] .. ")")
						t:add(f_dept_id, v_dept_id, v_dept_id:uint64(), nil, "(" .. tostring("0x" .. v_dept_id) .. ")")
						t:add(f_num, v_num)
					end
				elseif v_cmd:uint() == 0x02 then
					if buf_len == 32 then
						local v_dept_id = buf(20, 8)
						local v_last_update = buf(28, 4)
						t:add(f_dept_id, v_dept_id, v_dept_id:uint64(), nil, "(" .. tostring("0x" .. v_dept_id) .. ")")
						t:add(f_last_update, v_last_update, os.date("%Y-%m-%d %H:%M:%S", v_last_update:uint()), nil, "(" .. tostring("0x" .. v_last_update) .. ")")
					elseif buf_len == 30 then
						local v_use_shelf = buf(20, 1)
						local v_direct_out = buf(21, 1)
						local v_direct_in = buf(22, 1)
						local v_batch = buf(23, 1)
						local v_batch_order = buf(24, 1)
						local v_auto = buf(25, 1)
						local v_last_update = buf(26, 4)
						t:add(f_use_shelf, v_use_shelf, v_use_shelf:uint(), nil, "(" .. yesno[v_use_shelf:uint()] .. ")")
						t:add(f_direct_in, v_direct_in, v_direct_in:uint(), nil, "(" .. yesno[v_direct_in:uint()] .. ")")
						t:add(f_direct_out, v_direct_out, v_direct_out:uint(), nil, "(" .. yesno[v_direct_out:uint()] .. ")")
						t:add(f_batch, v_batch, v_batch:uint(), nil, "(" .. yesno[v_batch:uint()] .. ")")
						t:add(f_batch_order, v_batch_order, v_batch_order:uint(), nil, "(" .. yesno[v_batch_order:uint()] .. ")")
						t:add(f_auto, v_auto, v_auto:uint(), nil, "(" .. order[v_auto:uint()] .. ")")
						t:add(f_last_update, v_last_update, os.date("%Y-%m-%d %H:%M:%S", v_last_update:uint()), nil, "(" .. tostring("0x" .. v_last_update) .. ")")
					end
				end
			else
				t:add(f_cmd, v_cmd, "信令错误")
			end
		end

		return true
	end
	
	function p_syj.dissector(buf, pinfo, tree) 
		if syj_dissector(buf, pinfo, tree) then
			-- Valid SYJ Protocol
		else
			data_dis:call(buf, pinfo, tree)
		end
	end
	
	local udp_encap_table = DissectorTable.get("udp.port")
	udp_encap_table:add(20111, p_syj)
end
