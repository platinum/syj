#include "rxudp.h"
#include <sys/stat.h>

int
main (int argc, char *argv[])
{
	int socketfd;
	struct sockaddr_in sn;
	socklen_t snsize;
	int bind_rc;
	int ret;
	uint8_t recvbuf[sizeof(struct rxhdr) + SEGSIZE];
	struct rxhdr *rxh = (void *)recvbuf;
	int total_len, payload_len;
	struct stat file_stat;
	uint8_t *buf;
	int fd;


	if (argc != 2) {
		printf("USAGE: ./server_send filename\n");
		return -1;
	}

	buf = malloc(sizeof(struct rxhdr) + MAXSIZE);
	if (buf == NULL) {
		printf("malloc memory failed!\n");
		return -1;
	}

	if (stat(argv[1], &file_stat) < 0) {
		printf("file %s is not found\n", argv[1]);
		free(buf);
		return -1;
	}

	if (file_stat.st_size > MAXSIZE) {
		printf("File size is to big, must < 8MB.\n");
		free(buf);
		return -1;
	}

	fd = open(argv[1], O_RDWR, 0644);
	if (fd == -1) {
		printf("ERROR\n");
		free(buf);
		return -1;
	}

	if (read(fd, buf, file_stat.st_size) == -1) {
		printf("ERROR\n");
		free(buf);
		close(fd);
		return -1;
	}

	printf("read file \"%s\" %d bytes, OK\n",
		argv[1], (int)file_stat.st_size);
	close(fd);

	bzero(&sn, sizeof(sn));
	sn.sin_family = AF_INET;
	sn.sin_addr.s_addr = htonl(INADDR_ANY);
	sn.sin_port = htons(RXUDP_PORT);
	snsize = sizeof(sn);

	socketfd = socket(AF_INET, SOCK_DGRAM, 0);
	if (socketfd == -1) {
		perror("socket call failed");
		exit(errno);
	}

	bind_rc = bind(socketfd, (struct sockaddr *)&sn, snsize);
	if (bind_rc == -1) {
		perror("bind failed\n");
		exit(errno);
	}

	rxudp_setsim("FEDCBA987654321");
	for (;;) {
		ret = rxudp_get_packet(socketfd, &sn, TIMEOUT, &total_len, rxh);
		if ((ret <= GET_TIMEOUT) || (total_len != sizeof(struct rxhdr)))
			continue;

		switch (ret) {
			case GET_WRQ:
				ret = rxudp_server_recv(socketfd, &sn, buf, &payload_len);
				printf("ret = %d\n", ret);
				if (ret == ERR)
					continue;
				printf("received bytes: %d\n", payload_len);
				break;
			case GET_RRQ:
				ret = rxudp_server_send(socketfd, &sn, buf, file_stat.st_size);
				printf("ret = %d\n", ret);
				if (ret == ERR)
					continue;
				printf("send bytes: %u\n", (unsigned int)file_stat.st_size);
				break;
		}
	}

	close(socketfd);
	free(buf);
	return 0;
}
