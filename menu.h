#ifndef __MENU_H
#define __MENU_H

#include <stdio.h>
#include <setjmp.h>
#include <netdb.h>

#define MAXCHAR		11	/* 可输入最大字节数 - 1 */
#define NIPQUAD(addr) \
	((unsigned char *)&addr)[0], \
	((unsigned char *)&addr)[1], \
	((unsigned char *)&addr)[2], \
	((unsigned char *)&addr)[3]

typedef int menu_fun (void);

extern menu_fun *menu_getfun (int num);
extern jmp_buf go_out;
extern int menu_show (void);
extern int setup_syj (void);
extern int setup_server (void);
extern int syj_01 (void);
extern int syj_02 (void);
extern int syj_03 (void);
extern int syj_04 (void);
extern int syj_05 (void);
extern int syj_06 (void);
extern int syj_07 (void);
extern int syj_08 (void);
extern int syj_09 (void);
extern int syj_0a (void);
extern int syj_0b (void);
extern int syj_0c (void);
extern int syj_0d (void);
extern int syj_0e (void);
extern int syj_0f (void);
extern int syj_10 (void);
extern int syj_11 (void);
extern int syj_12 (void);
extern int syj_13 (void);
extern int syj_14 (void);
extern int syj_15 (void);
extern int syj_16 (void);
extern int syj_17 (void);
extern int syj_18 (void);
extern int syj_19 (void);
extern int syj_1a (void);
extern int syj_1b (void);
extern int syj_1c (void);
extern int syj_1d (void);
extern int syj_1e (void);
extern int syj_1f (void);
extern int syj_20 (void);
extern int syj_21 (void);
extern int syj_22 (void);
extern int quit (void);

struct menu {
	char *name;
	int (*fun)(void);
};

#endif /* __MENU_H */
