#include "rxudp.h"

#define NIPQUAD(addr) \
	((unsigned char *)&addr)[0], \
	((unsigned char *)&addr)[1], \
	((unsigned char *)&addr)[2], \
	((unsigned char *)&addr)[3]

int
main (int argc, char *argv[])
{
	int fd;
	struct sockaddr_in sn;
	struct hostent *host;
	int ret;
	int len = 0;
	uint8_t *buf;

	if (argc != 3) {
		printf("./client IP filename\n");
		return -1;
	}

	buf = malloc(sizeof(struct rxhdr) + MAXSIZE);
	if (buf == NULL) {
		printf("malloc memory failed!\n");
		return -1;
	}

	rxudp_setsim("123456789abcdef");
	bzero(&sn, sizeof(sn));
	sn.sin_family = AF_INET;
	host = gethostbyname(argv[1]);
	memcpy(&sn.sin_addr.s_addr, host->h_addr, sizeof(sn.sin_addr.s_addr));
	sn.sin_port = htons(RXUDP_PORT);
	fd = socket(AF_INET, SOCK_DGRAM, 0);
	printf("Connect to %u.%u.%u.%u. . .\n", NIPQUAD(*host->h_addr));
	ret = rxudp_client_recv(fd, &sn, buf, &len);
	close(fd);

	if (ret != OK) {
		printf("Receive file error!\n");
		free(buf);
		return -1;
	}

	printf("Save %u bytes to file: \"%s\". . .\n", len, argv[2]);
	fd = open(argv[2], O_RDWR | O_CREAT, 0644);
	if (fd == -1) {
		printf("Create file error!\n");
		free(buf);
		return -1;
	}
	if (write(fd, buf, len) == -1) {
		printf("Writing file error!\n");
		free(buf);
		return -1;
	}
	close(fd);

	free(buf);
	return 0;
}
