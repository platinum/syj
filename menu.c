#include "menu.h"
#include "flashmem.h"
#include "rxudp.h"
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "syj_struct.h"
#include "global.h"

int socketfd;
struct sockaddr_in sn;
extern int timestamp_flag;

struct menu menutab[] = {
	{ NULL 							},
	{ "设置硬件信息",			setup_syj	},
	{ "设置服务器信息",			setup_server	},
	{ "(0x01) 信息验证",			syj_01		},
	{ "(0x02) 全局参数",			syj_02		},
	{ "(0x03) 小票信息",			syj_03		},
	{ "(0x04) 商家信息",			syj_04		},
	{ "(0x05) 销售价格定制信息",		syj_05		},
	{ "(0x06) 仓库信息",			syj_06		},
	{ "(0x07) 员工信息",			syj_07		},
	{ "(0x08) 客户信息",			syj_08		},
	{ "(0x09) 供应商信息",			syj_09		},
	{ "(0x0a) 商品基本信息",		syj_0a		},
	{ "(0x0b) 商品单位信息",		syj_0b		},
	{ "(0x0c) 商品批次信息",		syj_0c		},
	{ "(0x0d) 待办任务信息取得",		syj_0d		},
	{ "(0x0e) 系统消息",			syj_0e		},
	{ "(0x0f) 商品类别信息",		syj_0f		},
	{ "(0x10) 数据字典",			syj_10		},
	{ "(0x11) 商品类别/证件编号名称对照",	syj_11		},
	{ "(0x12) 差异化同步请求",		syj_12		},
	{ "(0x13) 待办任务信息处理",		syj_13		},
	{ "(0x14) 索取商家证请求",		syj_14		},
	{ "(0x15) 索取产品证请求",		syj_15		},
	{ "(0x16) 进货单生成",			syj_16		},
	{ "(0x17) 销售单生成",			syj_17		},
	{ "(0x18) 退市单生成",			syj_18		},
	{ "(0x19) 退市处理单生成",		syj_19		},
	{ "(0x1a) 商品基本信息管理",		syj_1a		},
	{ "(0x1b) 商品单位信息管理",		syj_1b		},
	{ "(0x1c) 商品档案信息获取",		syj_1c		},
	{ "(0x1d) 取销售单商品信息请求",	syj_1d		},
	{ "(0x1e) 获取升级包信息请求",		syj_1e		},
	{ "(0x1f) 软件升级请求",		syj_1f		},
	{ "(0x20) 员工管理请求",		syj_20		},
	{ "(0x21) 供应商管理请求",		syj_21		},
	{ "(0x22) 客户管理请求",		syj_22		},
	{ "退出",				quit		},
	{ NULL							}
};

int
syj_connect (void)
{
	struct hostent *host;

	bzero(&sn, sizeof(sn));
	sn.sin_family = AF_INET;
	if (!(SYJ_HOST[0] && SYJ_PORT[0])) {
		printf("\n提示: 服务器信息不能为空!\n");
		exit(0);
	}
	/* 根据域名取 IP，并放到 socket 中 */
	host = gethostbyname(SYJ_HOST);
	memcpy(&sn.sin_addr.s_addr, host->h_addr, sizeof(sn.sin_addr.s_addr));
	sn.sin_port = htons(atoi(SYJ_PORT));

	socketfd = socket(AF_INET, SOCK_DGRAM, 0);
	if (socketfd == -1) {
		return -1;
	}
	rxudp_setsim(SYJ_SIM);

	return 0;
}

int
syj_disconnect (void)
{
	close(socketfd);
	return 0;
}

int
syj_put (char *buf, int len)
{
	int ret;

	ret = syj_connect();
	if (ret == -1) {
		printf("syj_connect error!\n");
	}

	ret = rxudp_client_send(socketfd, &sn, (void *)buf, len);
	if (ret == -1) {
		printf("rxudp_client_send error!\n");
		return ret;
	}

	syj_disconnect();

	return ret;
}

int
syj_get (char *buf, int *len)
{
	int ret;

	syj_connect();
	ret = rxudp_client_recv(socketfd, &sn, (void *)buf, len);
	syj_disconnect();

	return ret;
}

int
menu_show (void)
{
	int i = 1;

	printf("\n收银机模拟器（客户端）版本: %s\n", SYJ_VERSION);
	printf("\n----------- 菜 单 -----------------\n");
	while (menutab[i].name) {
		printf("%2d. %s\n", i, menutab[i].name);
		i++;
	}
	printf("----------------------------------\n");

	return i - 1;
}

menu_fun *
menu_getfun (int num)
{
	return menutab[num].fun;
}

void
format_data (char *p, int len)
{
	int i;

	for (i=0; i<len; i++) {
		if (p[i] >= 'a' && p[i] <= 'z')
			continue;
		if (p[i] >= 'A' && p[i] <= 'Z')
			continue;
		if (p[i] >= '0' && p[i] <= '9')
			continue;
		if (p[i] == ' ' || p[i] == '.' || p[i] == '-')
			continue;
		p[i] = 0x00;
	}
	p[len] = 0x00;
}

int
setup_syj (void)
{
	char buf[10];

	printf("设置收银机硬件信息\n");
	printf("收银机SIM卡号: %s\n", SYJ_SIM[0] ? SYJ_SIM : "（空）");
	printf("收银机硬件串号: %s\n", SYJ_HARDWARE[0] ? SYJ_HARDWARE : "（空）");

	printf("是否要重置？（y/N) ");
	while (fgets(buf, 10, stdin) == NULL);
	if (!(buf[0] == 'Y' || buf[0] == 'y'))
		return 0;

	printf("请输入收银机SIM卡号: ");
	while (fgets(SYJ_SIM, 0x100, stdin) == NULL);
	format_data(SYJ_SIM, 15);

	printf("请输入收银机硬件串号: ");
	while (fgets(SYJ_HARDWARE, 0x100, stdin) == NULL);
	format_data(SYJ_HARDWARE, 20);

	return 0;
}

int
setup_server (void)
{
	struct hostent *host;
	char buf[10];

	printf("设置服务器信息\n");
	printf("服务器地址: %s\n", SYJ_HOST[0] ? SYJ_HOST : "（空）");
	printf("服务器端口号: %s\n", SYJ_PORT[0] ? SYJ_PORT : "（空）");

	printf("是否要重新设置？（y/N) ");
	while (fgets(buf, 10, stdin) == NULL);
	if (!(buf[0] == 'Y' || buf[0] == 'y'))
		return 0;

	printf("请输入服务器地址: ");
	while (fgets(SYJ_HOST, 0x100, stdin) == NULL);
	format_data(SYJ_HOST, 100);
	host = gethostbyname(SYJ_HOST);
	if (host) {
		printf("IP 地址: %u.%u.%u.%u\n", NIPQUAD(*host->h_addr));
	} else {
		printf("域名 %s 不能被解析！\n", SYJ_HOST);
		return -1;
	}

	printf("请输入服务器端口号: ");
	while (fgets(SYJ_PORT, 0x100, stdin) == NULL);
	format_data(SYJ_PORT, 5);

	return 0;
}

int
syj_01 (void)
{
	char *buf;
	struct s_01_wrq *s01wh;
	struct s_01_rrq *s01rh;
	int len = 0;

	printf("\n>>> 收银机身份验证 <<<\n");

	buf = malloc(sizeof(struct rxhdr) + MAXSIZE);
	if (buf == NULL)
		return -1;

	s01wh = (void *)buf;
	s01rh = (void *)buf;

	s01wh->cmd = 0x01;
	memcpy(s01wh->sim, SYJ_SIM, sizeof(s01wh->sim));
	memcpy(s01wh->hardware, SYJ_HARDWARE, sizeof(s01wh->hardware));
	syj_put(buf, sizeof(struct s_01_wrq));

	syj_get(buf, &len);
	printf("接收到数据长度: %d\n", len);
	printf("result = 0x%.2x\n", s01rh->result);
	printf("收银机信息验证: ");
	if (s01rh->result) {
		printf("成功 ^O^\n");
		printf("商家 ID: %lu (0x%.16lx)\n",
			cvt64(s01rh->dept_id),
			cvt64(s01rh->dept_id));
		DEPT_ID = cvt64(s01rh->dept_id);
		printf("收银机编号: ");
		print_str(s01rh->num, sizeof(s01rh->num));
	}
	else
		printf("失败 -_-~!\n");

	free(buf);
	return 0;
}

int
syj_02 (void)
{
	char *buf;
	struct s_02_wrq *s02wh;
	struct s_02_rrq *s02rh;
	int len = 0, ret = 1;

	printf("\n>>> 全局参数同步 <<<\n");

	buf = malloc(sizeof(struct rxhdr) + MAXSIZE);
	if (buf == NULL)
		return -1;

	s02wh = (void *)buf;
	s02rh = (void *)buf;

	s02wh->cmd = 0x02;
	s02wh->dept_id = cvt64(DEPT_ID);
	s02wh->last_update = timestamp_flag ? 0 : htonl(0x11110002);
	syj_put(buf, sizeof(struct s_02_wrq));

	ret = syj_get(buf, &len);
	if (ret == OK) {
		printf("接收到数据长度: %d\n", len);
		printf("信令编号: 0x%.2x\n", s02rh->cmd);
		printf("是否启用在售货架: 0x%.2x%s\n", s02rh->use_shelf,
			s02rh->use_shelf > 2 ?
			yes_no[0] :
			yes_no[s02rh->use_shelf]);
		printf("是否直接出库: 0x%.2x%s\n", s02rh->direct_out,
			s02rh->direct_out > 2 ?
			yes_no[0] :
			yes_no[s02rh->direct_out]);
		printf("是否直接入库: 0x%.2x%s\n", s02rh->direct_in,
			s02rh->direct_in > 2 ?
			yes_no[0] :
			yes_no[s02rh->direct_in]);
		printf("是否选择批次: 0x%.2x%s\n", s02rh->batch,
			s02rh->batch > 2 ?
			yes_no[0] :
			yes_no[s02rh->batch]);
		printf("批次号排序规则: 0x%.2x%s\n", s02rh->batch_order,
			s02rh->batch_order > 2 ?
			order[0] :
			order[s02rh->batch_order]);
		printf("是否自动生成进货单: 0x%.2x%s\n", s02rh->if_auto,
			s02rh->if_auto > 2 ?
			yes_no[0] :
			yes_no[s02rh->if_auto]);
		printf("最后更新时间: 0x%.8x\n", ntohl(s02rh->last_update));
	} else
		printf("全局参数同步: 失败！ -_-!\n");

	free(buf);
	return 0;
}

int
syj_03 (void)
{
	char *buf;
	struct s_03_wrq *s03wh;
	struct s_03_rrq *s03rh;
	int len = 0, ret = OK, offset = 0;
	char *h1, *h2, *h3, *h4, *h5;
	char *t1, *t2, *t3;
	uint8_t h1_len;
	uint8_t h2_len;
	uint8_t h3_len;
	uint8_t h4_len;
	uint8_t h5_len;
	uint8_t t1_len;
	uint8_t t2_len;
	uint8_t t3_len;
	uint8_t h1_f;
	uint8_t h2_f;
	uint8_t h3_f;
	uint8_t h4_f;
	uint8_t h5_f;
	uint8_t t1_f;
	uint8_t t2_f;
	uint8_t t3_f;
	uint32_t *last_update;

	printf("\n>>> 小票信息同步 <<<\n");

	buf = malloc(sizeof(struct rxhdr) + MAXSIZE);
	if (buf == NULL)
		return -1;

	s03wh = (void *)buf;
	s03rh = (void *)buf;

	s03wh->cmd = 0x03;
	s03wh->last_update = timestamp_flag ? 0 : htonl(0x11110003);
	syj_put(buf, sizeof(struct s_03_wrq));

	ret = syj_get(buf, &len);
	if (ret == -1) {
		printf("小票信息接收失败！\n");
		return -1;
	}

	offset = 1;

	h1_len = buf[offset];
	offset += 1;
	h1 = (void *)buf + offset;
	offset += h1_len;

	h2_len = buf[offset];
	offset += 1;
	h2 = (void *)buf + offset;
	offset += h2_len;

	h3_len = buf[offset];
	offset += 1;
	h3 = (void *)buf + offset;
	offset += h3_len;

	h4_len = buf[offset];
	offset += 1;
	h4 = (void *)buf + offset;
	offset += h4_len;

	h5_len = buf[offset];
	offset += 1;
	h5 = (void *)buf + offset;
	offset += h5_len;

	t1_len = buf[offset];
	offset += 1;
	t1 = (void *)buf + offset;
	offset += t1_len;

	t2_len = buf[offset];
	offset += 1;
	t2 = (void *)buf + offset;
	offset += t2_len;

	t3_len = buf[offset];
	offset += 1;
	t3 = (void *)buf + offset;
	offset += t3_len;

	h1_f = buf[offset + 0];
	h2_f = buf[offset + 1];
	h3_f = buf[offset + 2];
	h4_f = buf[offset + 3];
	h5_f = buf[offset + 4];
	t1_f = buf[offset + 5];
	t2_f = buf[offset + 6];
	t3_f = buf[offset + 7];
	offset += 8;

	last_update = (void *)buf + offset;

	printf("接收到数据长度: %d\n", len);
	printf("信令编号: 0x%.2x\n", s03rh->cmd);
	printf("票头第 1 行长度: %d\n", h1_len);
	printf("票头第 1 行内容: ");
	print_str(h1, h1_len);
	printf("票头第 1 行双高: 0x%.2x\n", h1_f);

	printf("票头第 2 行长度: %d\n", h2_len);
	printf("票头第 2 行内容: ");
	print_str(h2, h2_len);
	printf("票头第 2 行双高: 0x%.2x\n", h2_f);

	printf("票头第 3 行长度: %d\n", h3_len);
	printf("票头第 3 行内容: ");
	print_str(h3, h3_len);
	printf("票头第 3 行双高: 0x%.2x\n", h3_f);

	printf("票头第 4 行长度: %d\n", h4_len);
	printf("票头第 4 行内容: ");
	print_str(h4, h4_len);
	printf("票头第 4 行双高: 0x%.2x\n", h4_f);

	printf("票头第 5 行长度: %d\n", h5_len);
	printf("票头第 5 行内容: ");
	print_str(h5, h5_len);
	printf("票头第 5 行双高: x%.2x\n", h5_f);

	printf("票尾第 1 行长度: %d\n", t1_len);
	printf("票尾第 1 行内容: ");
	print_str(t1, t1_len);
	printf("票尾第 1 行双高: 0x%.2x\n", t1_f);

	printf("票尾第 2 行长度: %d\n", t2_len);
	printf("票尾第 2 行内容: ");
	print_str(t2, t2_len);
	printf("票尾第 2 行双高: 0x%.2x\n", t2_f);

	printf("票尾第 3 行长度: %d\n", t3_len);
	printf("票尾第 3 行内容: ");
	print_str(t3, t3_len);
	printf("票尾第 3 行双高: 0x%.2x\n", t3_f);

	printf("最后更新时间: 0x%.8x\n", ntohl(*last_update));

	free(buf);
	return 0;
}

int
syj_04 (void)
{
	char *buf;
	struct s_04_wrq *s04wh;
	struct s_04_rrq *s04rh;
	int len = 0, ret = OK, offset = 0;

	printf("\n>>> 商家信息同步 <<<\n");

	buf = malloc(sizeof(struct rxhdr) + MAXSIZE);
	if (buf == NULL)
		return -1;

	s04wh = (void *)buf;
	s04rh = (void *)buf;

	s04wh->cmd = 0x04;
	s04wh->dept_id = cvt64(DEPT_ID);
	s04wh->last_update = timestamp_flag ? 0 : htonl(0x11110004);
	syj_put(buf, sizeof(struct s_04_wrq));

	ret = syj_get(buf, &len);
	if (ret == -1) {
		printf("商家信息接收失败！\n");
		return -1;
	}

	uint64_t *id, *pid, *code;
	char *num, *name, *tel, *address;
	uint16_t *province, *city;
	uint8_t num_len, name_len, tel_len, address_len;
	uint32_t *last_update;

	offset = 1;
	id = (void *)buf + offset;
	offset += 8;

	pid = (void *)buf + offset;
	offset += 8;

	num_len = buf[offset];
	offset += 1;
	num = (void *)buf + offset;
	offset += num_len;

	code = (void *)buf + offset;
	offset += sizeof(uint64_t);

	name_len = buf[offset];
	offset += sizeof(name_len);
	name = (void *)buf + offset;
	offset += name_len;

	tel_len = buf[offset];
	offset += sizeof(tel_len);
	tel = (void *)buf + offset;
	offset += tel_len;

	address_len = buf[offset];
	offset += sizeof(address_len);
	address = (void *)buf + offset;
	offset += address_len;

	province = (void *)buf + offset;
	offset += sizeof(*province);

	city = (void *)buf + offset;
	offset += sizeof(*city);

	last_update = (void *)buf + offset;

	printf("接收到数据长度: %d\n", len);
	printf("信令编号: 0x%.2x\n", s04rh->cmd);
	printf("接收到的商家 ID: %lu (0x%.16lx)\n",
		cvt64(*id), cvt64(*id));
	printf("接收到的商家 PID: %lu (0x%.16lx)\n",
		cvt64(*pid), cvt64(*pid));
	printf("接收到的营业执照号码长度: %d\n", num_len);
	printf("接收到的营业执照号码: ");
	print_str(num, num_len);
	printf("接收到的商家编码: ");
	print_str((void *)code, sizeof(uint64_t));
	printf("接收到的商家名称长度: %d\n", name_len);
	printf("接收到的商家名称: ");
	print_str(name, name_len);
	printf("接收到的商家电话长度: %d\n", tel_len);
	printf("接收到的商家电话: ");
	print_str(tel, tel_len);
	printf("接收到的商家地址长度: %d\n", address_len);
	printf("接收到的商家地址: ");
	print_str(address, address_len);
	printf("接收到的省份: 0x%.4x\n", ntohs(*province));
	printf("接收到的城市: 0x%.4x\n", ntohs(*city));
	printf("最后更新时间: 0x%.8x\n", ntohl(*last_update));

	free(buf);
	return 0;
}

int
syj_05 (void)
{
	char *buf;
	struct s_05_wrq *s05wh;
	struct s_05_rrq *s05rh;
	struct s_05_rrq_data *data;
	int len = 0, ret;
	int i;

	printf("\n>>> 销售价格定制信息同步 <<<\n");

	buf = malloc(sizeof(struct rxhdr) + MAXSIZE);
	if (buf == NULL)
		return -1;

	s05wh = (void *)buf;
	s05rh = (void *)buf;
	s05wh->cmd = 0x05;
	s05wh->dept_id = cvt64(DEPT_ID);
	s05wh->last_update = timestamp_flag ? 0 : htonl(0x11110005);
	syj_put(buf, sizeof(struct s_05_wrq));

	ret = syj_get(buf, &len);
	if (ret == -1) {
		printf("销售价格定制信息接收失败！\n");
		return -1;
	}

	printf("接收到数据长度: %d\n", len);
	printf("信令编号: 0x%.2x\n", s05rh->cmd);
	printf("总记录数: %d\n", s05rh->record_count);
	printf("最后更新时间: 0x%.8x\n", ntohl(s05rh->last_update));
	data = (void *)s05rh->data;
	for (i=0; i<s05rh->record_count; i++) {
		printf("\n--- 第 %d 条数据 ---\n", i+1);
		printf("客户类型: 0x%.2x%s\n",
			data->client_type,
			(data->client_type > 0x03) ?
			"（未知）" :
			client_type[data->client_type]);
		printf("销售价格类型: 0x%.2x%s\n",
			data->price_type,
			(data->price_type > 0x03) ?
			"（未知）" :
			price_type[data->price_type]);
		data++;
	}

	free(buf);
	return 0;
}

int
syj_06 (void)
{
	char *buf;
	struct s_06_wrq *s06wh;
	struct s_06_rrq *s06rh;
	uint8_t *data = NULL;
	int len = 0, ret;
	int i;

	printf("\n>>> 仓库信息同步 <<<\n");

	buf = malloc(sizeof(struct rxhdr) + MAXSIZE);
	if (buf == NULL)
		return -1;

	s06wh = (void *)buf;
	s06rh = (void *)buf;
	s06wh->cmd = 0x06;
	s06wh->dept_id = cvt64(DEPT_ID);
	s06wh->last_update = timestamp_flag ? 0 : htonl(0x11110006);
	syj_put(buf, sizeof(struct s_06_wrq));

	ret = syj_get(buf, &len);
	if (ret == -1) {
		printf("仓库信息接收失败！\n");
		return -1;
	}

	uint64_t *id;
	printf("接收到数据长度: %d\n", len);
	printf("信令编号: 0x%.2x\n", s06rh->cmd);
	printf("总记录数: %d\n", s06rh->record_count);

	printf("最后更新时间: 0x%.8x\n", ntohl(s06rh->last_update));
	data = (void *)s06rh->data;
	for (i=0; i<s06rh->record_count; i++) {
		printf("\n--- 第 %d 条数据 ---\n", i+1);
		id = (void *)data;
		printf("仓库 ID: %lu (0x%.16lx)\n",
			cvt64(*id), cvt64(*id));
		data += sizeof(*id);
		printf("仓库名称长度: %d\n", *data);
		printf("仓库名称: ");
		print_str((char *)data + 1, *data);
		data = data + 1 + *data;
		printf("仓库编码长度: %d\n", *data);
		printf("仓库编码: ");
		print_str((char *)data + 1, *data);
		data = data + 1 + *data;
		printf("仓库类型: 0x%.2x%s\n", *data,
			(*data > 0x3) ?
			cangku[0] :
			cangku[(uint8_t)*data]);
		data++;
		id = (void *)data;
		printf("库管员 ID: %lu (0x%.16lx)\n",
			cvt64(*id), cvt64(*id));
		data += 8;
		printf("库管员姓名长度: %d\n", *data);
		printf("库管员姓名: ");
		print_str((char *)data + 1, *data);
		data = data + 1 + *data;
		printf("是否默认: 0x%.2x%s\n", *data,
			*data > 2 ? yes_no[0] : yes_no[*data]);
		data++;
		printf("操作类型: 0x%.2x%s\n", *data,
			(*data > 0x03) ?
			oper_type[0] :
			oper_type[(uint8_t)*data]);
		data++;
	}

	free(buf);
	return 0;
}

int
syj_07 (void)
{
	char *buf;
	struct s_07_wrq *s07wh;
	struct s_07_rrq *s07rh;
	char *data = NULL;
	uint64_t *id;
	int len = 0, i, ret;

	printf("\n>>> 员工信息同步 <<<\n");

	buf = malloc(sizeof(struct rxhdr) + MAXSIZE);
	if (buf == NULL)
		return -1;

	s07wh = (void *)buf;
	s07rh = (void *)buf;
	s07wh->cmd = 0x07;
	s07wh->dept_id = cvt64(DEPT_ID);
	s07wh->last_update = timestamp_flag ? 0 : htonl(0x11110007);
	printf("信令编号: 0x%.2x\n", s07wh->cmd);
	printf("最后更新时间: 0x%.8x\n", ntohl(s07wh->last_update));
	printf("发送的数据长度: %ld\n", sizeof(*s07wh));
	syj_put(buf, sizeof(struct s_07_wrq));

	ret = syj_get(buf, &len);
	if (ret == -1) {
		printf("员工信息接收失败！\n");
		return -1;
	}

	printf("接收到数据长度: %d\n", len);
	printf("信令编号: 0x%.2x\n", s07rh->cmd);
	int num = ntohs(s07rh->record_count);
	printf("总记录数: %d\n", num);
	printf("最后更新时间: 0x%.8x\n", ntohl(s07rh->last_update));
	data = (void *)s07rh->data;
	for (i=0; i<num; i++) {
		printf("\n--- 第 %d 条数据 ---\n", i+1);
		id = (void *)data;
		printf("员工 ID: %lu (0x%.16lx)\n",
			cvt64(*id), cvt64(*id));
		data += 8;

		printf("员工编号: ");
		print_str(data, 4);
		data += 4;

		printf("员工密码: ");
		print_str(data, 6);
		data += 6;

		printf("员工姓名长度: %d\n", *data);
		printf("员工姓名: ");
		print_str(data + 1, *data);
		data = data + 1 + *data;

		printf("操作类型: 0x%.2x %s\n", *data,
			(*data > 0x03) ?
			oper_type[0] :
			oper_type[(uint8_t)*data]);
		data++;
	}

	free(buf);
	return 0;
}

int
syj_08 (void)
{
	char *buf;
	struct s_08_wrq *s08wh;
	struct s_08_rrq *s08rh;
	struct s_08_rrq_data s08data;
	char *data = NULL;
	uint64_t *id;
	int len = 0, i, ret;

	printf("\n>>> 客户信息同步 <<<\n");

	buf = malloc(sizeof(struct rxhdr) + MAXSIZE);
	if (buf == NULL)
		return -1;

	s08wh = (void *)buf;
	s08rh = (void *)buf;
	s08wh->cmd = 0x08;
	s08wh->last_update = timestamp_flag ? 0 : htonl(0x11110008);
	printf("信令编号: 0x%.2x\n", s08wh->cmd);
	s08wh->dept_id = cvt64(DEPT_ID);
	printf("最后更新时间: 0x%.8x\n", ntohl(s08wh->last_update));
	printf("发送的数据长度: %ld\n", sizeof(*s08wh));
	syj_put(buf, sizeof(struct s_08_wrq));

	ret = syj_get(buf, &len);
	if (ret == -1) {
		printf("客户信息接收失败！\n");
		return -1;
	}

	printf("接收到数据长度: %d\n", len);
	printf("信令编号: 0x%.2x\n", s08rh->cmd);
	int num = ntohs(s08rh->record_count);
	printf("总记录数: %d\n", num);
	printf("最后更新时间: 0x%.8x\n", ntohl(s08rh->last_update));
	data = (void *)s08rh->data;
	for (i=0; i<num; i++) {
		printf("\n--- 第 %d 条数据 ---\n", i+1);
		id = (void *)data;
		printf("客户 ID: %lu (0x%.16lx)\n",
			cvt64(*id), cvt64(*id));
		data += sizeof(s08data.id);;

		printf("营业执照号码长度: %d\n", *data);
		printf("营业执照号码: ");
		print_str(data + 1, *data);
		data = data + 1 + *data;

		printf("客户名称长度: %d\n", *data);
		printf("客户名称: ");
		print_str(data + 1, *data);
		data = data + 1 + *data;

		printf("客户编码: ");
		print_str(data, sizeof(s08data.sys_dept_code));
		data = data + sizeof(s08data.sys_dept_code);

		printf("操作类型: 0x%.2x %s\n", *data,
			(*data > 0x03) ?
			oper_type[0] :
			oper_type[(uint8_t)*data]);
		data++;
	}

	free(buf);
	return 0;
}

int
syj_09 (void)
{
	char *buf;
	struct s_09_wrq *s09wh;
	struct s_09_rrq *s09rh;
	struct s_09_rrq_data s09data;
	char *data = NULL;
	uint64_t *id;
	int len = 0, i, ret;

	printf("\n>>> 供应商信息同步 <<<\n");
	buf = malloc(sizeof(struct rxhdr) + MAXSIZE);
	if (buf == NULL)
		return -1;

	s09wh = (void *)buf;
	s09rh = (void *)buf;
	s09wh->cmd = 0x09;
	s09wh->last_update = timestamp_flag ? 0 : htonl(0x11110009);
	printf("信令编号: 0x%.2x\n", s09wh->cmd);
	s09wh->dept_id = cvt64(DEPT_ID);
	printf("最后更新时间: 0x%.8x\n", ntohl(s09wh->last_update));
	printf("发送的数据长度: %ld\n", sizeof(*s09wh));
	syj_put(buf, sizeof(struct s_09_wrq));

	ret = syj_get(buf, &len);
	if (ret == -1) {
		printf("供应商信息接收失败！\n");
		return -1;
	}

	printf("接收到数据长度: %d\n", len);
	printf("信令编号: 0x%.2x\n", s09rh->cmd);
	int num = ntohs(s09rh->record_count);
	printf("总记录数: %d\n", num);
	printf("最后更新时间: 0x%.8x\n", ntohl(s09rh->last_update));
	data = (void *)s09rh->data;
	for (i=0; i<num; i++) {
		printf("\n--- 第 %d 条数据 ---\n", i+1);
		id = (void *)data;
		printf("供应商 ID: %lu (0x%.16lx)\n",
			cvt64(*id), cvt64(*id));
		data += sizeof(*id);

		printf("营业执照号码长度: %d\n", *data);
		printf("营业执照号码: ");
		print_str(data + 1, *data);
		data = data + 1 + *data;

		printf("供应商名称长度: %d\n", *data);
		printf("供应商名称: ");
		print_str(data + 1, *data);
		data = data + 1 + *data;

		printf("供应商编码: ");
		print_str(data, sizeof(s09data.sys_dept_code));
		data = data + sizeof(s09data.sys_dept_code);

		printf("操作类型: 0x%.2x %s\n", *data,
			(*data > 0x03) ?
			oper_type[0] :
			oper_type[(uint8_t)*data]);
		data++;
	}

	free(buf);
	return 0;
}

int
syj_0a (void)
{
	char *buf;
	struct s_0a_wrq *s0awh;
	struct s_0a_rrq *s0arh;
	struct s_0a_rrq_data s0adata;
	uint8_t *data = NULL;
	uint64_t *id;
	int len = 0, i, ret;

	printf("\n>>> 商品基本信息同步 <<<\n");

	buf = malloc(sizeof(struct rxhdr) + MAXSIZE);
	if (buf == NULL)
		return -1;

	s0awh = (void *)buf;
	s0arh = (void *)buf;
	s0awh->cmd = 0x0a;
	s0awh->last_update = timestamp_flag ? 0 : htonl(0x1111000a);
	printf("信令编号: 0x%.2x\n", s0awh->cmd);
	s0awh->dept_id = cvt64(DEPT_ID);
	printf("最后更新时间: 0x%.8x\n", ntohl(s0awh->last_update));
	printf("发送的数据长度: %ld\n", sizeof(*s0awh));
	syj_put(buf, sizeof(struct s_0a_wrq));

	ret = syj_get(buf, &len);
	if (ret == -1) {
		printf("供应商信息接收失败！\n");
		return -1;
	}

	printf("接收到数据长度: %d\n", len);
	printf("信令编号: 0x%.2x\n", s0arh->cmd);
	int num = ntohs(s0arh->record_count);
	printf("总记录数: %d\n", num);
	printf("最后更新时间: 0x%.8x\n", ntohl(s0arh->last_update));
	data = (void *)s0arh->data;
	for (i=0; i<num; i++) {
		printf("\n--- 第 %d 条数据 ---\n", i+1);
		id = (void *)data;
		printf("商品 ID: %lu (0x%.16lx)\n",
			cvt64(*id), cvt64(*id));
		data += sizeof(*id);

		printf("商品条码长度: %d\n", *data);
		printf("商品条码: ");
		print_str((char *)data + 1, *data);
		data = data + 1 + *data;

		printf("商品名称长度: %d\n", *data);
		printf("商品名称: ");
		print_str((char *)data + 1, *data);
		data = data +1 + *data;

		printf("基本单位名称长度: %d\n", *data);
		printf("基本单位名称: ");
		print_str((char *)data + 1, *data);
		data += 1 + *data;

		printf("商品类别 ID: %lu (0x%.16lx)\n",
			cvt64(*((uint64_t *)data)),
			cvt64(*((uint64_t *)data)));
		data += 8;

		printf("规格长度: %d\n", *data);
		printf("规格");
		print_str((char *)data + 1, *data);
		data = data + 1 + *data;

		printf("基础商品 ID: %lu (0x%.16lx)\n",
			cvt64(*((uint64_t *)data)),
			cvt64(*((uint64_t *)data)));
		data += 8;

		printf("是否公有: 0x%.2x%s\n", *data,
			*data > 2 ?
			yes_no[0] :
			yes_no[*data]);
		data++;

		printf("商家 ID: %lu (0x%.16lx)\n",
			cvt64(*((uint64_t *)data)),
			cvt64(*((uint64_t *)data)));
		data += 8;

		printf("证件编号长度: %d\n", *data);
		printf("证件编号: ");
		print_str((char *)data + 1, *data);
		data = data + 1 + *data;

		printf("操作类型: 0x%.2x%s\n", *data,
			(*data > 0x03) ?
			oper_type[0] :
			oper_type[(uint8_t)*data]);
		data += sizeof(s0adata.oper_type);
	}

	free(buf);
	return 0;
}

int
syj_0b (void)
{
	char *buf;
	struct s_0b_wrq *s0bwh;
	struct s_0b_rrq *s0brh;
	struct s_0b_rrq_data s0bdata;
	char *data = NULL;
	uint64_t *id;
	uint16_t *uint16;
	int len = 0, price_len = 10, i, ret;

	printf("\n>>> 商品基本单位信息同步 <<<\n");

	buf = malloc(sizeof(struct rxhdr) + MAXSIZE);
	if (buf == NULL)
		return -1;

	s0bwh = (void *)buf;
	s0brh = (void *)buf;
	s0bwh->cmd = 0x0b;
	s0bwh->last_update = timestamp_flag ? 0 : htonl(0x1111000b);
	printf("信令编号: 0x%.2x\n", s0bwh->cmd);
	s0bwh->dept_id = cvt64(DEPT_ID);
	printf("最后更新时间: 0x%.8x\n", ntohl(s0bwh->last_update));
	printf("发送的数据长度: %ld\n", sizeof(*s0bwh));
	syj_put(buf, sizeof(struct s_0b_wrq));

	ret = syj_get(buf, &len);
	if (ret == -1) {
		printf("商品基本单位信息接收失败！\n");
		return -1;
	}

	printf("接收到数据长度: %d\n", len);
	printf("信令编号: 0x%.2x\n", s0brh->cmd);
	int num = ntohl(s0brh->record_count);
	printf("总记录数: %d\n", num);
	printf("最后更新时间: 0x%.8x\n", ntohl(s0brh->last_update));
	data = (void *)s0brh->data;
	for (i=0; i<num; i++) {
		printf("\n--- 第 %d 条数据 ---\n", i+1);
		id = (void *)data;
		printf("商品 ID: %lu (0x%.16lx)\n",
			cvt64(*id), cvt64(*id));
		data += sizeof(*id);

		id = (void *)data;
		printf("单位 ID: %lu (0x%.16lx)\n",
			cvt64(*id), cvt64(*id));
		data += sizeof(*id);

		printf("单位名称长度: %d\n", *data);
		printf("单位名称: ");
		print_str(data + 1, *data);
		data = data + 1 + *data;

		uint16 = (void *)data;
		printf("单位关系: %d\n", ntohs(*uint16));
		data += 2;

		printf("条码长度: %d\n", *data);
		printf("条码: ");
		print_str(data + 1, *data);
		data = data + 1 + *data;

		printf("零售价: ");
		format_price(data, price_len);
		data += price_len;

		printf("会员价: ");
		format_price(data, price_len);
		data += price_len;

		printf("经销商价: ");
		format_price(data, price_len);
		data += price_len;

		printf("进货参考价: ");
		format_price(data, price_len);
		data += price_len;

		printf("单位类型: 0x%.2x%s\n", *data,
			(*data > 0x02) ?
			sorts[0] :
			sorts[(uint8_t)*data]);
		data += sizeof(s0bdata.sort);

		printf("操作类型: 0x%.2x%s\n", *data,
			(*data > 0x03) ?
			oper_type[0] :
			oper_type[(uint8_t)*data]);
		data += sizeof(s0bdata.oper_type);
	}

	free(buf);
	return 0;
}

int
syj_0c (void)
{
	char *buf;
	struct s_0c_wrq *s0cwh;
	struct s_0c_rrq *s0crh;
	struct s_0c_rrq_data s0cdata;
	char *data = NULL;
	uint64_t *id;
	uint16_t *uint16;
	int len = 0, i, ret;

	printf("\n>>> 商品批次信息同步 <<<\n");
	buf = malloc(sizeof(struct rxhdr) + MAXSIZE);
	if (buf == NULL)
		return -1;

	s0cwh = (void *)buf;
	s0crh = (void *)buf;
	s0cwh->cmd = 0x0c;
	s0cwh->dept_id = cvt64(DEPT_ID);
	s0cwh->last_update = timestamp_flag ? 0 : htonl(0x1111000c);
	printf("信令编号: 0x%.2x\n", s0cwh->cmd);
	printf("最后更新时间: 0x%.8x\n", ntohl(s0cwh->last_update));
	printf("发送的数据长度: %ld\n", sizeof(*s0cwh));
	syj_put(buf, sizeof(struct s_0c_wrq));

	ret = syj_get(buf, &len);
	if (ret == -1) {
		printf("商品批次信息接收失败！\n");
		return -1;
	}

	printf("接收到数据长度: %d\n", len);
	printf("信令编号: 0x%.2x\n", s0crh->cmd);
	int num = ntohl(s0crh->record_count);
	printf("总记录数: %d\n", num);
	printf("最后更新时间: 0x%.8x\n", ntohl(s0crh->last_update));
	data = (void *)s0crh->data;
	for (i=0; i<num; i++) {
		printf("\n--- 第 %d 条数据 ---\n", i+1);
		id = (void *)data;
		printf("商品 ID: %lu (0x%.16lx)\n",
			cvt64(*id), cvt64(*id));
		data += sizeof(*id);

		printf("批次号长度: %d\n", *data);
		printf("批次号: ");
		print_str(data + 1, *data);
		data = data + 1 + *data;

		printf("生产日期长度: %d\n", *data);
		printf("生产日期: ");
		print_str(data + 1, *data);
		data = data + 1 + *data;

		uint16 = (void *)data;
		printf("保质期: %d\n", ntohs(*uint16));
		data = data + 2;

		printf("保质单位: 0x%.2x%s\n", *data,
			(*data > 0x03) ?
			quality_units[0] :
			quality_units[(uint8_t)*data]);
		data = data + 1;

		printf("过期日期长度: %d\n", *data);
		printf("过期日期: ");
		print_str(data + 1, *data);
		data = data + 1 + *data;

		id = (void *)data;
		printf("仓库 ID: %lu (0x%.16lx)\n",
			cvt64(*id), cvt64(*id));
		data += sizeof(*id);

		printf("操作类型: 0x%.2x%s\n", *data,
			(*data > 0x03) ?
			oper_type[0] :
			oper_type[(uint8_t)*data]);
		data += sizeof(s0cdata.oper_type);
	}

	free(buf);
	return 0;
}

int
syj_0d (void)
{
	char *buf;
	struct s_0d_wrq *s0dwh;
	struct s_0d_rrq *s0drh;
	struct s_0d_rrq_data s0ddata;
	char *data = NULL;
	uint64_t *id;
	int len = 0, i, ret;

	printf("\n>>> 待办任务信息同步 <<<\n");
	buf = malloc(sizeof(struct rxhdr) + MAXSIZE);
	if (buf == NULL)
		return -1;

	s0dwh = (void *)buf;
	s0drh = (void *)buf;
	s0dwh->cmd = 0x0d;
	s0dwh->dept_id = cvt64(DEPT_ID);
	s0dwh->last_update = timestamp_flag ? 0 : htonl(0x1111000d);
	printf("信令编号: 0x%.2x\n", s0dwh->cmd);
	printf("最后更新时间: 0x%.8x\n", ntohl(s0dwh->last_update));
	printf("发送的数据长度: %ld\n", sizeof(*s0dwh));
	syj_put(buf, sizeof(struct s_0d_wrq));

	ret = syj_get(buf, &len);
	if (ret == -1) {
		printf("待办任务信息接收失败！\n");
		return -1;
	}

	printf("接收到数据长度: %d\n", len);
	printf("信令编号: 0x%.2x\n", s0drh->cmd);
	printf("总记录数: %d\n", s0drh->record_count);
	printf("最后更新时间: 0x%.8x\n", ntohl(s0drh->last_update));
	data = (void *)s0drh->data;
	for (i=0; i<s0drh->record_count; i++) {
		printf("\n--- 第 %d 条数据 ---\n", i+1);
		id = (void *)data;
		printf("任务 ID: %lu (0x%.16lx)\n",
			cvt64(*id), cvt64(*id));
		data += sizeof(*id);

		printf("任务标题长度: %d\n", *data);
		printf("任务标题: ");
		print_str(data + 1, *data);
		data = data + 1 + *data;

		uint16_t *content_len;
		content_len = (void *)data;
		printf("任务内容长度: %d\n", ntohs(*content_len));
		printf("任务内容: ");
		print_str(data + 2, ntohs(*content_len));
		data = data + 2 + ntohs(*content_len);

		printf("发送时间: ");
		print_str(data, sizeof(s0ddata.send_time));
		data += sizeof(s0ddata.send_time);

		printf("任务类型: 0x%.2x%s\n", *data,
			(*data > 0x03) ?
			sort_type[0] :
			sort_type[(uint8_t)*data]);
		data += sizeof(s0ddata.sort);

		printf("任务参数长度: %d\n", *data);
		printf("任务参数: ");
		print_str(data + 1, *data);
		data = data + 1 + *data;

		printf("处理状态: 0x%.2x%s\n", *data,
			(*data > 0x01) ?
			status[2] :
			status[(uint8_t)*data]);
		data += sizeof(s0ddata.status);

		printf("操作类型: 0x%.2x%s\n", *data,
			(*data > 0x03) ?
			oper_type[0] :
			oper_type[(uint8_t)*data]);
		data += sizeof(s0ddata.oper_type);
	}

	free(buf);
	return 0;
}

int
syj_0e (void)
{
	char *buf;
	struct s_0e_wrq *s0ewh;
	struct s_0e_rrq *s0erh;
	struct s_0e_rrq_data s0edata;
	char *data = NULL;
	uint64_t *id;
	int len = 0, i, ret;

	printf("\n>>> 系统消息同步 <<<\n");
	buf = malloc(sizeof(struct rxhdr) + MAXSIZE);
	if (buf == NULL)
		return -1;

	s0ewh = (void *)buf;
	s0erh = (void *)buf;
	s0ewh->cmd = 0x0e;
	s0ewh->dept_id = cvt64(DEPT_ID);
	s0ewh->last_update = timestamp_flag ? 0 : htonl(0x1111000e);
	printf("信令编号: 0x%.2x\n", s0ewh->cmd);
	printf("最后更新时间: 0x%.8x\n", ntohl(s0ewh->last_update));
	printf("发送的数据长度: %ld\n", sizeof(*s0ewh));
	syj_put(buf, sizeof(struct s_0e_wrq));

	ret = syj_get(buf, &len);
	if (ret == -1) {
		printf("系统消息接收失败！\n");
		return -1;
	}

	printf("接收到数据长度: %d\n", len);
	printf("信令编号: 0x%.2x\n", s0erh->cmd);
	printf("总记录数: %d\n", s0erh->record_count);
	printf("最后更新时间: 0x%.8x\n", ntohl(s0erh->last_update));
	data = (void *)s0erh->data;
	for (i=0; i<s0erh->record_count; i++) {
		printf("\n--- 第 %d 条数据 ---\n", i+1);
		id = (void *)data;
		printf("消息 ID: %lu (0x%.16lx)\n",
			cvt64(*id), cvt64(*id));
		data += sizeof(*id);

		printf("消息标题长度: %d\n", *data);
		printf("消息标题: ");
		print_str(data + 1, *data);
		data = data + 1 + *data;

		uint16_t *content_len;
		content_len = (void *)data;
		printf("消息内容长度: %d\n", ntohs(*content_len));
		printf("消息内容: ");
		print_str(data + 2, ntohs(*content_len));
		data = data + 2 + ntohs(*content_len);

		printf("发送时间: ");
		print_str(data, sizeof(s0edata.send_time));
		data += sizeof(s0edata.send_time);

		printf("消息类型: 0x%.2x%s%s\n", *data,
			(*data > 0x03) ?
			sort_type[0] :
			sort_type[(uint8_t)*data], "结果");
		data += sizeof(s0edata.sort);

		printf("操作类型: 0x%.2x%s\n", *data,
			(*data > 0x03) ?
			oper_type[0] :
			oper_type[(uint8_t)*data]);
		data += sizeof(s0edata.oper_type);
	}

	free(buf);
	return 0;
}

int
syj_0f (void)
{
	char *buf;
	struct s_0f_wrq *s0fwh;
	struct s_0f_rrq *s0frh;
	uint8_t *p;
	uint64_t *id;
	int len = 0, i, ret;

	printf("\n>>> 商品类别信息 <<<\n");
	buf = malloc(sizeof(struct rxhdr) + MAXSIZE);
	if (buf == NULL)
		return -1;

	s0fwh = (void *)buf;
	s0frh = (void *)buf;
	s0fwh->cmd = 0x0f;
	s0fwh->last_update = timestamp_flag ? 0 : htonl(0x1111000f);
	printf("信令编号: 0x%.2x\n", s0fwh->cmd);
	printf("最后更新时间: 0x%.8x\n", ntohl(s0fwh->last_update));
	printf("发送的数据长度: %ld\n", sizeof(*s0fwh));
	syj_put(buf, sizeof(struct s_0f_wrq));

	ret = syj_get(buf, &len);
	if (ret == -1) {
		printf("商品类别信息接收失败！\n");
		return -1;
	}

	printf("接收到数据长度: %d\n", len);
	printf("信令编号: 0x%.2x\n", s0frh->cmd);
	printf("总记录数: %d\n", ntohs(s0frh->record_count));
	printf("最后更新时间: 0x%.8x\n", ntohl(s0frh->last_update));
	p = (void *)s0frh->data;
	for (i=0; i<ntohs(s0frh->record_count); i++) {
		printf("\n--- 第 %d 条数据 ---\n", i+1);
		id = (void *)p;
		printf("类别 ID: %lu (0x%.16lx)\n",
			cvt64(*id), cvt64(*id));
		p += 8;

		printf("类别编号长度: %d\n", *p);
		p++;

		printf("类别编号: ");
		print_str((char *)p, *(p-1));
		p += *(p-1);

		printf("类别名称长度: %d\n", *p);
		p++;

		printf("类别名称: ");
		print_str((char *)p, *(p-1));
		p += *(p-1);

		printf("监管属性: 0x%.2x%s\n", *p,
			*p > 0x04 ? supervise[0] : supervise[*p]);
		p++;

		printf("编码长度: %d\n", *p);
		p++;

		printf("编码: ");
		print_str((char *)p, *(p-1));
		p += *(p-1);

		printf("操作类型: 0x%.2x%s\n", *p,
			*p > 0x03 ? oper_type[0] : oper_type[*p]);
		p++;
	}

	free(buf);
	return 0;
}

int
syj_10 (void)
{
	char *buf;
	struct s_10_wrq *s10wh;
	struct s_10_rrq *s10rh;
	uint8_t *p;
	uint64_t *id;
	int len = 0, i, ret;

	printf("\n>>> 数据字典 <<<\n");

	buf = malloc(sizeof(struct rxhdr) + MAXSIZE);
	if (buf == NULL)
		return -1;

	s10wh = (void *)buf;
	s10rh = (void *)buf;
	s10wh->cmd = 0x10;
	s10wh->last_update = timestamp_flag ? 0 : htonl(0x11110010);

	printf("信令编号: 0x%.2x\n", s10wh->cmd);
	printf("最后更新时间: 0x%.8x\n", ntohl(s10wh->last_update));
	printf("发送的数据长度: %ld\n", sizeof(*s10wh));
	syj_put(buf, sizeof(struct s_10_wrq));

	ret = syj_get(buf, &len);
	if (ret == -1) {
		printf("数据字典接收失败！\n");
		return -1;
	}

	printf("接收到数据长度: %d\n", len);
	printf("信令编号: 0x%.2x\n", s10rh->cmd);
	printf("总记录数: %d\n", ntohs(s10rh->record_count));
	printf("最后更新时间: 0x%.8x\n", ntohl(s10rh->last_update));
	p = (void *)s10rh->data;
	for (i=0; i<ntohs(s10rh->record_count); i++) {
		printf("\n--- 第 %d 条数据 ---\n", i+1);
		id = (void *)p;
		printf("ID: %lu (0x%.16lx)\n",
			cvt64(*id), cvt64(*id));
		p += 8;

		printf("名称长度: %d\n", *p);
		p++;

		printf("名称: ");
		print_str((char *)p, *(p-1));
		p += *(p-1);

		id = (void *)p;
		printf("对应值: %lu (0x%.16lx)\n",
			cvt64(*id), cvt64(*id));
		p += 8;

		printf("类别: 0x%.2x%s\n", *p,
			*p == 0x05 ? handle_type[*p] : handle_type[0]);
		p++;

		printf("显示顺序: %d\n", *p);
		p++;

		printf("操作类型: 0x%.2x%s\n", *p,
			*p > 0x03 ? oper_type[0] : oper_type[*p]);
		p++;
	}

	free(buf);
	return 0;
}

int
syj_11 (void)
{
	char *buf;
	struct s_11_wrq *s11wh;
	struct s_11_rrq *s11rh;
	uint8_t *p;
	uint64_t *id;
	int len = 0, i, ret;

	printf("\n>>> 商品类别/证件编号名称对照 <<<\n");

	buf = malloc(sizeof(struct rxhdr) + MAXSIZE);
	if (buf == NULL)
		return -1;

	s11wh = (void *)buf;
	s11rh = (void *)buf;
	s11wh->cmd = 0x11;
	s11wh->last_update = timestamp_flag ? 0 : htonl(0x11110011);

	printf("信令编号: 0x%.2x\n", s11wh->cmd);
	printf("最后更新时间: 0x%.8x\n", ntohl(s11wh->last_update));
	printf("发送的数据长度: %ld\n", sizeof(*s11wh));
	syj_put(buf, sizeof(struct s_11_wrq));

	ret = syj_get(buf, &len);
	if (ret == -1) {
		printf("商品类别/证件编号名称对照接收失败！\n");
		return -1;
	}

	printf("接收到数据长度: %d\n", len);
	printf("信令编号: 0x%.2x\n", s11rh->cmd);
	printf("总记录数: %d\n", ntohs(s11rh->record_count));
	printf("最后更新时间: 0x%.8x\n", ntohl(s11rh->last_update));
	p = (void *)s11rh->data;
	for (i=0; i<ntohs(s11rh->record_count); i++) {
		printf("\n--- 第 %d 条数据 ---\n", i+1);
		id = (void *)p;
		printf("ID: %lu (0x%.16lx)\n",
			cvt64(*id), cvt64(*id));
		p += 8;

		printf("类别编号: ");
		print_str((char *)p, 4);
		p += 4;

		printf("证件编号名称长度: %d\n", *p);
		p++;

		printf("证件编号名称: ");
		print_str((char *)p, *(p-1));
		p += *(p-1);

		printf("操作类型: 0x%.2x%s\n", *p,
			*p > 0x03 ? oper_type[0] : oper_type[*p]);
		p++;
	}

	free(buf);
	return 0;
}

int
syj_12 (void)
{
	char *buf;
	int len = 0, ret;
	int i;
	uint32_t *last_update;
	uint8_t *result;

	struct s_12_rrq *s12rh;
	struct s_12_wrq *s12wh;
	uint32_t s12w[] = {
		0x11110003,
		0x11110004,
		0x11110005,
		0x11110006,
		0x11110007,
		0x11110008,
		0x11110009,
		0x1111000a,
		0x1111000b,
		0x1111000c,
		0x1111000d,
		0x1111000e,
		0x1111000f,
		0x11110010,
		0x11110011,
	};
	char *update_data[] = {
		"[小票信息]",
		"[商家信息]",
		"[销售价格]",
		"[仓库信息]",
		"[员工信息]",
		"[客户信息]",
		"[供应商信息]",
		"[商品基本信息]",
		"[商品单位信息]",
		"[商品批次信息]",
		"[待办任务]",
		"[系统消息]",
		"[商品类别信息]",
		"[数据字典]",
		"[商品类别/证件编号名称对照]",
	};

	printf("\n>>> 差异化同步请求 <<<\n");
	buf = malloc(sizeof(struct rxhdr) + MAXSIZE);
	if (buf == NULL)
		return -1;

	s12wh = (void *)buf;
	s12rh = (void *)buf;

	s12wh->cmd = 0x12;
	s12wh->dept_id = cvt64(DEPT_ID);
	last_update = &s12wh->last_update2;

	int num = sizeof(s12w) / sizeof(s12w[0]);
	for (i=0; i<num; i++) {
		*last_update = htonl(s12w[i]);
		last_update++;
	}

	syj_put(buf, sizeof(struct s_12_wrq));
	ret = syj_get(buf, &len);
	if (ret == -1) {
		printf("差异化同步请求结果接收失败!\n");
		return -1;
	}

	printf("发送的数据长度: %ld\n", sizeof(*s12wh));
	printf("信令编号: 0x%.2x\n", s12wh->cmd);
	for (i=0; i<num; i++) {
		printf("%s: 最后更新时间: 0x%.8x\n",
			update_data[i], s12w[i]);
	}

	result = &s12rh->result1;
	printf("\n接收数据长度: %d\n", len);
	printf("信令编号: 0x%.2x\n", s12rh->cmd);
	for (i=0; i<num; i++) {
		printf("返回的%s: 0x%.2x%s\n",
			update_data[i],
			*result,
			*result ?
			"（需要更新）" :
			"（无需更新）");
		result++;
	}

	free(buf);
	return 0;
}

int
syj_13 (void)
{
	char *buf;
	uint8_t *p;
	int len = 0;
	int i;
	struct s_13_wrq *s13wh;
	uint64_t deldata[] = {
		0x112233445566ULL,
		0x223344556677ULL,
		1234567890ULL,
	};

	struct s_13_wrq_dealdata dealdata[] = {
		{
			0x112233445566ULL,		/* 任务 ID		*/
			0x01,				/* 任务类型		*/
			9,				/* 任务参数长度		*/
			"任务参数1",			/* 任务参数		*/
			"0100001111110220020001",	/* 进货单编号		*/
			0x112233445511ULL,		/* 入库仓库 ID		*/
			9,				/* 入库仓库名称长度	*/
			"入库仓库1",			/* 入库仓库名称		*/
			0x112233445566ULL,		/* 登录人ID		*/
			7,				/* 登录人姓名长度	*/
			"登录人1",			/* 登录人姓名		*/
		},
		{
			0x1357924680ABULL,
			0x02,
			10,
			"任务参数11",
			"0100001111110220020002",	/* 进货单编号		*/
			0x112233445511ULL,		/* 入库仓库 ID		*/
			9,				/* 入库仓库名称长度	*/
			"入库仓库2",			/* 入库仓库名称		*/
			0x112233446666ULL,		/* 登录人ID		*/
			7,				/* 登录人姓名长度	*/
			"登录人2",			/* 登录人姓名		*/
		},
		{
			12345678901234ULL,
			0x03,
			11,
			"任务参数111",
			"0100001111110220020003",	/* 进货单编号		*/
			0x112233445511ULL,		/* 入库仓库 ID		*/
			9,				/* 入库仓库名称长度	*/
			"入库仓库3",			/* 入库仓库名称		*/
			0x112233447766ULL,		/* 登录人ID		*/
			7,				/* 登录人姓名长度	*/
			"登录人3",			/* 登录人姓名		*/
		}
	};

	printf("\n>>> 待办任务信息处理 <<<\n");

	buf = malloc(sizeof(struct rxhdr) + MAXSIZE);
	if (buf == NULL)
		return -1;

	s13wh = (void *)buf;
	s13wh->cmd = 0x13;
	s13wh->dept_id = cvt64(DEPT_ID);
	s13wh->del_count = htons(sizeof(deldata) / sizeof(deldata[0]));
	s13wh->deal_count = htons(sizeof(dealdata) / sizeof(dealdata[0]));

	printf("信令编号: 0x%.2x\n", s13wh->cmd);
	printf("商家 ID: %lu (0x%.16lx)\n",
		cvt64(s13wh->dept_id),
		cvt64(s13wh->dept_id));
	printf("删除记录数: %u\n", ntohs(s13wh->del_count));
	printf("处理记录数: %u\n", ntohs(s13wh->deal_count));
	p = (void *)s13wh->data;

	for (i=0; i<ntohs(s13wh->del_count); i++) {
		printf("\n--- 第 %d 条删除记录 ---\n", i+1);
		printf("任务 ID: %lu (0x%.16lx)\n",
			deldata[i], deldata[i]);
		*((uint64_t *)p) = cvt64(deldata[i]);
		p += sizeof(deldata[i]);
	}

	for (i=0; i<ntohs(s13wh->deal_count); i++) {
		printf("\n--- 第 %d 条处理记录 ---\n", i+1);
		printf("任务 ID: %lu (0x%.16lx)\n",
			dealdata[i].id, dealdata[i].id);
		*((uint64_t *)p) = cvt64(dealdata[i].id);
		p += sizeof(dealdata[i].id);

		printf("任务类型: 0x%.2x, %s\n", dealdata[i].sort,
			dealdata[i].sort > 0x03 ?
			sort_type[0] :
			sort_type[dealdata[i].sort]);
		*p = dealdata[i].sort;
		p += sizeof(dealdata[i].sort);

		printf("任务参数长度: %d\n", dealdata[i].pm_len);
		*p = dealdata[i].pm_len;
		p += sizeof(dealdata[i].pm_len);

		printf("任务参数: %s\n", dealdata[i].pm);
		memcpy(p, dealdata[i].pm, dealdata[i].pm_len);
		p += dealdata[i].pm_len;

		switch (dealdata[i].sort) {
			case 0x01:
				printf("进货单编号: ");
				print_str(dealdata[i].num, sizeof(dealdata[i].num));
				memcpy(p, dealdata[i].num, sizeof(dealdata[i].num));
				p += sizeof(dealdata[i].num);

				printf("入库仓库 ID: %lu (0x%.16lx)\n",
					dealdata[i].depot_id,
					dealdata[i].depot_id);
				*((uint64_t *)p) = cvt64(dealdata[i].depot_id);
				p += sizeof(dealdata[i].depot_id);

				printf("入库仓库名称长度: %d\n", dealdata[i].depot_name_len);
				*p = dealdata[i].depot_name_len;
				p += sizeof(dealdata[i].depot_name_len);

				printf("入库仓库名称: %s\n", dealdata[i].depot_name);
				memcpy(p, dealdata[i].depot_name,
					dealdata[i].depot_name_len);
				p += dealdata[i].depot_name_len;

				printf("登录人 ID: %lu (0x%.16lx)\n",
					dealdata[i].login_user_id,
					dealdata[i].login_user_id);
				*((uint64_t *)p) = cvt64(dealdata[i].login_user_id);
				p += sizeof(dealdata[i].login_user_id);

				printf("登录人姓名长度: %d\n", dealdata[i].name_len);
				*p = dealdata[i].name_len;
				p += sizeof(dealdata[i].name_len);

				printf("登录人姓名: %s\n", dealdata[i].name);
				memcpy(p, dealdata[i].name,
					dealdata[i].name_len);
				p += dealdata[i].name_len;
				break;
			default:
				printf("错误的任务类型: 0x%.2x\n", dealdata[i].sort);
				break;
		}
	}

	len = (void *)p - (void *)s13wh->data;

	printf("发送数据长度: %ld\n", len + sizeof(s13wh->cmd)
		+ sizeof(s13wh->dept_id));
	syj_put(buf, sizeof(struct s_13_wrq) + len);

	free(buf);
	return 0;
}

int
syj_14 (void)
{
	char *buf;
	struct s_14_wrq *s14wh;
	char str[] = "aabbccddee123456789";

	printf("\n>>> 索取商家证请求 <<<\n");

	buf = malloc(sizeof(struct rxhdr) + MAXSIZE);
	if (buf == NULL)
		return -1;

	s14wh = (void *)buf;
	s14wh->cmd = 0x14;
	s14wh->dept_id = cvt64(DEPT_ID);
	s14wh->num_len = strlen(str);
	memcpy(&s14wh->num, str, s14wh->num_len);
	syj_put(buf, s14wh->num_len + 10);

	printf("信令编号: 0x%.2x\n", s14wh->cmd);
	printf("被索商家营业执照号码长度: %d\n", s14wh->num_len);
	printf("被索商家营业执照号码: ");
	print_str((void *)&s14wh->num, s14wh->num_len);
	printf("发送的数据长度: %ld\n", s14wh->num_len + sizeof(s14wh->cmd) +
		sizeof(s14wh->dept_id) + sizeof(s14wh->num_len));

	free(buf);
	return 0;
}

int
syj_15 (void)
{
	char *buf;
	struct s_15_wrq *s15wh;
	uint8_t cmd = 0x15;
	char code[] = "987654321987654321";
	uint8_t code_len = strlen(code);
	uint64_t *id = NULL, num = 0x112233445566ULL;
	int len;

	printf("\n>>> 索取产品证请求 <<<\n");

	buf = malloc(sizeof(struct rxhdr) + MAXSIZE);
	if (buf == NULL)
		return -1;

	s15wh = (void *)buf;
	s15wh->cmd = cmd;
	s15wh->dept_id = cvt64(DEPT_ID);
	s15wh->code_len = code_len;
	memcpy(&s15wh->code, code, code_len);
	id = (void *)&s15wh->code + code_len;
	*id = cvt64(num);

	printf("信令编号: 0x%.2x\n", cmd);
	printf("被索产品证条码长度: %d\n", code_len);
	printf("被索产品证条码: %s\n", code);
	printf("供应商 ID: %lu (0x%.16lx)\n",
		cvt64(*id), cvt64(*id));

	len = sizeof(s15wh->cmd) + sizeof(s15wh->code_len) +
		code_len + sizeof(s15wh->num + sizeof(s15wh->dept_id));

	syj_put(buf, len);

	free(buf);
	return len;
}

int
syj_16 (void)
{
	struct s_16_wrq *s16wh;
	struct s_16_wrq_data *s16data;
	struct s_16_wrq _s16wh1 = {
		0x16,				/* 信令编号			*/
		1,				/* 商家 ID			*/
		"0100001111110220020001",	/* 单据编号			*/
		10,				/* 商家名称长度			*/
		"万年宏超市",			/* 商家名称			*/
		934,				/* 供应商 ID			*/
		22,				/* 供应商名称长度		*/
		"白山市百盛商业有限公司",	/* 供应商名称			*/
		1,				/* 入库仓库 ID			*/
						/* 数据库中可能为空，如果为空传	*/
						/* 递参数的值为零		*/
		9,				/* 入库仓库名称长度		*/
		"存货仓库1",			/* 入库仓库名称			*/
		11,				/* 经手人 ID			*/
		6,				/* 经手人姓名长度		*/
		"高天龙",			/* 经手人姓名			*/
		6,				/* 数量合计			*/
		"0000002000",			/* 应付款金额			*/
		"0000002000",			/* 实际付款金额			*/
		11,				/* 制单人 ID			*/
		6,				/* 制单人姓名长度		*/
		"高天龙",			/* 制单人姓名			*/
		"2011-03-01",			/* 制单日期			*/
		"0000000090",			/* 优惠金额			*/
		"0000080000",			/* 折前金额合计			*/
		"0000079000",			/* 折后金额合计			*/
		3				/* 明细记录数			*/
	};
	struct s_16_wrq_data s16data1[] = {
		{
			441,			/* 商品 ID			*/
			0x01,			/* 是赠品			*/
			1,			/* 数量				*/
			"0000000550",		/* 单价				*/
			691,			/* 单位 ID			*/
			0x01,			/* 保质单位			*/
			90,			/* 保质期: 网络字节序		*/
			0,			/* 生产日期长度			*/
			"",			/* 生产日期			*/
			0,			/* 过期日期长度			*/
			"",			/* 过期日期			*/
			0,			/* 批次长度			*/
			"",			/* 批次				*/
			90,			/* 折扣				*/
		},
		{
			442,
			0x01,
			3,
			"0000000250",
			692,
			0x00,
			0x0000,
			0,
			"",
			0,
			"",
			0,
			"",
			95,			/* 折扣				*/
		},
		{
			443,
			0x01,
			2,
			"0000000350",
			693,
			0x03,
			9,
			0,
			"",
			0,
			"",
			0,
			"",
			80,			/* 折扣				*/
		}
	};

	struct s_16_wrq _s16wh2 = {
		0x16,				/* 信令编号			*/
		2,				/* 商家 ID			*/
		"0100000011110201020002",	/* 单据编号			*/
		10,				/* 商家名称长度			*/
		"万乐福超市",			/* 商家名称			*/
		949,				/* 供应商 ID			*/
		20,				/* 供应商名称长度		*/
		"白山市汇隆食品经销处",		/* 供应商名称			*/
		13,				/* 入库仓库 ID			*/
						/* 数据库中可能为空，如果为空传	*/
						/* 递参数的值为零		*/
		9,				/* 入库仓库名称长度		*/
		"存货仓库1",			/* 入库仓库名称			*/
		2,				/* 经手人 ID			*/
		4,				/* 经手人姓名长度		*/
		"王强",				/* 经手人姓名			*/
		16,				/* 数量合计			*/
		"0000003100",			/* 应付款金额			*/
		"0000083100",			/* 实际付款金额			*/
		2,				/* 制单人 ID			*/
		4,				/* 制单人姓名长度		*/
		"王强",				/* 制单人姓名			*/
		"2011-03-01",			/* 制单日期			*/
		"0000000003",			/* 优惠金额			*/
		"0000080000",			/* 折前金额合计			*/
		"0000079000",			/* 折后金额合计			*/
		3				/* 明细记录数			*/
	};

	struct s_16_wrq_data s16data2[] = {
		{
			224,			/* 商品 ID		*/
			0x02,			/* 是赠品		*/
			10,			/* 数量			*/
			"0000000150",		/* 单价			*/
			224,			/* 单位 ID		*/
			0x01,			/* 保质单位		*/
			90,			/* 保质期: 网络字节序	*/
			0,			/* 生产日期长度		*/
			"",			/* 生产日期		*/
			0,			/* 过期日期长度		*/
			"",			/* 过期日期		*/
			0,			/* 批次长度		*/
			"",			/* 批次			*/
			70,			/* 折扣			*/
		},
		{
			225,
			0x02,
			5,
			"0000000250",
			225,
			0x00,
			0x0000,
			0,
			"",
			0,
			"",
			0,
			"",
			90,			/* 折扣				*/
		},
		{
			226,
			0x02,
			1,
			"0000000350",
			226,
			0x03,
			9,
			0,
			"",
			0,
			"",
			0,
			"",
			60,			/* 折扣				*/
		}
	};

	struct s_16_wrq _s16wh3 = {
		0x16,				 /* 信令编号			*/
		3,				 /* 商家 ID			*/
		"0100003511110229020003",	 /* 单据编号			*/
		8,				 /* 商家名称长度		*/
		"旭日超市",			 /* 商家名称			*/
		929,				 /* 供应商 ID			*/
		14,				 /* 供应商名称长度		*/
		"白山得利斯商贸",		 /* 供应商名称			*/
		7,				 /* 入库仓库 ID			*/
						 /* 数据库中可能为空，如果为空	*/
						 /* 传递参数的值为零		*/
		9,				 /* 入库仓库名称长度		*/
		"存货仓库1",			 /* 入库仓库名称		*/
		15,				 /* 经手人 ID			*/
		6,				 /* 经手人姓名长度		*/
		"葛春波",			 /* 经手人姓名			*/
		5,				 /* 数量合计			*/
		"0000001700",			 /* 应付款金额			*/
		"0000081700",			 /* 实际付款金额		*/
		15,				 /* 制单人 ID			*/
		6,				 /* 制单人姓名长度		*/
		"葛春波",			 /* 制单人姓名			*/
		"2011-03-01",			 /* 制单日期			*/
		"0000003003",			 /* 优惠金额			*/
		"0000080000",			 /* 折前金额合计		*/
		"0000079000",			 /* 折后金额合计		*/
		3				 /* 明细记录数			*/
	};
	struct s_16_wrq_data s16data3[] = {
		{
			701,			 /* 商品 ID		*/
			0x02,			 /* 是赠品		*/
			2,			 /* 数量		*/
			"0000000450",		 /* 单价		*/
			471,			 /* 单位 ID		*/
			0x01,			 /* 保质单位		*/
			9,			 /* 保质期: 网络字节序	*/
			0,			 /* 生产日期长度	*/
			"",			 /* 生产日期		*/
			0,			 /* 过期日期长度	*/
			"",			 /* 过期日期		*/
			10,			 /* 批次长度		*/
			"2011-09-11",		 /* 批次		*/
			90,			 /* 折扣		*/
		},
		{
			702,
			0x02,
			1,
			"0000000550",
			472,
			0x00,
			0x0000,
			10,
			"2010-02-21",
			10,
			"2011-01-01",
			0,
			"",
			90,			/* 折扣				*/
		},
		{
			703,
			0x02,
			1,
			"0000000250",
			473,
			0x03,
			9,
			0,
			"",
			0,
			"",
			0,
			"",
			100,			/* 折扣				*/
		}
	};

	uint8_t num_len;
	uint16_t *quality_date;
	uint32_t *sum_count = NULL, *count = NULL;
	uint64_t *id = NULL;
	char *buf;
	int i, money_len, pay_len, fact_len, len = 0;
	uint8_t attr, is_circulate[] = {3, 2, 1};

	printf("\n>>> 进货单生成 <<<\n");

	if (!memcmp(SYJ_SIM, "000000000000001", 15)) {
		s16wh = &_s16wh1;
		s16data = s16data1;
	} else if (!memcmp(SYJ_SIM, "000000000000002", 15)) {
		s16wh = &_s16wh2;
		s16data = s16data2;
	} else if (!memcmp(SYJ_SIM, "000000000000003", 15)) {
		s16wh = &_s16wh3;
		s16data = s16data3;
	} else {
		printf("SIM 卡 ID 不符合规定，退出！\n");
		return -1;
	}

	buf = malloc(sizeof(struct rxhdr) + MAXSIZE);
	if (buf == NULL)
		return -1;

	printf("\n--- 生成的进货单信息 ---\n");
	printf("信令编号: 0x%.2x\n", s16wh->cmd);
	buf[len] = s16wh->cmd;
	len++;

	id = (void *)buf + len;
	*id = cvt64(DEPT_ID);
	printf("商家 ID: %lu (0x%.16lx)\n",
		cvt64(*id), cvt64(*id));
	len += sizeof(*id);

	num_len = sizeof(s16wh->num);
	memcpy(buf + len, s16wh->num, num_len);
	printf("单据编号: ");
	print_str(buf + len, num_len);
	len += num_len;

	buf[len] = s16wh->dept_name_len;
	printf("商家名称长度: %d\n", s16wh->dept_name_len);
	memcpy(buf + 1 + len, s16wh->dept_name, buf[len]);
	printf("商家名称: ");
	print_str(buf + 1 + len, s16wh->dept_name_len);
	len = len + 1 + s16wh->dept_name_len;

	id = (void *)buf + len;
	*id = cvt64(s16wh->client_id);
	printf("供应商 ID: %lu (0x%.16lx)\n",
		cvt64(*id), cvt64(*id));
	len += sizeof(*id);
	buf[len] = s16wh->client_name_len;
	printf("供应商名称长度: %d\n", s16wh->client_name_len);
	memcpy(buf + len + 1, s16wh->client_name, buf[len]);
	printf("供应商名称: ");
	print_str(buf + len +1, s16wh->client_name_len);
	len = len + 1 + s16wh->client_name_len;

	id = (void *)buf + len;
	*id = cvt64(s16wh->depot_id);
	printf("入库仓库 ID: %lu (0x%.16lx)\n",
		cvt64(*id), cvt64(*id));
	len += sizeof(*id);
	buf[len] = s16wh->depot_name_len;
	printf("入库仓库名称长度: %d\n", buf[len]);
	memcpy(buf + len + 1, s16wh->depot_name, buf[len]);
	printf("入库仓库名称: ");
	print_str(buf + len + 1, s16wh->depot_name_len);
	len = len + 1 + s16wh->depot_name_len;

	id = (void *)buf + len;
	*id = cvt64(s16wh->deal_user_id);
	printf("经手人 ID（员工ID）: %lu (0x%.16lx)\n",
		cvt64(*id), cvt64(*id));
	len += sizeof(*id);
	buf[len] = s16wh->deal_user_name_len;
	printf("经手人姓名长度: %d\n", buf[len]);
	memcpy(buf + len + 1, s16wh->deal_user_name, buf[len]);
	printf("经手人姓名: ");
	print_str(buf + 1 + len, s16wh->deal_user_name_len);
	len = len + 1 + s16wh->deal_user_name_len;

	sum_count = (void *)buf + len;
	*sum_count = htonl(s16wh->sum_count);
	printf("数量合计: %d\n", ntohl(*sum_count));
	len += sizeof(*sum_count);

	pay_len = sizeof(s16wh->pay_money);
	memcpy(buf + len, s16wh->pay_money, pay_len);
	printf("应付款金额: ");
	format_price(buf + len, pay_len);
	len += sizeof(s16wh->pay_money);

	fact_len = sizeof(s16wh->fact_money);
	memcpy(buf + len, s16wh->fact_money, fact_len);
	printf("实际付款金额: ");
	format_price(buf + len, fact_len);
	len += sizeof(s16wh->fact_money);

	id = (void *)buf + len;
	*id = cvt64(s16wh->create_user_id);
	printf("制单人 ID: %lu (0x%.16lx)\n",
		cvt64(*id), cvt64(*id));
	len += sizeof(*id);
	buf[len] = s16wh->create_user_name_len;
	printf("制单人姓名长度: %d\n", buf[len]);
	memcpy(buf + 1 + len, s16wh->create_user_name, s16wh->create_user_name_len);
	printf("制单人姓名: ");
	print_str(buf + 1 + len, s16wh->create_user_name_len);
	len = len + 1 + s16wh->create_user_name_len;

	memcpy(buf + len, s16wh->create_date, 10);
	printf("制单日期: ");
	print_str(buf + len, sizeof(s16wh->create_date));
	len += sizeof(s16wh->create_date);

	memcpy(buf + len, s16wh->fav_money, sizeof(s16wh->fav_money));
	printf("优惠金额: ");
	format_price(buf + len, sizeof(s16wh->fav_money));
	len += sizeof(s16wh->fav_money);

	memcpy(buf + len, s16wh->rebate_front_money, 
		sizeof(s16wh->rebate_front_money));
	printf("折前金额合计: ");
	format_price(buf + len, sizeof(s16wh->rebate_front_money));
	len += sizeof(s16wh->rebate_front_money);

	memcpy(buf + len, s16wh->rebate_back_money, 
		sizeof(s16wh->rebate_back_money));
	printf("折后金额合计: ");
	format_price(buf + len, sizeof(s16wh->rebate_back_money));
	len += sizeof(s16wh->rebate_back_money);

	buf[len] = s16wh->record_count;
	printf("明细记录数: %d\n", buf[len]);
	len += sizeof(s16wh->record_count);

	printf("\n--- 进货单明细 ---\n");
	for (i=0; i<s16wh->record_count; i++) {
		printf("\n--- 第 %d 条数据 ---\n", i+1);
		attr = is_circulate[i] - 1;

		id = (void *)buf + len;
		*id = cvt64(s16data[i].commodity_id);
		printf("商品 ID: %lu (0x%.16lx)\n",
			cvt64(*id), cvt64(*id));
		len += sizeof(*id);

		buf[len] = s16data[i].is_give;
		printf("是否赠品: 0x%.2x%s\n", buf[len], (buf[len] > 0x02) ?
				is_give[0] : is_give[(uint8_t)buf[len]]);
		len += sizeof(buf[len]);

		count = (void *)buf + len;
		*count = htonl(s16data[i].count);
		printf("数量: %d\n", ntohl(*count));
		len += sizeof(*count);

		money_len = sizeof(s16data[i].price);
		memcpy(buf + len, s16data[i].price, money_len);
		printf("单价: ");
		format_price(buf + len, money_len);
		len += sizeof(s16data[i].price);

		id = (void *)buf + len;
		*id = cvt64(s16data[i].unit_id);
		printf("单位 ID: %lu (0x%.16lx)\n",
			cvt64(*id), cvt64(*id));
		len += sizeof(*id);

		buf[len] = s16data[i].quality_unit;
		if (attr & 1) {
			printf("保质单位: 0x%.2x%s\n", buf[len],
				(buf[len] > 0x03) ?
				quality_units[0] :
				quality_units[(uint8_t)buf[len]]);
		}
		len += sizeof(s16data[i].quality_unit);

		quality_date = (void *)buf + len;
		*quality_date = htons(s16data[i].quality_date);
		if (attr & 1) {
			printf("保质期: 0x%.2x\n", ntohs(*quality_date));
		}
		len += sizeof(*quality_date);

		buf[len] = s16data[i].create_date_len;
		if (attr & 1) {
			printf("生产日期长度: %d\n", buf[len]);
		}
		len += sizeof(s16data[i].create_date_len);

		memcpy((void *)buf + len, s16data[i].create_date,
			s16data[i].create_date_len);
		if (attr & 1) {
			printf("生产日期: ");
			print_str((void *)buf + len, s16data[i].create_date_len);
		}
		len += s16data[i].create_date_len;

		buf[len] = s16data[i].exceed_date_len;
		if (attr & 1) {
			printf("过期日期长度: %d\n", buf[len]);
		}
		len += sizeof(s16data[i].exceed_date_len);

		memcpy((void *)buf + len, s16data[i].exceed_date,
			s16data[i].exceed_date_len);
		if (attr & 1) {
			printf("过期日期: ");
			print_str((void *)buf + len, s16data[i].exceed_date_len);
		}
		len += s16data[i].exceed_date_len;

		buf[len] = s16data[i].batch_len;
		if (attr & 2) {
			printf("批次长度: %d\n", buf[len]);
		}
		len += sizeof(s16data[i].batch_len);

		memcpy((void *)buf + len, s16data[i].batch,s16data[i].batch_len);
		if (attr & 2) {
			printf("批次: ");
			print_str((void *)buf + len, s16data[i].batch_len);
		}
		len += s16data[i].batch_len;

		buf[len] = s16data[i].rebate;
		printf("折扣: %d\n",buf[len]);
		len += sizeof(s16data[i].rebate);
	}
	printf("\n发送的数据长度: %d\n", len);

	syj_put(buf, len);

	free(buf);
	return 0;
}

int
syj_17 (void)
{
	struct s_17_wrq *s17wh;
	struct s_17_wrq_data *s17data;
	struct s_17_wrq _s17wh1 = {
		0x17,				 /* 信令编号                     */
		1, 			         /* 商家 ID                      */
		"0100001111110301070001",	 /* 单据编号                     */
		10,                              /* 商家名称长度                 */
		"万年宏超市",                    /* 商家名称                     */
		940,			         /* 客户 ID                      */
		8,                               /* 客户名称名称长度             */
		"永发食品",                      /* 客户名称                     */
		1,			         /* 发货仓库 ID                  */
		9,                               /* 发货仓库名称长度             */
		"存货仓库1",                     /* 发货仓库名称                 */
		12,			         /* 经手人 ID                    */
		6,                               /* 经手人姓名长度               */
		"张明豪",                        /* 经手人姓名                   */
		35,                              /* 数量合计                     */
		"0000012250",                    /* 应付款金额                   */
		"0000012250",                    /* 实际付款金额                 */
		12,				 /* 制单人 ID                    */
		6,                               /* 制单人姓名长度               */
		"张明豪", 	                 /* 制单人姓名                   */
		"2011-03-01",                    /* 制单日期                     */
		"0000000002",			 /* 优惠金额			 */
		"0000011120",			 /* 折前金额合计		 */
		"0000011110",			 /* 折后金额合计		 */
		3                                /* 明细记录数                   */
	};
	struct s_17_wrq_data s17data1[] = {
		{
			441,			 /* 商品 ID              */
			0x02,                    /* 是赠品               */
			10,		         /* 数量                 */
			"0000000550",            /* 单价                 */
			691,			 /* 单位 ID              */
			0x01,                    /* 保质单位             */
			90,                      /* 保质期: 网络字节序   */
			0,                       /* 生产日期长度         */
			"",           		 /* 生产日期             */
			0,                       /* 过期日期长度         */
			"",            		 /* 过期日期             */
			0,                       /* 批次长度             */
			"",        		 /* 批次                 */
			75,
		},
		{
			442,
			0x02,
			20,
			"0000000250",
			692,
			0x00,
			0x0000,
			0,
			"",
			0,
			"",
			0,
			"",
			80,
		},
		{
			443,
			0x02,
			5,
			"0000000350",
			693,
			0x02,
			3,
			0,
			"",
			0,
			"",
			0,
			"",
			90,
		},
	};

	struct s_17_wrq _s17wh2 = {
		0x17,				 /* 信令编号                     */
		2,         			 /* 商家 ID                      */
		"0100000011110301070002",	 /* 单据编号                     */
		10,                              /* 商家名称长度                 */
		"万乐福超市",                    /* 商家名称                     */
		945,				 /* 客户 ID                      */
		8,                               /* 客户名称名称长度             */
		"天顺食品",                      /* 客户名称                     */
		13,				 /* 发货仓库 ID                  */
		9,                               /* 发货仓库名称长度             */
		"存货仓库1",                     /* 发货仓库名称                 */
		5,			         /* 经手人 ID                    */
		6,                               /* 经手人姓名长度               */
		"刘文武",                        /* 经手人姓名                   */
		2,                               /* 数量合计                     */
		"0000014750",                    /* 应付款金额                   */
		"0000014750",                    /* 实际付款金额                 */
		5, 			         /* 制单人 ID                    */
		6,                               /* 制单人姓名长度               */
		"刘文武",	                 /* 制单人姓名                   */
		"2011-03-01",                    /* 制单日期                     */
		"0000000005",			 /* 优惠金额			 */
		"0000011150",			 /* 折前金额合计		 */
		"0000011120",			 /* 折后金额合计		 */
		3                                /* 明细记录数                   */
	};
	struct s_17_wrq_data s17data2[] = {
		{
			224,			 /* 商品 ID              */
			0x02,                    /* 是赠品               */
			20, 		         /* 数量                 */
			"0000000250",            /* 单价                 */
			224,			 /* 单位 ID              */
			0x01,                    /* 保质单位             */
			90,                      /* 保质期: 网络字节序   */
			0,                       /* 生产日期长度         */
			"",            		 /* 生产日期             */
			0,                       /* 过期日期长度         */
			"",            		 /* 过期日期             */
			0,                       /* 批次长度             */
			"",		         /* 批次                 */
			80,
		},
		{
			225,
			0x02,
			25,
			"0000000250",
			225,
			0x00,
			0x0000,
			0,
			"",
			0,
			"",
			0,
			"",
			50,
		},
		{
			226,
			0x2,
			10,
			"0000000350",
			226,
			0x02,
			3,
			0,
			"",
			0,
			"",
			0,
			"",
			80,
		},
	};
	struct s_17_wrq _s17wh3 = {
		0x17,				 /* 信令编号                     */
		3,			         /* 商家 ID                      */
		"0100003511110301070003",	 /* 单据编号                     */
		8,                               /* 商家名称长度                 */
		"旭日超市",                      /* 商家名称                     */
		926,			         /* 客户 ID                      */
		8,                               /* 客户名称名称长度             */
		"合兴超市",                      /* 客户名称                     */
		7,        			 /* 发货仓库 ID                  */
		9,                               /* 发货仓库名称长度             */
		"存货仓库1",                     /* 发货仓库名称                 */
		17,			         /* 经手人 ID                    */
		4,                               /* 经手人姓名长度               */
		"张玲",                          /* 经手人姓名                   */
		20,                              /* 数量合计                     */
		"0000009100",                    /* 应付款金额                   */
		"0000009100",                    /* 实际付款金额                 */
		17,			         /* 制单人 ID                    */
		4,                               /* 制单人姓名长度               */
		"张玲",				 /* 制单人姓名                   */
		"2011-03-01",                    /* 制单日期                     */
		"0000000001",			 /* 优惠金额			 */
		"0000011120",			 /* 折前金额合计		 */
		"0000011110",			 /* 折后金额合计		 */
		3                                /* 明细记录数                   */
	};
	struct s_17_wrq_data s17data3[] = {
		{
			701,   			 /* 商品 ID              */
			0x02,                    /* 是赠品               */
			10,              	 /* 数量                 */
			"0000000450",            /* 单价                 */
			471,   			 /* 单位 ID              */
			0x00,                    /* 保质单位             */
			0x00000,                 /* 保质期: 网络字节序   */
			10,                      /* 生产日期长度         */
			"2010-09-09",		 /* 生产日期             */
			10,                      /* 过期日期长度         */
			"2010-08-08",            /* 过期日期             */
			10,                      /* 批次长度             */
			"2011-10-10",       	 /* 批次                 */
			100,
		},
		{
			702,
			0x02,
			7,
			"0000000550",
			472,
			0x03,
			9,
			0,
			"",
			0,
			"",
			0,
			"",
			70,
		},
		{
			703,
			0x02,
			3,
			"0000000250",
			473,
			0x02,
			3,
			0,
			"",
			0,
			"",
			0,
			"",
			80,
		},
	};
	uint8_t num_len;
	uint16_t *quality_date;
	uint32_t *sum_count = NULL, *count = NULL;
	uint64_t *id = NULL;
	char *buf;
	int i, money_len, pay_len, fact_len, len = 0;
	uint8_t attr, is_circulate[] = {4, 2, 1};
	printf("\n>>> 销售单生成 <<<\n");

	if (!memcmp(SYJ_SIM, "000000000000001", 15)) {
		s17wh = &_s17wh1;
		s17data = s17data1;
	} else if (!memcmp(SYJ_SIM, "000000000000002", 15)) {
		s17wh = &_s17wh2;
		s17data = s17data2;
	} else if (!memcmp(SYJ_SIM, "000000000000003", 15)) {
		s17wh = &_s17wh3;
		s17data = s17data3;
	} else {
		printf("SIM 卡 ID 不符合规定，退出！\n");
		return -1;
	}

	buf = malloc(sizeof(struct rxhdr) + MAXSIZE);
	if (buf == NULL)
		return -1;

	printf("\n--- 生成销售单的信息 ---\n");
	printf("信令编号: 0x%.2x\n", s17wh->cmd);
	buf[len] = s17wh->cmd;
	len++;

	id = (void *)buf + len;
	*id = cvt64(DEPT_ID);
	printf("商家 ID: %lu (0x%.16lx)\n",
		cvt64(*id), cvt64(*id));
	len += sizeof(*id);

	num_len = sizeof(s17wh->num);
	memcpy(buf + len, s17wh->num, num_len);
	printf("单据编号: ");
	print_str(buf + len, num_len);
	len += num_len;

	buf[len] = s17wh->dept_name_len;
	printf("商家名称长度: %d\n", s17wh->dept_name_len);
	memcpy(buf + 1 + len, s17wh->dept_name, buf[len]);
	printf("商家名称: ");
	print_str(buf + 1 + len, s17wh->dept_name_len);
	len = len + 1 + s17wh->dept_name_len;

	id = (void *)buf + len;
	*id = cvt64(s17wh->client_id);
	printf("客户 ID: %lu (0x%.16lx)\n",
		cvt64(*id), cvt64(*id));
	len += sizeof(*id);

	buf[len] = s17wh->client_name_len;
	printf("客户名称长度: %d\n", s17wh->client_name_len);
	memcpy(buf + len + 1, s17wh->client_name, buf[len]);
	printf("客户名称: ");
	print_str(buf + len +1, s17wh->client_name_len);
	len = len + 1 + s17wh->client_name_len;

	id = (void *)buf + len;
	*id = cvt64(s17wh->depot_id);
	printf("发货仓库 ID: %lu (0x%.16lx)\n",
		cvt64(*id), cvt64(*id));
	len += sizeof(*id);

	buf[len] = s17wh->depot_name_len;
	printf("发货仓库名称长度: %d\n", buf[len]);
	memcpy(buf + len + 1, s17wh->depot_name, buf[len]);
	printf("发货仓库名称: ");
	print_str(buf + len + 1, s17wh->depot_name_len);
	len = len + 1 + s17wh->depot_name_len;

	id = (void *)buf + len;
	*id = cvt64(s17wh->deal_user_id);
	printf("经手人 ID（员工ID）: %lu (0x%.16lx)\n",
		cvt64(*id), cvt64(*id));
	len += sizeof(*id);

	buf[len] = s17wh->deal_user_name_len;
	printf("经手人姓名长度: %d\n", buf[len]);
	memcpy(buf + len + 1, s17wh->deal_user_name, buf[len]);
	printf("经手人姓名: ");
	print_str(buf + 1 + len, s17wh->deal_user_name_len);
	len = len + 1 + s17wh->deal_user_name_len;

	sum_count = (void *)buf + len;
	*sum_count = htonl(s17wh->sum_count);
	printf("数量合计: %d\n", ntohl(*sum_count));
	len += sizeof(*sum_count);

	pay_len = sizeof(s17wh->pay_money);
	memcpy(buf + len, s17wh->pay_money, pay_len);
	printf("应付款金额: ");
	format_price(buf + len, pay_len);
	len += sizeof(s17wh->pay_money);

	fact_len = sizeof(s17wh->fact_money);
	memcpy(buf + len, s17wh->fact_money, fact_len);
	printf("实际付款金额: ");
	format_price(buf + len, fact_len);
	len += sizeof(s17wh->fact_money);

	id = (void *)buf + len;
	*id = cvt64(s17wh->create_user_id);
	printf("制单人 ID: %lu (0x%.16lx)\n",
		cvt64(*id), cvt64(*id));
	len += sizeof(*id);

	buf[len] = s17wh->create_user_name_len;
	printf("制单人姓名长度: %d\n", buf[len]);
	memcpy(buf + 1 + len, s17wh->create_user_name, s17wh->create_user_name_len);
	printf("制单人姓名: ");
	print_str(buf + 1 + len, s17wh->create_user_name_len);
	len = len + 1 + s17wh->create_user_name_len;

	memcpy(buf + len, s17wh->create_date, sizeof(s17wh->create_date));
	printf("制单日期: ");
	print_str(buf + len, sizeof(s17wh->create_date));
	len += sizeof(s17wh->create_date);

	memcpy(buf + len, s17wh->fav_money, sizeof(s17wh->fav_money));
	printf("优惠金额: ");
	format_price(buf + len, sizeof(s17wh->fav_money));
	len += sizeof(s17wh->fav_money);

	memcpy(buf + len, s17wh->rebate_front_money, 
		sizeof(s17wh->rebate_front_money));
	printf("折前金额合计: ");
	format_price(buf + len, sizeof(s17wh->rebate_front_money));
	len += sizeof(s17wh->rebate_front_money);

	memcpy(buf + len, s17wh->rebate_back_money, 
		sizeof(s17wh->rebate_back_money));
	printf("折后金额合计: ");
	format_price(buf + len, sizeof(s17wh->rebate_back_money));
	len += sizeof(s17wh->rebate_back_money);

	buf[len] = s17wh->record_count;
	printf("明细记录数: %d\n", buf[len]);
	len += sizeof(s17wh->record_count);

	printf("\n--- 销售单明细 ---\n");
	for (i=0; i<s17wh->record_count; i++) {
		printf("\n--- 第 %d 条数据 ---\n", i+1);
		attr = is_circulate[i] - 1;

		id = (void *)buf + len;
		*id = cvt64(s17data[i].commodity_id);
		printf("商品 ID: %lu (0x%.16lx)\n",
			cvt64(*id), cvt64(*id));
		len += sizeof(*id);

		buf[len] = s17data[i].is_give;
		printf("是否赠品: 0x%.2x%s\n", buf[len], (buf[len] > 0x02) ?
				is_give[0] : is_give[(uint8_t)buf[len]]);
		len += sizeof(buf[len]);

		count = (void *)buf + len;
		*count = htonl(s17data[i].count);
		printf("数量: %d\n", ntohl(*count));
		len += sizeof(*count);

		money_len = sizeof(s17data[i].price);
		memcpy(buf + len, s17data[i].price, money_len);
		printf("单价: ");
		format_price(buf + len, money_len);
		len += sizeof(s17data[i].price);

		id = (void *)buf + len;
		*id = cvt64(s17data[i].unit_id);
		printf("单位 ID: %lu (0x%.16lx)\n",
			cvt64(*id), cvt64(*id));
		len += sizeof(*id);

		buf[len] = s17data[i].quality_unit;
		if (attr & 1) {
			printf("保质单位: 0x%.2x%s\n", buf[len],
				(buf[len] > 0x03) ?
				quality_units[0] :
				quality_units[(uint8_t)buf[len]]);
		}
		len += sizeof(s17data[i].quality_unit);

		quality_date = (void *)buf + len;
		*quality_date = htons(s17data[i].quality_date);
		if (attr & 1) {
			printf("保质期: 0x%.2x\n", ntohs(*quality_date));
		}
		len += sizeof(*quality_date);

		buf[len] = s17data[i].create_date_len;
		if (attr & 1) {
			printf("生产日期长度: %d\n", buf[len]);
		}
		len += sizeof(s17data[i].create_date_len);

		memcpy((void *)buf + len, s17data[i].create_date,
			s17data[i].create_date_len);
		if (attr & 1) {
			printf("生产日期: ");
			print_str((void *)buf + len, s17data[i].create_date_len);
		}
		len += s17data[i].create_date_len;

		buf[len] = s17data[i].exceed_date_len;
		if (attr & 1) {
			printf("过期日期长度: %d\n", buf[len]);
		}
		len += sizeof(s17data[i].exceed_date_len);

		memcpy((void *)buf + len, s17data[i].exceed_date,
			s17data[i].exceed_date_len);
		if (attr & 1) {
			printf("过期日期: ");
			print_str((void *)buf + len, s17data[i].exceed_date_len);
		}
		len += s17data[i].exceed_date_len;

		buf[len] = s17data[i].batch_len;
		if (attr & 2) {
			printf("批次长度: %d\n", buf[len]);
		}
		len += sizeof(s17data[i].batch_len);

		memcpy((void *)buf + len, s17data[i].batch,s17data[i].batch_len);
		if (attr & 2) {
			printf("批次: ");
			print_str((void *)buf + len, s17data[i].batch_len);
		}
		len += s17data[i].batch_len;

		buf[len] = s17data[i].rebate;
		printf("折扣: %d\n",buf[len]);
		len += sizeof(s17data[i].rebate);
	}

	printf("\n发送的数据长度: %d\n", len);

	syj_put(buf, len);

	free(buf);
	return 0;
}

int
syj_18 (void)
{
	struct s_18_wrq *s18wh;
	struct s_18_wrq_data *s18data;
	struct s_18_wrq _s18wh = {
		0x18,				 /* 信令编号                     */
		3,			         /* 商家 ID                      */
		"0100003511030107000388",	 /* 单据编号                     */
		8,                               /* 商家名称长度                 */
		"旭日超市",                      /* 商家名称                     */
		8,        			 /* 出货仓库 ID                  */
		9,                               /* 出货仓库名称长度             */
		"出货仓库1",                     /* 出货仓库名称                 */
		7,        			 /* 入货仓库 ID                  */
		9,                               /* 入货仓库名称长度             */
		"入货仓库1",                     /* 入货仓库名称                 */
		17,			         /* 经手人 ID                    */
		4,                               /* 经手人姓名长度               */
		"张玲",                          /* 经手人姓名                   */
		20,                              /* 出数量合计                   */
		17,			         /* 制单人 ID                    */
		4,                               /* 制单人姓名长度               */
		"张玲",				 /* 制单人姓名                   */
		"2011-03-01",                    /* 制单日期                     */
		3                                /* 明细记录数                   */
	};
	struct s_18_wrq_data _s18data[] = {
		{
			701,   			 /* 商品 ID              */
			10,              	 /* 数量                 */
			0x01,                    /* 保质单位             */
			10,                      /* 保质期: 网络字节序   */
			10,                      /* 生产日期长度         */
			"2010-09-09",		 /* 生产日期             */
			10,                      /* 过期日期长度         */
			"2010-08-08",            /* 过期日期             */
			10,                      /* 批次长度             */
			"2011-10-10"        	 /* 批次                 */
		},
		{
			702,
			7,
			0x03,
			9,
			10,
			"2011-11-11",
			10,
			"2011-12-12",
			10,
			"2011-11-11"
		},
		{
			703,
			3,
			0x02,
			3,
			10,
			"2013-12-13",
			10,
			"2013-12-30",
			10,
			"2013-12-13"
		},
	};
	uint8_t num_len;
	uint16_t *quality_date;
	uint32_t *sum_count = NULL, *count = NULL;
	uint64_t *id = NULL;
	char *buf;
	int i, len = 0;
	uint8_t attr, is_circulate[] = {4, 4, 4};
	printf("\n>>> 退市单生成 <<<\n");

	s18wh = &_s18wh;
	s18data = _s18data;

	buf = malloc(sizeof(struct rxhdr) + MAXSIZE);
	if (buf == NULL)
		return -1;

	printf("\n--- 退市单的信息 ---\n");
	printf("信令编号: 0x%.2x\n", s18wh->cmd);
	buf[len] = s18wh->cmd;
	len++;

	id = (void *)buf + len;
	*id = cvt64(DEPT_ID);
	printf("商家 ID: %lu (0x%.16lx)\n",
		cvt64(*id), cvt64(*id));
	len += sizeof(*id);

	num_len = sizeof(s18wh->num);
	memcpy(buf + len, s18wh->num, num_len);
	printf("单据编号: ");
	print_str(buf + len, num_len);
	len += num_len;

	buf[len] = s18wh->dept_name_len;
	printf("商家名称长度: %d\n", s18wh->dept_name_len);
	memcpy(buf + 1 + len, s18wh->dept_name, buf[len]);
	printf("商家名称: ");
	print_str(buf + 1 + len, s18wh->dept_name_len);
	len = len + 1 + s18wh->dept_name_len;

	id = (void *)buf + len;
	*id = cvt64(s18wh->out_depot_id);
	printf("出货仓库 ID: %lu (0x%.16lx)\n",
		cvt64(*id), cvt64(*id));
	len += sizeof(*id);
	buf[len] = s18wh->out_depot_name_len;
	printf("出货仓库名称长度: %d\n", buf[len]);
	memcpy(buf + len + 1, s18wh->out_depot_name, buf[len]);
	printf("出货仓库名称: ");
	print_str(buf + len + 1, s18wh->out_depot_name_len);
	len = len + 1 + s18wh->out_depot_name_len;

	id = (void *)buf + len;
	*id = cvt64(s18wh->in_depot_id);
	printf("入货仓库 ID: %lu (0x%.16lx)\n",
		cvt64(*id), cvt64(*id));
	len += sizeof(*id);
	buf[len] = s18wh->in_depot_name_len;
	printf("入货仓库名称长度: %d\n", buf[len]);
	memcpy(buf + len + 1, s18wh->in_depot_name, buf[len]);
	printf("入货仓库名称: ");
	print_str(buf + len + 1, s18wh->in_depot_name_len);
	len = len + 1 + s18wh->in_depot_name_len;

	id = (void *)buf + len;
	*id = cvt64(s18wh->deal_user_id);
	printf("经手人（员工）ID: %lu (0x%.16lx)\n",
		cvt64(*id), cvt64(*id));
	len += sizeof(*id);

	buf[len] = s18wh->deal_user_name_len;
	printf("经手人姓名长度: %d\n", buf[len]);
	memcpy(buf + len + 1, s18wh->deal_user_name, buf[len]);
	printf("经手人姓名: ");
	print_str(buf + 1 + len, s18wh->deal_user_name_len);
	len = len + 1 + s18wh->deal_user_name_len;

	sum_count = (void *)buf + len;
	*sum_count = htonl(s18wh->sum_count);
	printf("退市数量合计: %d\n", ntohl(*sum_count));
	len += sizeof(*sum_count);

	id = (void *)buf + len;
	*id = cvt64(s18wh->create_user_id);
	printf("制单人 ID: %lu (0x%.16lx)\n",
		cvt64(*id), cvt64(*id));
	len += sizeof(*id);

	buf[len] = s18wh->create_user_name_len;
	printf("制单人姓名长度: %d\n", buf[len]);
	memcpy(buf + 1 + len, s18wh->create_user_name, s18wh->create_user_name_len);
	printf("制单人姓名: ");
	print_str(buf + 1 + len, s18wh->create_user_name_len);
	len = len + 1 + s18wh->create_user_name_len;

	memcpy(buf + len, s18wh->create_date, sizeof(s18wh->create_date));
	printf("制单日期: ");
	print_str(buf + len, sizeof(s18wh->create_date));
	len += sizeof(s18wh->create_date);

	buf[len] = s18wh->record_count;
	printf("明细记录数: %d\n", buf[len]);
	len += sizeof(s18wh->record_count);

	printf("\n>>> 退市单生成 <<<\n");
	for (i=0; i<s18wh->record_count; i++) {
		printf("\n--- 第 %d 条数据 ---\n", i+1);
		attr = is_circulate[i] - 1;

		id = (void *)buf + len;
		*id = cvt64(s18data[i].commodity_id);
		printf("商品 ID: %lu (0x%.16lx)\n",
			cvt64(*id), cvt64(*id));
		len += sizeof(*id);

		count = (void *)buf + len;
		*count = htonl(s18data[i].count);
		printf("退市数量: %d\n", ntohl(*count));
		len += sizeof(*count);

		buf[len] = s18data[i].quality_unit;
		if (attr & 1) {
			printf("保质单位: 0x%.2x%s\n", buf[len],
				(buf[len] > 0x03) ?
				quality_units[0] :
				quality_units[(uint8_t)buf[len]]);
		}
		len += sizeof(s18data[i].quality_unit);

		quality_date = (void *)buf + len;
		*quality_date = htons(s18data[i].quality_date);
		if (attr & 1) {
			printf("保质期: %d\n", ntohs(*quality_date));
		}
		len += sizeof(*quality_date);

		buf[len] = s18data[i].create_date_len;
		if (attr & 1) {
			printf("生产日期长度: %d\n", buf[len]);
		}
		len += sizeof(s18data[i].create_date_len);

		memcpy((void *)buf + len, s18data[i].create_date,
			s18data[i].create_date_len);
		if (attr & 1) {
			printf("生产日期: ");
			print_str((void *)buf + len, s18data[i].create_date_len);
		}
		len += s18data[i].create_date_len;

		buf[len] = s18data[i].exceed_date_len;
		if (attr & 1) {
			printf("过期日期长度: %d\n", buf[len]);
		}
		len += sizeof(s18data[i].exceed_date_len);

		memcpy((void *)buf + len, s18data[i].exceed_date,
			s18data[i].exceed_date_len);
		if (attr & 1) {
			printf("过期日期: ");
			print_str((void *)buf + len, s18data[i].exceed_date_len);
		}
		len += s18data[i].exceed_date_len;

		buf[len] = s18data[i].batch_len;
		if (attr & 2) {
			printf("批次长度: %d\n", buf[len]);
		}
		len += sizeof(s18data[i].batch_len);

		memcpy((void *)buf + len, s18data[i].batch,s18data[i].batch_len);
		if (attr & 2) {
			printf("批次: ");
			print_str((void *)buf + len, s18data[i].batch_len);
		}
		len += s18data[i].batch_len;
	}

	printf("\n发送的数据长度: %d\n", len);

	syj_put(buf, len);

	free(buf);
	return 0;
}

int
syj_19 (void)
{
	struct s_19_wrq *s19wh;
	struct s_19_wrq_data *s19data;
	struct s_19_wrq _s19wh = {
		0x19,				/* 信令编号                     */
		3,				/* 商家 ID                      */
		"0100003511030107000399",	/* 单据编号                     */
		8,				/* 商家名称长度                 */
		"旭日超市",			/* 商家名称                     */
		8,				/* 禁售仓库 ID                  */
		9,				/* 禁售仓库名称长度             */
		"出货仓库1",			/* 禁售仓库名称                 */
		17,				/* 经手人 ID                    */
		4,				/* 经手人姓名长度               */
		"张玲",				/* 经手人姓名                   */
		1,				/* 处理方式			 */
		20,				/* 出数量合计                   */
		17,				/* 制单人 ID                    */
		4,				/* 制单人姓名长度               */
		"张玲",				/* 制单人姓名                   */
		"2011-03-01",			/* 制单日期                     */
		16,				/* 处理方式描述长度		*/
		"处理方式描述测试",		/* 处理方式描述			*/
		3,				/* 明细记录数                   */
	};
	struct s_19_wrq_data _s19data[] = {
		{
			701,   			 /* 商品 ID              */
			10,              	 /* 处理数量             */
			0x01,                    /* 保质单位             */
			10,                      /* 保质期: 网络字节序   */
			10,                      /* 生产日期长度         */
			"2010-09-09",		 /* 生产日期             */
			10,                      /* 过期日期长度         */
			"2010-08-08",            /* 过期日期             */
			10,                      /* 批次长度             */
			"2011-10-10"        	 /* 批次                 */
		},
		{
			702,
			12,
			0x03,
			9,
			10,
			"2011-11-11",
			10,
			"2011-12-12",
			10,
			"2011-11-11"
		},
		{
			703,
			13,
			0x02,
			3,
			10,
			"2013-12-13",
			10,
			"2013-12-30",
			10,
			"2013-12-13"
		},
	};
	uint8_t num_len;
	uint16_t *quality_date;
	uint32_t *sum_count = NULL, *count = NULL;
	uint64_t *id = NULL;
	char *buf;
	int i, len = 0;
	printf("\n>>> 退市处理单生成 <<<\n");

	s19wh = &_s19wh;
	s19data = _s19data;

	buf = malloc(sizeof(struct rxhdr) + MAXSIZE);
	if (buf == NULL)
		return -1;

	printf("\n--- 退市处理单的信息 ---\n");
	printf("信令编号: 0x%.2x\n", s19wh->cmd);
	buf[len] = s19wh->cmd;
	len++;

	id = (void *)buf + len;
	*id = cvt64(DEPT_ID);
	printf("商家 ID: %lu (0x%.16lx)\n",
		cvt64(*id), cvt64(*id));
	len += sizeof(*id);

	num_len = sizeof(s19wh->num);
	memcpy(buf + len, s19wh->num, num_len);
	printf("单据编号: ");
	print_str(buf + len, num_len);
	len += num_len;

	buf[len] = s19wh->dept_name_len;
	printf("商家名称长度: %d\n", s19wh->dept_name_len);
	memcpy(buf + 1 + len, s19wh->dept_name, buf[len]);
	printf("商家名称: ");
	print_str(buf + 1 + len, s19wh->dept_name_len);
	len = len + 1 + s19wh->dept_name_len;

	id = (void *)buf + len;
	*id = cvt64(s19wh->depot_id);
	printf("禁售仓库 ID: %lu (0x%.16lx)\n",
		cvt64(*id), cvt64(*id));
	len += sizeof(*id);
	buf[len] = s19wh->depot_name_len;
	printf("禁售仓库名称长度: %d\n", buf[len]);
	memcpy(buf + len + 1, s19wh->depot_name, buf[len]);
	printf("禁售仓库名称: ");
	print_str(buf + len + 1, s19wh->depot_name_len);
	len = len + 1 + s19wh->depot_name_len;

	id = (void *)buf + len;
	*id = cvt64(s19wh->deal_user_id);
	printf("经手人 ID（员工ID）: %lu (0x%.16lx)\n",
		cvt64(*id), cvt64(*id));
	len += sizeof(*id);

	buf[len] = s19wh->deal_user_name_len;
	printf("经手人姓名长度: %d\n", buf[len]);
	memcpy(buf + len + 1, s19wh->deal_user_name, buf[len]);
	printf("经手人姓名: ");
	print_str(buf + 1 + len, s19wh->deal_user_name_len);
	len = len + 1 + s19wh->deal_user_name_len;

	printf("处理方式: 0x%.2x%s\n", s19wh->deal_way,
		(buf[len] > 0x05) ?
		deal_way[0]:deal_way[(uint8_t)s19wh->deal_way]);
	buf[len] = s19wh->deal_way;
	len += sizeof(s19wh->deal_way);

	sum_count = (void *)buf + len;
	*sum_count = htonl(s19wh->sum_count);
	printf("退市数量合计: %d\n", ntohl(*sum_count));
	len += sizeof(*sum_count);

	id = (void *)buf + len;
	*id = cvt64(s19wh->create_user_id);
	printf("制单人 ID: %lu (0x%.16lx)\n",
		cvt64(*id), cvt64(*id));
	len += sizeof(*id);

	buf[len] = s19wh->create_user_name_len;
	printf("制单人姓名长度: %d\n", buf[len]);
	memcpy(buf + 1 + len, s19wh->create_user_name, s19wh->create_user_name_len);
	printf("制单人姓名: ");
	print_str(buf + 1 + len, s19wh->create_user_name_len);
	len = len + 1 + s19wh->create_user_name_len;

	memcpy(buf + len, s19wh->create_date, sizeof(s19wh->create_date));
	printf("制单日期: ");
	print_str(buf + len, sizeof(s19wh->create_date));
	len += sizeof(s19wh->create_date);

	buf[len] = s19wh->describe_len;
	printf("处理方式描述长度: %d\n", buf[len]);
	memcpy(buf + 1 + len, s19wh->describe, s19wh->describe_len);
	printf("处理方式描述: ");
	print_str(buf + 1 + len, s19wh->describe_len);
	len = len + 1 + s19wh->describe_len;

	buf[len] = s19wh->record_count;
	printf("明细记录数: %d\n", buf[len]);
	len += sizeof(s19wh->record_count);

	printf("\n>>> 退市处理单明细 <<<\n");
	for (i=0; i<s19wh->record_count; i++) {
		printf("\n--- 第 %d 条数据 ---\n", i+1);

		id = (void *)buf + len;
		*id = cvt64(s19data[i].commodity_id);
		printf("商品 ID: %lu (0x%.16lx)\n",
			cvt64(*id), cvt64(*id));
		len += sizeof(*id);

		count = (void *)buf + len;
		*count = htonl(s19data[i].deal_count);
		printf("处理数量: %d\n", ntohl(*count));
		len += sizeof(*count);

		buf[len] = s19data[i].quality_unit;
		printf("保质单位: 0x%.2x%s\n", buf[len],
			(buf[len] > 0x03) ?
			quality_units[0] :
			quality_units[(uint8_t)buf[len]]);
		len += sizeof(s19data[i].quality_unit);

		quality_date = (void *)buf + len;
		*quality_date = htons(s19data[i].quality_date);
		printf("保质期: %d\n", ntohs(*quality_date));
		len += sizeof(*quality_date);

		buf[len] = s19data[i].create_date_len;
		printf("生产日期长度: %d\n", buf[len]);
		len += sizeof(s19data[i].create_date_len);

		memcpy((void *)buf + len, s19data[i].create_date,
			s19data[i].create_date_len);
		printf("生产日期: ");
		print_str((void *)buf + len, s19data[i].create_date_len);
		len += s19data[i].create_date_len;

		buf[len] = s19data[i].exceed_date_len;
		printf("过期日期长度: %d\n", buf[len]);
		len += sizeof(s19data[i].exceed_date_len);

		memcpy((void *)buf + len, s19data[i].exceed_date,
			s19data[i].exceed_date_len);
		printf("过期日期: ");
		print_str((void *)buf + len, s19data[i].exceed_date_len);
		len += s19data[i].exceed_date_len;

		buf[len] = s19data[i].batch_len;
		printf("批次长度: %d\n", buf[len]);
		len += sizeof(s19data[i].batch_len);

		memcpy((void *)buf + len, s19data[i].batch,s19data[i].batch_len);
		printf("批次: ");
		print_str((void *)buf + len, s19data[i].batch_len);
		len += s19data[i].batch_len;
	}

	printf("\n发送的数据长度: %d\n", len);

	syj_put(buf, len);

	free(buf);
	return 0;
}

int
syj_1a (void)
{
	char *buf;
	int i;
	char *p;
	int len;

	struct s_1a_wrq *s1awh;
	struct s_1a_wrq_data wrq_data[] = {
		{
			1234567890ULL,		/* 商品 ID		*/
			9876543210ULL,		/* 基础商品 ID		*/
			7,			/* 商品条码长度		*/
			"ABCDEFG",		/* 商品条码		*/
			11,			/* 商品名称长度		*/
			"DELL 笔记本",		/* 商品名称		*/
			2,			/* 基本单位名称长度	*/
			"台",			/* 基本单位名称		*/
			1234UL,			/* 商品类别 ID		*/
			3,			/* 类别编码长度		*/
			"XYZ",			/* 类别编码		*/
			8,			/* 规格长度		*/
			"Inspiron",		/* 规格			*/
			"0000500000",		/* 进货参考价		*/
			"0000600000",		/* 零售价		*/
			"0000580000",		/* 会员价		*/
			"0000550000",		/* 经销商价		*/
			10,			/* 证件编号长度		*/
			"1234567890",		/* 证件编号		*/
			0x02,			/* 操作类型		*/
		},
		{
			0ULL,			/* 商品 ID		*/
			5544332211ULL,		/* 基础商品 ID		*/
			8,			/* 商品条码长度		*/
			"fdsafdsa",		/* 商品条码		*/
			12,			/* 商品名称长度		*/
			"自增添加测试",		/* 商品名称		*/
			2,			/* 基本单位名称长度	*/
			"箱",			/* 基本单位名称		*/
			2345UL,			/* 商品类别 ID		*/
			3,			/* 类别编码长度		*/
			"abc",			/* 类别编码		*/
			4,			/* 规格长度		*/
			"5110",			/* 规格			*/
			"1234567890",		/* 进货参考价		*/
			"0987654321",		/* 零售价		*/
			"9876543210",		/* 会员价		*/
			"1357924680",		/* 经销商价		*/
			7,			/* 证件编号长度		*/
			"aBcDeFg",		/* 证件编号		*/
			0x01,			/* 操作类型		*/
		},
	};

	printf("\n>>> 商品管理请求 <<<\n");

	buf = malloc(sizeof(struct rxhdr) + MAXSIZE);
	if (buf == NULL)
		return -1;

	s1awh = (void *)buf;
	s1awh->cmd = 0x1a;
	s1awh->dept_id = cvt64(DEPT_ID);
	s1awh->record_count = htons(sizeof(wrq_data) / sizeof(wrq_data[0]));

	printf("\n--- 商品管理请求 ---\n");
	printf("信令编号: 0x%.2x\n", s1awh->cmd);
	printf("商家 ID: %lu (0x%.16lx)\n",
		cvt64(s1awh->dept_id),
		cvt64(s1awh->dept_id));
	printf("总记录数: %u\n", ntohs(s1awh->record_count));
	p = (void *)s1awh->data;

	for (i=0; i<ntohs(s1awh->record_count); i++) {
		printf("\n--- 第 %d 条数据 ---\n", i+1);
		*((uint64_t *)p) = cvt64(wrq_data[i].id);
		printf("商品 ID: %lu (0x%.16lx)\n",
			cvt64(*((uint64_t *)p)),
			cvt64(*((uint64_t *)p)));
		p += 8;

		*((uint64_t *)p) = cvt64(wrq_data[i].commondity_id);
		printf("基础商品 ID: %lu (0x%.16lx)\n",
			cvt64(*((uint64_t *)p)),
			cvt64(*((uint64_t *)p)));
		p += 8;

		*p = wrq_data[i].code_len;
		printf("商品条码长度: %d\n", *p);
		p++;

		memcpy(p, wrq_data[i].code, wrq_data[i].code_len);
		printf("商品条码: ");
		print_str(p, *(p-1));
		p += *(p-1);

		*p = wrq_data[i].name_len;
		printf("商品名称长度: %d\n", *p);
		p++;

		memcpy(p, wrq_data[i].name, wrq_data[i].name_len);
		printf("商品名称: ");
		print_str(p, *(p-1));
		p += *(p-1);

		*p = wrq_data[i].base_unit_name_len;
		printf("基本单位名称长度: %d\n", *p);
		p++;

		memcpy(p, wrq_data[i].base_unit_name, wrq_data[i].base_unit_name_len);
		printf("基本单位名称: ");
		print_str(p, *(p-1));
		p += *(p-1);

		*((uint64_t *)p) = cvt64(wrq_data[i].category_id);
		printf("商品类别 ID: %lu (0x%.16lx)\n",
			cvt64(*((uint64_t *)p)),
			cvt64(*((uint64_t *)p)));
		p += 8;

		*p = wrq_data[i].category_len;
		printf("类别编码长度: %d\n", *p);
		p++;

		memcpy(p, wrq_data[i].category_code, wrq_data[i].category_len);
		printf("类别编码: ");
		print_str(p, *(p-1));
		p += *(p-1);

		*p = wrq_data[i].spec_len;
		printf("规格长度: %d\n", *p);
		p++;

		memcpy(p, wrq_data[i].spec, wrq_data[i].spec_len);
		printf("规格: ");
		print_str(p, *(p-1));
		p += *(p-1);

		memcpy(p, wrq_data[i].price, 10);
		printf("进货参考价: ");
		format_price(p, 10);
		p += 10;

		memcpy(p, wrq_data[i].retail_price, 10);
		printf("零售价: ");
		format_price(p, 10);
		p += 10;

		memcpy(p, wrq_data[i].vip_price, 10);
		printf("会员价: ");
		format_price(p, 10);
		p += 10;

		memcpy(p, wrq_data[i].dealer_price, 10);
		printf("经销商价: ");
		format_price(p, 10);
		p += 10;

		*p = wrq_data[i].cert_code_len;
		printf("证件编号长度: %d\n", *p);
		p++;

		memcpy(p, wrq_data[i].cert_code, wrq_data[i].cert_code_len);
		printf("证件编号: ");
		print_str(p, *(p-1));
		p += *(p-1);

		*p = wrq_data[i].oper_type;
		printf("操作类型: 0x%.2x%s\n", *p,
			(*p > 0x03) ?
			oper_type[0] :
			oper_type[(uint8_t)*p]);
		p++;
	}
	len = (void *)p - (void *)buf;
	printf("\n发送的数据长度: %d\n", len);
	syj_put(buf, len);

	free(buf);
	return 0;
}

int
syj_1b (void)
{
	char *buf;
	int i;
	char *p;
	int len;

	struct s_1b_wrq *s1bwh;
	struct s_1b_wrq_data wrq_data[] = {
		{
			1234567890ULL,		/* 商品 ID		*/
			7,			/* 条码长度		*/
			"ABCDEFG",		/* 条码			*/
			2,			/* 单位名称长度		*/
			"台",			/* 单位名称		*/
			800,			/* 单位关系		*/
			"0000500000",		/* 进货参考价		*/
			"0000600000",		/* 零售价		*/
			"0000580000",		/* 会员价		*/
			"0000550000",		/* 经销商价		*/
			0x02,			/* 操作类型		*/
		},
		{
			0ULL,			/* 商品 ID		*/
			8,			/* 条码长度		*/
			"fdsafdsa",		/* 条码			*/
			2,			/* 单位名称长度		*/
			"箱",			/* 单位名称		*/
			500,			/* 单位关系		*/
			"1234567890",		/* 进货参考价		*/
			"0987654321",		/* 零售价		*/
			"9876543210",		/* 会员价		*/
			"1357924680",		/* 经销商价		*/
			0x01,			/* 操作类型		*/
		},
	};

	printf("\n>>> 商品单位管理请求 <<<\n");

	buf = malloc(sizeof(struct rxhdr) + MAXSIZE);
	if (buf == NULL)
		return -1;

	s1bwh = (void *)buf;
	s1bwh->cmd = 0x1b;
	s1bwh->dept_id = cvt64(DEPT_ID);
	s1bwh->record_count = htons(sizeof(wrq_data) / sizeof(wrq_data[0]));

	printf("\n--- 商品单位管理请求 ---\n");
	printf("信令编号: 0x%.2x\n", s1bwh->cmd);
	printf("商家 ID: %lu (0x%.16lx)\n",
		cvt64(s1bwh->dept_id),
		cvt64(s1bwh->dept_id));
	printf("总记录数: %u\n", ntohs(s1bwh->record_count));
	p = (void *)s1bwh->data;

	for (i=0; i<ntohs(s1bwh->record_count); i++) {
		printf("\n--- 第 %d 条数据 ---\n", i+1);
		*((uint64_t *)p) = cvt64(wrq_data[i].id);
		printf("商品 ID: %lu (0x%.16lx)\n",
			cvt64(*((uint64_t *)p)),
			cvt64(*((uint64_t *)p)));
		p += 8;

		*p = wrq_data[i].code_len;
		printf("条码长度: %d\n", *p);
		p++;

		memcpy(p, wrq_data[i].code, wrq_data[i].code_len);
		printf("条码: ");
		print_str(p, *(p-1));
		p += *(p-1);

		*p = wrq_data[i].name_len;
		printf("单位名称长度: %d\n", *p);
		p++;

		memcpy(p, wrq_data[i].name, wrq_data[i].name_len);
		printf("单位名称: ");
		print_str(p, *(p-1));
		p += *(p-1);

		*((uint16_t *)p) = htons(wrq_data[i].uint_relation);
		printf("单位关系: %u (0x%.4x)\n",
			ntohs(*((uint16_t *)p)),
			ntohs(*((uint16_t *)p)));
		p += 2;

		memcpy(p, wrq_data[i].price, 10);
		printf("进货参考价: ");
		format_price(p, 10);
		p += 10;

		memcpy(p, wrq_data[i].retail_price, 10);
		printf("零售价: ");
		format_price(p, 10);
		p += 10;

		memcpy(p, wrq_data[i].vip_price, 10);
		printf("会员价: ");
		format_price(p, 10);
		p += 10;

		memcpy(p, wrq_data[i].dealer_price, 10);
		printf("经销商价: ");
		format_price(p, 10);
		p += 10;

		*p = wrq_data[i].oper_type;
		printf("操作类型: 0x%.2x%s\n", *p,
			(*p > 0x03) ?
			oper_type[0] :
			oper_type[(uint8_t)*p]);
		p++;
	}
	len = (void *)p - (void *)buf;
	printf("\n发送的数据长度: %d\n", len);
	syj_put(buf, len);

	free(buf);
	return 0;
}

int
syj_1c (void)
{
	char *buf;
	char *p;
	int len;
	int ret;

	struct s_1c_wrq *s1cwh;
	struct s_1c_rrq *s1crh;

	printf("\n>>> 商品档案管理请求 <<<\n");

	buf = malloc(sizeof(struct rxhdr) + MAXSIZE);
	if (buf == NULL)
		return -1;

	s1cwh = (void *)buf;
	s1cwh->cmd = 0x1c;
	s1cwh->dept_id = cvt64(DEPT_ID);

	printf("\n--- 商品档案信息请求 ---\n");
	printf("信令编号: 0x%.2x\n", s1cwh->cmd);
	printf("商家 ID: %lu (0x%.16lx)\n",
		cvt64(s1cwh->dept_id),
		cvt64(s1cwh->dept_id));
	p = (void *)&s1cwh->code_len;
	*p = 10;
	printf("商品条码长度: %d\n", *p);
	p++;
	memcpy(p, "1234567890", 10);
	printf("商品条码: ");
	print_str(p, *(p-1));
	p += *(p-1);
	syj_put(buf, (void *)p - (void *)buf);

	ret = syj_get(buf, &len);
	if (ret == -1) {
		printf("商品档案信息接收失败！\n");
		return -1;
	}

	s1crh = (void *)buf;
	printf("接收到数据长度: %d\n", len);
	printf("信令编号: 0x%.2x\n", s1crh->cmd);
	printf("总记录数: %d\n", s1crh->record_count);
	p = (void *)&s1crh->code_len;

	printf("商品条码长度: %d\n", *p);
	p++;

	printf("商品条码: ");
	print_str(p, *(p-1));
	p += *(p-1);

	printf("商品名称长度: %d\n", *p);
	p++;

	printf("商品名称: ");
	print_str(p, *(p-1));
	p += *(p-1);

	printf("基本单位名称长度: %d\n", *p);
	p++;

	printf("基本单位名称: ");
	print_str(p, *(p-1));
	p += *(p-1);

	printf("商品类别 ID: %lu (0x%.16lx)\n",
		cvt64(*((uint64_t *)p)),
		cvt64(*((uint64_t *)p)));
	p += 8;

	printf("规格长度: %d\n", *p);
	p++;

	printf("规格: ");
	print_str(p, *(p-1));
	p += *(p-1);

	printf("证件编号长度: %d\n", *p);
	p++;

	printf("证件编号: ");
	print_str(p, *(p-1));
	p += *(p-1);

	free(buf);
	return 0;
}

int
syj_1d (void)
{
	char *buf;
	struct s_1d_wrq *s1dwh;
	struct s_1d_rrq *s1drh;
	uint8_t *p;
	uint32_t *uint32;
	int len = 0, i, ret;

	printf("\n>>> 获取销售单商品信息请求 <<<\n");

	buf = malloc(sizeof(struct rxhdr) + MAXSIZE);
	if (buf == NULL)
		return -1;

	s1dwh = (void *)buf;
	s1drh = (void *)buf;
	s1dwh->cmd = 0x1d;
	s1dwh->dept_id = cvt64(DEPT_ID);
	memcpy(s1dwh->sale_uuid, "0123456789abcdeffedcba9876543210",
		sizeof(s1dwh->sale_uuid));

	printf("信令编号: 0x%.2x\n", s1dwh->cmd);
	printf("商家 ID: %lu (0x%.16lx)\n",
		cvt64(s1dwh->dept_id),
		cvt64(s1dwh->dept_id));
	printf("销售 UUID: ");
	print_str(s1dwh->sale_uuid, sizeof(s1dwh->sale_uuid));
	printf("发送的数据长度: %ld\n", sizeof(*s1dwh));
	syj_put(buf, sizeof(struct s_1d_wrq));

	ret = syj_get(buf, &len);
	if (ret == -1) {
		printf("获取销售单商品信息请求接收失败！\n");
		return -1;
	}

	printf("接收到数据长度: %d\n", len);
	printf("信令编号: 0x%.2x\n", s1drh->cmd);
	printf("总记录数: %d\n", ntohs(s1drh->record_count));
	p = (void *)s1drh->data;
	for (i=0; i<ntohs(s1drh->record_count); i++) {
		printf("\n--- 第 %d 条数据 ---\n", i+1);
		printf("商品名称长度: %d\n", *p);
		p++;

		printf("商品名称: ");
		print_str((char *)p, *(p-1));
		p += *(p-1);

		printf("单位名称长度: %d\n", *p);
		p++;

		printf("单位名称: ");
		print_str((char *)p, *(p-1));
		p += *(p-1);

		uint32 = (void *)p;
		printf("数量: %u (0x%.8x)\n", ntohl(*uint32), ntohl(*uint32));
		p += 4;
	}

	free(buf);
	return 0;
}

int
syj_1e (void)
{
	char *buf;
	struct s_1e_wrq *s1ewh;
	struct s_1e_rrq *s1erh;
	int len = 0, ret;

	printf("\n>>> 获取销售单商品信息请求 <<<\n");

	buf = malloc(sizeof(struct rxhdr) + MAXSIZE);
	if (buf == NULL)
		return -1;

	s1ewh = (void *)buf;
	s1erh = (void *)buf;
	s1ewh->cmd = 0x1e;
	memcpy(s1ewh->hard_version, "010000", 6);
	memcpy(s1ewh->soft_version, "010000", 6);

	printf("信令编号: 0x%.2x\n", s1ewh->cmd);
	printf("当前硬件版本号: ");
	print_ver(s1ewh->hard_version);
	printf("当前软件版本号: ");
	print_ver(s1ewh->soft_version);
	printf("发送的数据长度: %ld\n", sizeof(*s1ewh));
	syj_put(buf, sizeof(struct s_1e_wrq));

	ret = syj_get(buf, &len);
	if (ret == -1) {
		printf("获取销售单商品信息请求接收失败！\n");
		return -1;
	}

	printf("接收到数据长度: %d\n", len);
	printf("信令编号: 0x%.2x\n", s1erh->cmd);
	printf("最新软件版本号: ");
	print_ver(s1erh->last_version);
	printf("升级包大小: %u\n", ntohl(s1erh->filesize));
	printf("是否需要升级: 0x%.2x%s\n", s1erh->need_update,
		s1erh->need_update == 1 ?
		yes_no[1] :
		yes_no[2]);

	free(buf);
	return 0;
}

int
syj_1f (void)
{
	char *buf;
	uint8_t *p;
	struct s_1f_wrq *s1fwh;
	struct s_1f_rrq *s1frh;
	int len = 0, ret;
	int fd;

	printf("\n>>> 软件升级请求 <<<\n");

	buf = malloc(sizeof(struct rxhdr) + MAXSIZE);
	memset(buf, 0, sizeof(struct rxhdr) + MAXSIZE);
	if (buf == NULL)
		return -1;

	s1fwh = (void *)buf;
	s1frh = (void *)buf;
	s1fwh->cmd = 0x1f;
	s1fwh->dept_id = cvt64(DEPT_ID);
	memcpy(s1fwh->hard_version, "010000", 6);
	memcpy(s1fwh->cursoft_version, "010000", 6);
	memcpy(s1fwh->reqsoft_version, "010001", 6);

	printf("信令编号: 0x%.2x\n", s1fwh->cmd);
	printf("商家 ID: %lu (0x%.16lx)\n",
		cvt64(s1fwh->dept_id),
		cvt64(s1fwh->dept_id));
	printf("当前硬件版本号: ");
	print_ver(s1fwh->hard_version);
	printf("当前软件版本号: ");
	print_ver(s1fwh->cursoft_version);
	printf("请求软件版本号: ");
	print_ver(s1fwh->reqsoft_version);
	printf("发送的数据长度: %ld\n", sizeof(*s1fwh));
	syj_put(buf, sizeof(struct s_1f_wrq));

	ret = syj_get(buf, &len);
	if (ret == -1) {
		printf("软件升级请求接收失败！\n");
		return -1;
	}

	printf("接收到数据长度: %d\n", len);
	printf("信令编号: 0x%.2x\n", s1frh->cmd);
	printf("升级包大小: %u\n", ntohl(s1frh->filesize));
	p = (void *)s1frh->filecontent;
	printf("升级包内容:\n");
	hexdump(s1frh->filecontent, ntohl(s1frh->filesize));
	p += ntohl(s1frh->filesize);

	unlink(UPDATE_FILE);
	fd = open(UPDATE_FILE, O_RDWR | O_CREAT, 0644);
	ret = write(fd, s1frh->filecontent, ntohl(s1frh->filesize));
	close(fd);
	printf("Save %u bytes to %s. . .\n",
		ntohl(s1frh->filesize), UPDATE_FILE);

	free(buf);
	return 0;
}

int
syj_20 (void)
{
	char *buf;
	int i;
	uint8_t *p;
	int len;

	struct s_20_wrq *s20wh;
	struct s_20_wrq_data wrq_data[] = {
		{
			1234567890ULL,
			"6666",
			"123456",
			4,
			"白金",
			0x02,
		},
		{
			9876543210ULL,
			"8888",
			"passwd",
			6,
			"程淑英",
			0x03,
		},
		{
			0ULL,
			"9999",
			"123321",
			12,
			"员工自增测试",
			0x01,
		},
	};

	printf("\n>>> 员工管理请求 <<<\n");

	buf = malloc(sizeof(struct rxhdr) + MAXSIZE);
	if (buf == NULL)
		return -1;

	s20wh = (void *)buf;
	s20wh->cmd = 0x20;
	s20wh->dept_id = cvt64(DEPT_ID);
	s20wh->record_count = htons(sizeof(wrq_data) / sizeof(wrq_data[0]));

	printf("\n--- 员工管理请求 ---\n");
	printf("信令编号: 0x%.2x\n", s20wh->cmd);
	printf("商家 ID: %lu (0x%.16lx)\n",
		cvt64(s20wh->dept_id),
		cvt64(s20wh->dept_id));
	printf("总记录数: %u\n", ntohs(s20wh->record_count));
	p = (void *)s20wh->data;

	for (i=0; i<ntohs(s20wh->record_count); i++) {
		printf("\n--- 第 %d 条数据 ---\n", i+1);
		printf("员工 ID: %lu (0x%.16lx)\n",
			wrq_data[i].id,
			wrq_data[i].id);
		*((uint64_t *)p) = cvt64(wrq_data[i].id);
		p += 8;

		printf("员工编号: ");
		print_str(wrq_data[i].num, sizeof(wrq_data[i].num));
		memcpy(p, wrq_data[i].num, sizeof(wrq_data[i].num));
		p += sizeof(wrq_data[i].num);

		printf("员工密码: ");
		print_str(wrq_data[i].password, sizeof(wrq_data[i].password));
		memcpy(p, wrq_data[i].password, sizeof(wrq_data[i].password));
		p += sizeof(wrq_data[i].password);

		printf("员工姓名长度: %d\n", wrq_data[i].name_len);
		*p = wrq_data[i].name_len;
		p++;

		printf("员工姓名: %s\n", wrq_data[i].name);
		memcpy(p, wrq_data[i].name, wrq_data[i].name_len);
		p += wrq_data[i].name_len;

		printf("操作类型: 0x%.2x%s\n", wrq_data[i].oper_type,
			(wrq_data[i].oper_type > 0x03) ?
			oper_type[0] :
			oper_type[wrq_data[i].oper_type]);
		*p = wrq_data[i].oper_type;
		p++;
	}
	len = (void *)p - (void *)buf;
	printf("\n发送的数据长度: %d\n", len);
	syj_put(buf, len);

	free(buf);
	return 0;
}

int
syj_21 (void)
{
	char *buf;
	int i;
	uint8_t *p;
	int len;

	struct s_21_wrq *s21wh;
	struct s_21_wrq_data wrq_data[] = {
		{
			1234567890ULL,
			5,
			"12345",
			14,
			"供应商锐迅科技",
			0x02,
		},
		{
			9876543210ULL,
			7,
			"1234321",
			17,
			"供应商 - 深圳中收",
			0x03,
		},
		{
			0ULL,
			4,
			"test",
			18,
			"供应商自增插入测试",
			0x01,
		},
	};

	printf("\n>>> 供应商管理请求 <<<\n");

	buf = malloc(sizeof(struct rxhdr) + MAXSIZE);
	if (buf == NULL)
		return -1;

	s21wh = (void *)buf;
	s21wh->cmd = 0x21;
	s21wh->dept_id = cvt64(DEPT_ID);
	s21wh->record_count = htons(sizeof(wrq_data) / sizeof(wrq_data[0]));

	printf("\n--- 供应商管理请求 ---\n");
	printf("信令编号: 0x%.2x\n", s21wh->cmd);
	printf("商家 ID: %lu (0x%.16lx)\n",
		cvt64(s21wh->dept_id),
		cvt64(s21wh->dept_id));
	printf("总记录数: %u\n", ntohs(s21wh->record_count));
	p = (void *)s21wh->data;

	for (i=0; i<ntohs(s21wh->record_count); i++) {
		printf("\n--- 第 %d 条数据 ---\n", i+1);
		printf("供应商 ID: %lu (0x%.16lx)\n",
			wrq_data[i].id,
			wrq_data[i].id);
		*((uint64_t *)p) = cvt64(wrq_data[i].id);
		p += 8;

		printf("营业执照号码长度: %d\n", wrq_data[i].num_len);
		*p = wrq_data[i].num_len;
		p++;

		printf("营业执照号码: %s\n", wrq_data[i].num);
		memcpy(p, wrq_data[i].num, wrq_data[i].num_len);
		p += wrq_data[i].num_len;

		printf("供应商名称长度: %d\n", wrq_data[i].name_len);
		*p = wrq_data[i].name_len;
		p++;

		printf("供应商名称: %s\n", wrq_data[i].name);
		memcpy(p, wrq_data[i].name, wrq_data[i].name_len);
		p += wrq_data[i].name_len;

		printf("操作类型: 0x%.2x%s\n", wrq_data[i].oper_type,
			(wrq_data[i].oper_type > 0x03) ?
			oper_type[0] :
			oper_type[wrq_data[i].oper_type]);
		*p = wrq_data[i].oper_type;
		p++;
	}
	len = (void *)p - (void *)buf;
	printf("\n发送的数据长度: %d\n", len);
	syj_put(buf, len);

	free(buf);
	return 0;
}

int
syj_22 (void)
{
	char *buf;
	int i;
	uint8_t *p;
	int len;

	struct s_22_wrq *s22wh;
	struct s_22_wrq_data wrq_data[] = {
		{
			1234567890ULL,
			5,
			"12345",
			16,
			"客户锐迅科技白金",
			0x02,
		},
		{
			9876543210ULL,
			7,
			"1234321",
			21,
			"客户 - 深圳中收王英胜",
			0x03,
		},
		{
			0ULL,
			4,
			"test",
			16,
			"客户自增插入测试",
			0x01,
		},
	};

	printf("\n>>> 客户管理请求 <<<\n");

	buf = malloc(sizeof(struct rxhdr) + MAXSIZE);
	if (buf == NULL)
		return -1;

	s22wh = (void *)buf;
	s22wh->cmd = 0x22;
	s22wh->dept_id = cvt64(DEPT_ID);
	s22wh->record_count = htons(sizeof(wrq_data) / sizeof(wrq_data[0]));

	printf("\n--- 客户管理请求 ---\n");
	printf("信令编号: 0x%.2x\n", s22wh->cmd);
	printf("商家 ID: %lu (0x%.16lx)\n",
		cvt64(s22wh->dept_id),
		cvt64(s22wh->dept_id));
	printf("总记录数: %u\n", ntohs(s22wh->record_count));
	p = (void *)s22wh->data;

	for (i=0; i<ntohs(s22wh->record_count); i++) {
		printf("\n--- 第 %d 条数据 ---\n", i+1);
		printf("客户 ID: %lu (0x%.16lx)\n",
			wrq_data[i].id,
			wrq_data[i].id);
		*((uint64_t *)p) = cvt64(wrq_data[i].id);
		p += 8;

		printf("营业执照号码长度: %d\n", wrq_data[i].num_len);
		*p = wrq_data[i].num_len;
		p++;

		printf("营业执照号码: %s\n", wrq_data[i].num);
		memcpy(p, wrq_data[i].num, wrq_data[i].num_len);
		p += wrq_data[i].num_len;

		printf("供应商名称长度: %d\n", wrq_data[i].name_len);
		*p = wrq_data[i].name_len;
		p++;

		printf("供应商名称: %s\n", wrq_data[i].name);
		memcpy(p, wrq_data[i].name, wrq_data[i].name_len);
		p += wrq_data[i].name_len;

		printf("操作类型: 0x%.2x%s\n", wrq_data[i].oper_type,
			(wrq_data[i].oper_type > 0x03) ?
			oper_type[0] :
			oper_type[wrq_data[i].oper_type]);
		*p = wrq_data[i].oper_type;
		p++;
	}
	len = (void *)p - (void *)buf;
	printf("\n发送的数据长度: %d\n", len);
	syj_put(buf, len);

	free(buf);
	return 0;
}

int
quit (void)
{
	printf("退出模拟客户端\n");
	longjmp(go_out, -1);
	return 0;
}
