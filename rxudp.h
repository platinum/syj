#ifndef __RXUDP_H
#define __RXUDP_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <errno.h>
#include <stdint.h>
#include <fcntl.h>
#include <sys/time.h>
#include <unistd.h>

#define RXUDP_PORT	20111		/* 服务器端口 */
#define SEGSIZE		1000		/* 最大数据块 */
#define MAXSIZE		8 * 1024 * 1024	/* 支持最大传输 8MB，理论上只要内存够大，无最大传输限制 */
#define TIMEOUT		5		/* 超时时间 */
#define TIMEOUT_TIMES	5		/* 超时次数 */

/* 函数调用返回值 */
#define ERR		-1		/* 错误 */
#define OK		0		/* 正确 */

/* 操作码定义 */
#define RRQ		1		/* 获取数据请求 */
#define WRQ		2		/* 发送数据请求 */
#define DATA		3		/* 数据包标记 */
#define ACK		4		/* 应答包标记 */
#define ERROR		5		/* 错误包标记 */

/* 错误码定义 */
#define EUNDEF		0		/* 未定义 */
#define EOPCODE		1		/* 操作码错误 */
#define EBADID		2		/* BLOCK ID 错误 */
#define ESERVER		3		/* 服务端数据接口异常 */
#define ECLIENT		4		/* 收银机端硬件异常 */
#define EDATABASE	5		/* 后台数据库异常 */

/* 获取的数据包类型 */
#define GET_DISCARD	0		/* 该包应被丢弃 */
#define GET_TIMEOUT	1		/* 超时了 */
#define GET_RRQ		2		/* 是个 RRQ 包 */
#define GET_WRQ		3		/* 是个 WRQ 包 */
#define GET_ACK		4		/* 是个 ACK 包 */
#define GET_ERROR	5		/* 是个 ERROR 包 */
#define GET_DATA	6		/* 是个 DATA 包 */

/* 状态机状态 */
#define S_BEGIN		0		/* 状态机开始 */
#define S_SEND_REQ	1		/* 发送了请求 */
#define S_SEND_ACK	2		/* 发送了应答 */
#define S_SEND_DATA	3		/* 发送了数据 */
#define S_WAIT_PACKET	4		/* 等待接收 */
#define S_REQ_RECEIVED	5		/* 收到了请求 */
#define S_ACK_RECEIVED	6		/* 收到了应答 */
#define S_DATA_RECEIVED 7		/* 收到了数据 */
#define S_ABORT		8		/* 放弃传输 */
#define S_END		9		/* 传输完成 */

#define block_after(a, b) ((uint8_t)(a - b) == 1)

/*
 * -----------------------------------------------------------
 * | RX | SIMID | OP | BL|      DATA: Total 1000 bytes       |
 * -----------------------------------------------------------
 *       RXUDP HEADER                        DATA
 */
struct rxhdr
{
	uint8_t rexen[2];	 /* 存放 "RX" */
	uint8_t simid[15];	 /* SIM 卡唯一标识 */
	uint8_t opcode;		 /* 操作码 */
	union
	{
		uint8_t block;	 /* 数据块编号 */
		uint8_t errcode; /* 错误码 */
	} u;
	uint8_t data[0];	 /* 业务数据指针 */
} __attribute__((packed));

extern void rxudp_setsim (char *sim);

extern uint8_t rxudp_get_packet (int socketfd,
				 struct sockaddr_in *sn,
				 int timeout,
				 int *total_len,
				 struct rxhdr *rxh);

extern int rxudp_client_send (int socketfd,
			      struct sockaddr_in *sn,
			      uint8_t *data,
			      int len);

extern int rxudp_client_recv (int socketfd,
			      struct sockaddr_in *sn,
			      uint8_t *data,
			      int *len);

extern int rxudp_server_recv (int socketfd,
			      struct sockaddr_in *sn,
			      uint8_t *data,
			      int *len);

extern int rxudp_server_send (int socketfd,
			      struct sockaddr_in *sn,
			      uint8_t *data,
			      int len);

#endif /* __RXUDP_H */
