#ifndef __FLASHMEM_H
#define __FLASHMEM_H

#include <stdio.h>
#include <error.h>
#include <fcntl.h>
#include <string.h>
#include <stdint.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>

#define FLASH_SIZE	(64 * 1024 * 1024)
#define FLASH_NAME	"flash_64MB.sav"
#define UPDATE_FILE	"update.bin"

extern uint8_t *flash;
extern int flash_load (void);
extern int flash_init (void);
extern int flash_deinit (void);
extern int flash_save (void);

/* 定义 FLASH 的使用位置 */
#define FLASH_SYJ_HOST		0x0000000	/* 存放主机名称偏移量		*/
#define SYJ_HOST		((char *)flash + FLASH_SYJ_HOST)
#define FLASH_SYJ_PORT		0x0000100	/* 存放端口号偏移量		*/
#define SYJ_PORT		((char *)flash + FLASH_SYJ_PORT)
#define FLASH_SYJ_SIM		0x0000200	/* 存放 SIM 卡编号偏移量	*/
#define SYJ_SIM			((char *)flash + FLASH_SYJ_SIM)
#define FLASH_SYJ_HARDWARE	0x0000300	/* 存放硬件串号偏移量		*/
#define SYJ_HARDWARE		((char *)flash + FLASH_SYJ_HARDWARE)
#define FLASH_DEPT_ID		0x0000400	/* 商家 ID			*/
#define DEPT_ID			*((uint64_t *)((char *)flash + FLASH_DEPT_ID))

#endif	/* __FLASHMEM_H */
