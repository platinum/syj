#ifndef __GLOBAL_H
#define __GLOBAL_H

#define SYJ_VERSION	"v1.7.3 build 11052001"

void
hexdump (char *buf, int len)
{
	int i;

	printf("offset 00 01 02 03 04 05 06 07-08 09 0A 0B 0C 0D 0E 0F\n");
	for (i=0; i<len; i++) {
		if ((i+1) % 16 == 1)
			printf("%.5X: ", i);
		printf("%.2X%s", (uint8_t)buf[i],
			(i+1) % 16 == 8 ?
			"-" : (i+1) % 16 == 0 ? "\n" : " ");
	}

	printf("\n");
}

void
print_str (char *buf, int len)
{
	int i;

	printf("\"");
	for (i=0; i<len; i++)
		printf("%c", buf[i]);
	printf("\"");

	printf("\n");
	fflush(stdout);
}

uint64_t
cvt64 (uint64_t src) {
	uint8_t c;
	union {
		uint64_t ull;
		uint8_t c[8];
	} x;

	x.ull = src;
	c = x.c[0]; x.c[0] = x.c[7]; x.c[7] = c;
	c = x.c[1]; x.c[1] = x.c[6]; x.c[6] = c;
	c = x.c[2]; x.c[2] = x.c[5]; x.c[5] = c;
	c = x.c[3]; x.c[3] = x.c[4]; x.c[4] = c;

	return x.ull;
}

void
format_price (char *buf, int len)
{
	/* 跳过之前的 0 */

	int i, j = 0;
	for (i=0; i<len; i++) {
		if (buf[i] != '0') {
			j = i;
			break;
		}
	}
	
	printf("（");
	for (i=0; i<len-j; i++) {
		if ((len-j) == 2) {
			printf("0.%c%c", buf[j + i], buf[j + i + 1]);
			break;
		}
		else if ((len-j) == 1) {
			printf("0.0%c", buf[j + i]);
			break;
		}
		else if((len-j) == 0) {
			printf("0");
			break;
		}
		else {
			printf("%c", buf[j + i]);
			if (j + i == 7)
				printf(".");
		}
	}
	printf("）元 ---> ");

	/*
	uint64_t price1 =0;
	double price2 = 0.0;
	price1 = atol(buf);
	printf("（");
	price2 = (double)price1 / 100;
	printf("%.2f", price2);
	printf("）");
	fflush(stdout);
	*/
	print_str(buf, len);

	fflush(stdout);
}

void
print_ver (char *buf)
{
	char num[3] = {0};
	uint16_t a, b, c;

	memcpy(num, buf + 0, 2);
	a = atoi(num);
	memcpy(num, buf + 2, 2);
	b = atoi(num);
	memcpy(num, buf + 4, 2);
	c = atoi(num);

	printf("v%u.%u.%u ---> ", a, b, c);
	print_str(buf, 6);
}

#endif /* __GLOBAL_H */
