#include "rxudp.h"
#include "syj_struct.h"
#include "global.h"

uint8_t buf[MAXSIZE];

int
syj_01 (uint8_t *buf)
{
	/* 硬件信息模拟 */
	struct s_01_wrq *s01wh = (void *)buf;
	struct s_01_rrq *s01rh = (void *)buf;
	char *sim = "fedcba987654321";
	char *hardware = "p0o9i8u7y6t5r4e3w2q1";
	uint64_t dept_id = 1234554321Ull;
	char *num = "09";

	printf("\n>>> 服务器收到数据 <<<\n");

	printf("信令编号: 0x%.2x\n", s01wh->cmd);
	printf("SIM 卡编号: ");
	print_str(s01wh->sim, 15);
	printf("硬件串号: ");
	print_str(s01wh->hardware, 20);

	printf("----- 服务器验证信息 -----\n");
	printf("验证 SIM 卡编号: %s\n", sim);
	printf("验证硬件串号: %s\n", hardware);

	printf("----- 服务器构造信息 -----\n");
	s01rh->cmd = 0x01;
	if (!memcmp(s01wh->sim, sim, sizeof(s01wh->sim)) &&
	    !memcmp(s01wh->hardware, hardware, sizeof(s01wh->hardware))) {
		s01rh->result = 1;
		s01rh->dept_id = cvt64(dept_id);
		memcpy(s01rh->num, num, sizeof(s01rh->num));
	}
	else
		s01rh->result = 0;

	printf("信令编号: 0x%.2x\n", s01rh->cmd);
	printf("验证结果: 0x%.2x\n", s01rh->result);
	printf("商家 ID:  %lu (0x%.16lx)\n",
		dept_id, dept_id);
	printf("收银机编号: ");
	print_str(s01rh->num, sizeof(s01rh->num));

	return sizeof(struct s_01_rrq);
}

int
syj_02 (uint8_t *buf)
{
	/* 全局参数模拟 */
	struct s_02_wrq *s02wh = (void *)buf;
	struct s_02_rrq *s02rh = (void *)buf;

	printf("\n>>> 服务器收到数据 <<<\n");

	printf("信令编号: 0x%.2x\n", s02wh->cmd);
	printf("商家 ID: %lu (0x%.16lx)\n",
		cvt64(s02wh->dept_id),
		cvt64(s02wh->dept_id));
	printf("最后更新时间: 0x%.8x\n", ntohl(s02wh->last_update));

	s02rh->cmd = 0x02;
	s02rh->use_shelf = 0x01;
	s02rh->direct_out = 0x02;
	s02rh->direct_in = 0x02;
	s02rh->batch = 0x02;
	s02rh->batch_order = 0x01;
	s02rh->if_auto = 0x01;
	s02rh->last_update = htonl(0x22220002);
	printf("----- 服务器构造信息 -----\n");
	printf("信令编号: 0x%.2x\n", s02rh->cmd);
	printf("是否启用在售货架: 0x%.2x%s\n", s02rh->use_shelf,
		s02rh->use_shelf > 2 ?
		yes_no[0] :
		yes_no[s02rh->use_shelf]);
	printf("是否直接出库: 0x%.2x%s\n", s02rh->direct_out,
		s02rh->direct_out > 2 ?
		yes_no[0] :
		yes_no[s02rh->direct_out]);
	printf("是否直接入库: 0x%.2x%s\n", s02rh->direct_in,
		s02rh->direct_in > 2 ?
		yes_no[0] :
		yes_no[s02rh->direct_in]);
	printf("是否选择批次: 0x%.2x%s\n", s02rh->batch,
		s02rh->batch > 2 ?
		yes_no[0] :
		yes_no[s02rh->batch]);
	printf("批次号排序规则: 0x%.2x%s\n", s02rh->batch_order,
		s02rh->batch_order > 2 ?
		order[0] :
		order[s02rh->batch_order]);
	printf("是否自动生成进货单: 0x%.2x%s\n", s02rh->if_auto,
		s02rh->if_auto > 2 ?
		yes_no[0] :
		yes_no[s02rh->if_auto]);
	printf("最后更新时间: 0x%.8x\n", ntohl(s02rh->last_update));

	return sizeof(struct s_02_rrq);
}

int
syj_03 (uint8_t *buf)
{
	/* 定义小票内容 */
	char h1[] = "锐迅科技";
	char h2[] = "中收科技";
	char h3[] = "票头第三行";
	char h4[] = "head line: 4";
	char h5[] = "";
	char t1[] = "票尾第一行";
	char t2[] = "tail: 2";
	char t3[] = "";
	uint8_t h1_len = strlen(h1);
	uint8_t h2_len = strlen(h2);
	uint8_t h3_len = strlen(h3);
	uint8_t h4_len = strlen(h4);
	uint8_t h5_len = strlen(h5);
	uint8_t t1_len = strlen(t1);
	uint8_t t2_len = strlen(t2);
	uint8_t t3_len = strlen(t3);
	uint8_t h1_f = 0x01;
	uint8_t h2_f = 0x01;
	uint8_t h3_f = 0x00;
	uint8_t h4_f = 0x00;
	uint8_t h5_f = 0x00;
	uint8_t t1_f = 0x00;
	uint8_t t2_f = 0x01;
	uint8_t t3_f = 0x00;
	struct s_03_wrq *s03wh = (void *)buf;
	struct s_03_rrq *s03rh = (void *)buf;
	uint32_t *last_update;
	int i;

	printf("\n>>> 服务器收到数据 <<<\n");

	printf("信令编号: 0x%.2x\n", s03wh->cmd);
	printf("最后更新时间: 0x%.8x\n", ntohl(s03wh->last_update));

	i = 0;
	/* 设置信令 */
	s03rh->cmd = 0x03;
	i += sizeof(s03rh->cmd);
	/* 设置 h1_len */
	buf[i] = h1_len;
	i += sizeof(h1_len);
	/* 复制 h1 内容 */
	memcpy(buf + i, h1, h1_len);
	i += h1_len;
	/* 设置 h2_len */
	buf[i] = h2_len;
	i += sizeof(h2_len);
	/* 复制 h2 内容 */
	memcpy(buf + i, h2, h2_len);
	i += h2_len;
	/* 设置 h3_len */
	buf[i] = h3_len;
	i += sizeof(h3_len);
	/* 复制 h3 内容 */
	memcpy(buf + i, h3, h3_len);
	i += h3_len;
	/* 设置 h4_len */
	buf[i] = h4_len;
	i += sizeof(h4_len);
	/* 复制 h4 内容 */
	memcpy(buf + i, h4, h4_len);
	i += h4_len;
	/* 设置 h5_len */
	buf[i] = h5_len;
	i += sizeof(h5_len);
	/* 复制 h5 内容 */
	memcpy(buf + i, h5, h5_len);
	i += h5_len;
	/* 设置 t1_len */
	buf[i] = t1_len;
	i += sizeof(t1_len);
	/* 复制 t1 内容 */
	memcpy(buf + i, t1, t1_len);
	i += t1_len;
	/* 设置 t2_len */
	buf[i] = t2_len;
	i += sizeof(t2_len);
	/* 复制 t2 内容 */
	memcpy(buf + i, t2, t2_len);
	i += t2_len;
	/* 设置 t3_len */
	buf[i] = t3_len;
	i += sizeof(t3_len);
	/* 复制 t3 内容 */
	memcpy(buf + i, t3, t3_len);
	i += t3_len;
	/* 设置 h1 - h5, t1 - t3 的双高标记 */
	buf[i + 0] = h1_f;
	buf[i + 1] = h2_f;
	buf[i + 2] = h3_f;
	buf[i + 3] = h4_f;
	buf[i + 4] = h5_f;
	buf[i + 5] = t1_f;
	buf[i + 6] = t2_f;
	buf[i + 7] = t3_f;
	i += 8;
	/* 设置最后更新时间 */
	last_update = (void *)buf + i;
	*last_update = htonl(0x22220003);
	i += 4;

	/* 打印待发送数据 */
	printf("----- 服务器构造信息 -----\n");
	printf("信令编号: 0x%.2x\n", s03rh->cmd);
	printf("票头第 1 行长度: %d\n", h1_len);
	printf("票头第 1 行内容: \"%s\"\n", h1);
	printf("票头第 1 行双高: 0x%.2x\n", h1_f);
	printf("票头第 2 行长度: %d\n", h2_len);
	printf("票头第 2 行内容: \"%s\"\n", h2);
	printf("票头第 2 行双高: 0x%.2x\n", h2_f);
	printf("票头第 3 行长度: %d\n", h3_len);
	printf("票头第 3 行内容: \"%s\"\n", h3);
	printf("票头第 3 行双高: 0x%.2x\n", h3_f);
	printf("票头第 4 行长度: %d\n", h4_len);
	printf("票头第 4 行内容: \"%s\"\n", h4);
	printf("票头第 4 行双高: 0x%.2x\n", h4_f);
	printf("票头第 5 行长度: %d\n", h5_len);
	printf("票头第 5 行内容: \"%s\"\n", h5);
	printf("票头第 5 行双高: 0x%.2x\n", h5_f);
	printf("票尾第 1 行长度: %d\n", t1_len);
	printf("票尾第 1 行内容: \"%s\"\n", t1);
	printf("票尾第 1 行双高: 0x%.2x\n", t1_f);
	printf("票尾第 2 行长度: %d\n", t2_len);
	printf("票尾第 2 行内容: \"%s\"\n", t2);
	printf("票尾第 2 行双高: 0x%.2x\n", t2_f);
	printf("票尾第 3 行长度: %d\n", t3_len);
	printf("票尾第 3 行内容: \"%s\"\n", t3);
	printf("票尾第 3 行双高: 0x%.2x\n", t3_f);
	printf("最后更新时间: 0x%.8x\n", ntohl(*last_update));

	return i;
}

int
syj_04 (uint8_t *buf)
{
	/* 定义商家信息 */
	uint64_t *pid;
	char num[] = "123456789012345";
	char code[] = "12348012";
	char name[] = "大辉狼";
	char tel[] = "89565347";
	char address[] = "北京市西城区新风街大成科技大厦8012";
	uint16_t *province;
	uint16_t *city;
	uint8_t num_len = strlen(num);
	uint8_t code_len = strlen(code);
	uint8_t name_len = strlen(name);
	uint8_t tel_len = strlen(tel);
	uint8_t address_len = strlen(address);
	struct s_04_wrq *s04wh = (void *)buf;
	struct s_04_rrq *s04rh = (void *)buf;
	uint32_t *last_update;
	uint64_t *id;
	int i;

	printf("\n>>> 服务器收到数据 <<<\n");

	printf("信令编号: 0x%.2x\n", s04wh->cmd);
	printf("商家 ID: %lu (0x%.16lx)\n",
		cvt64(s04wh->dept_id),
		cvt64(s04wh->dept_id));
	printf("最后更新时间: 0x%.8x\n", ntohl(s04wh->last_update));

	i = 0;
	/* 设置信令 */
	buf[i] = 0x04;
	i += sizeof(s04rh->cmd);
	/* 设置商家 ID */
	id = (void *)buf + i;
	*id = cvt64(0x1122334455667788ULL);
	i += sizeof(*id);
	/* 设置商家 PID */
	pid = (void *)buf +i;
	*pid = cvt64(0x1122334455667700ULL);
	i += sizeof(*pid);
	/* 设置营业执照号码长度	*/
	buf[i] = num_len;
	i += sizeof(num_len);
	/* 设置营业执照号码	*/
	memcpy(buf+i, num, num_len);
	i += num_len;
	/* 设置商家编码		*/
	memcpy(buf+i, code, code_len);
	i += code_len;
	/* 设置商家名称长度	*/
	buf[i] = name_len;
	i += sizeof(name_len);
	/* 设置商家名称		*/
	memcpy(buf+i, name, name_len);
	i += name_len;
	/* 设置电话号码长度	*/
	buf[i] = tel_len;
	i += sizeof(tel_len);
	/* 设置电话号码		*/
	memcpy(buf+i, tel,tel_len);
	i += tel_len;
	/* 设置商家地址长度	*/
	buf[i] = address_len;
	i += sizeof(address_len);
	/* 复制商家地址		*/
	memcpy(buf+i, address, address_len);
	i += address_len;
	/* 设置省份 */
	province = (void *)buf + i;
	*province = htons(0x02);
	i += sizeof(*province);
	/* 设置城市 */
	city = (void *)buf + i;
	*city = htons(0x03);
	i += sizeof(*city);

	/* 设置最后更新时间 */
	last_update = (void *)buf + i;
	*last_update = htonl(0x22220004);
	i += 4;

	/* 打印发送数据 */
	printf("----- 服务器构造信息 ------\n");
	printf("信令编号: 0x%.2x\n", s04rh->cmd);
	printf("商家 ID: %lu (0x%.16lx)\n",
		cvt64(*id), cvt64(*id));
	printf("商家 PID %lu (0x%.16lx)\n",
		cvt64(*pid), cvt64(*pid));
	printf("营业执照号码长度: %d\n", num_len);
	printf("营业执照号码: %s\n", num);
	printf("商家编码: %s\n", code);
	printf("商家名称长度: %d\n", name_len);
	printf("商家名称: %s\n", name);
	printf("商家电话长度: %d\n", tel_len);
	printf("商家电话: %s\n", tel);
	printf("商家地址长度: %d\n", address_len);
	printf("商家地址: %s\n", address);
	printf("省份: 0x%.4x\n", ntohs(*province));
	printf("城市: 0x%.4x\n", ntohs(*city));
	printf("最后更新时间: 0x%.8x\n", ntohl(*last_update));

	return i;
}

int
syj_05 (uint8_t *buf)
{
	struct s_05_wrq *s05wh = (void *)buf;
	struct s_05_rrq *s05rh = (void *)buf;
	struct s_05_rrq_data *s05data_p;
	struct s_05_rrq_data s05data[] = {
		{ 0x00, 0x00 },
		{ 0x01, 0x01 },
		{ 0x02, 0x02 },
		{ 0x01, 0x00 },
	};
	int len;
	int i;

	printf("\n>>> 服务器收到数据 <<<\n");

	printf("信令编号: 0x%.2x\n", s05wh->cmd);
	printf("商家 ID: %lu (0x%.16lx)\n",
		cvt64(s05wh->dept_id),
		cvt64(s05wh->dept_id));
	printf("最后更新时间: 0x%.8x\n", ntohl(s05wh->last_update));

	printf("----- 服务器构造信息 -----\n");
	s05rh->cmd = 0x05;
	s05rh->record_count = sizeof(s05data)/sizeof(s05data[0]);
	s05rh->last_update = htonl(0x22220005);
	printf("信令编号: 0x%.2x\n", s05rh->cmd);
	printf("总记录数: %d\n", s05rh->record_count);
	printf("最后更新时间: 0x%.8x\n", ntohl(s05rh->last_update));
	len = sizeof(struct s_05_rrq);
	s05data_p = (void *)s05rh->data;
	for (i=0; i<s05rh->record_count; i++) {
		s05data_p->client_type = s05data[i].client_type;
		s05data_p->price_type = s05data[i].price_type;
		printf("\n--- 第 %d 条数据 ---\n", i+1);
		printf("客户类型: 0x%.2x", s05data_p->client_type);
		printf("%s\n", client_type[s05data_p->client_type]);
		printf("销售价格类型: 0x%.2x", s05data_p->price_type);
		printf("%s\n", price_type[s05data_p->price_type]);
		s05data_p++;
		len += sizeof(struct s_05_rrq_data);
	}

	return len;
}

int
syj_06 (uint8_t *buf)
{
	struct s_06_wrq *s06wh = (void *)buf;
	struct s_06_rrq *s06rh = (void *)buf;
	struct s_06_rrq_data s06data[] = {
		{
			0x1122334455667788ULL,	/* 仓库 ID	*/
			6,			/* 仓库名称长度	*/
			"仓库一",		/* 仓库名称	*/
			4,			/* 仓库编码长度	*/
			"0001",			/* 仓库编码	*/
			1,			/* 仓库类型	*/
			0x1122334455667788ULL,	/* 库管员 ID	*/
			4,			/* 库管员长度	*/
			"白金",			/* 库管员名称	*/
			1,			/* 是否默认	*/
			1,			/* 操作类型	*/
		},
		{
			0x1357924680ABCDEFULL,	/* 仓库 ID	*/
			8,			/* 仓库名称长度	*/
			"锐迅仓库",		/* 仓库名称	*/
			4,			/* 仓库编码长度	*/
			"6666",			/* 仓库编码	*/
			2,			/* 仓库类型	*/
			0x1357924680ABCDEFULL,	/* 库管员 ID	*/
			6,			/* 库管员长度	*/
			"程淑英",		/* 库管员名称	*/
			1,			/* 是否默认	*/
			2,			/* 操作类型	*/
		},
		{
			0xABCDABCDABCDABCDULL,	/* 仓库 ID	*/
			10,			/* 仓库名称长度	*/
			"一二三四五",		/* 仓库名称	*/
			4,			/* 仓库编码长度	*/
			"8888",			/* 仓库编码	*/
			3,			/* 仓库类型	*/
			0xABCDABCDABCDABCDULL,	/* 库管员 ID	*/
			6,			/* 库管员长度	*/
			"赵宇东",		/* 库管员名称	*/
			1,			/* 是否默认	*/
			3,			/* 操作类型	*/
		},
		{
			0xFEDCBA9876543210ULL,	/* 仓库 ID	*/
			5,			/* 仓库名称长度	*/
			"12345",		/* 仓库名称	*/
			4,			/* 仓库编码长度	*/
			"9999",			/* 仓库编码	*/
			3,			/* 仓库类型	*/
			0xFEDCBA9876543210ULL,	/* 库管员 ID	*/
			6,			/* 库管员长度	*/
			"大灰狼",		/* 库管员名称	*/
			2,			/* 是否默认	*/
			2,			/* 操作类型	*/
		},
	};
	uint64_t *id;
	int len;
	int i;

	printf("\n>>> 服务器收到数据 <<<\n");

	printf("信令编号: 0x%.2x\n", s06wh->cmd);
	printf("最后更新时间: 0x%.8x\n", ntohl(s06wh->last_update));

	printf("----- 服务器构造信息 -----\n");
	s06rh->cmd = 0x06;
	s06rh->record_count = sizeof(s06data)/sizeof(s06data[0]);
	s06rh->last_update = htonl(0x22220006);
	printf("信令编号: 0x%.2x\n", s06rh->cmd);
	printf("总记录数: %d\n", s06rh->record_count);
	printf("最后更新时间: 0x%.8x\n", ntohl(s06rh->last_update));
	len = 0;
	for (i=0; i<s06rh->record_count; i++) {
		printf("\n--- 第 %d 条数据 ---\n", i+1);

		printf("仓库 ID: %lu (0x%.16lx)\n",
			s06data[i].id, s06data[i].id);
		id = (void *)s06rh->data + len;
		*id = cvt64(s06data[i].id);
		len += 8;

		printf("仓库名称长度: %d\n", s06data[i].name_len);
		s06rh->data[len] = s06data[i].name_len;
		len++;

		printf("仓库名称: %s\n", s06data[i].name);
		memcpy(s06rh->data + len,
		       s06data[i].name,
		       s06data[i].name_len);
		len += s06data[i].name_len;

		printf("仓库编码长度: %d\n", s06data[i].code_len);
		s06rh->data[len] = s06data[i].code_len;
		len++;

		printf("仓库编码: %s\n", s06data[i].code);
		memcpy(s06rh->data + len,
		       s06data[i].code,
		       s06data[i].code_len);
		len += s06data[i].code_len;

		printf("仓库类型: 0x%.2x%s\n", s06data[i].type,
			(s06data[i].type > 0x03) ?
			cangku[0] :
			cangku[s06data[i].type]);
		s06rh->data[len] = s06data[i].type;
		len++;

		printf("库管员 ID: %lu (0x%.16lx)\n",
			s06data[i].userid,
			s06data[i].userid);
		id = (void *)s06rh->data + len;
		*id = cvt64(s06data[i].userid);
		len += 8;

		printf("库管员姓名长度: %d\n", s06data[i].user_name_len);
		s06rh->data[len] = s06data[i].user_name_len;
		len++;

		printf("库管员姓名: %s\n", s06data[i].user_name);
		memcpy(s06rh->data + len,
		       s06data[i].user_name,
		       s06data[i].user_name_len);
		len += s06data[i].user_name_len;

		printf("是否默认: 0x%.2x%s\n", s06data[i].is_default,
			s06data[i].is_default ?
			"（是）" : "（否）");
		s06rh->data[len] = s06data[i].is_default;
		len++;

		printf("操作类型: 0x%.2x%s\n",
			s06data[i].oper_type,
			oper_type[s06data[i].oper_type]);
		s06rh->data[len] = s06data[i].oper_type;
		len++;
	}
	len += sizeof(struct s_06_rrq);

	return len;
}

int
syj_07 (uint8_t *buf)
{
	uint64_t *id;
	int len, i, num_len, pwd_len;
	struct s_07_wrq *s07wh = (void *)buf;
	struct s_07_rrq *s07rh = (void *)buf;
	struct s_07_rrq_data s07data[] = {
		{
			0x1122334455667788ULL,	/* 员工 ID	*/
			"1001",			/* 员工编号	*/
			"111111",		/* 员工密码	*/
			4,			/* 员工姓名长度	*/
			"杨过",			/* 员工姓名	*/
			0x01			/* 操作类型	*/
		},				/* 0x01: 添加
						   0x02: 修改
						   0x03: 删除	*/
		{
			0x1357924680ABCDEFULL,
			"1002",
			"222222",
			6,
			"小龙女",
			0x02
		},
		{
			0xABCDABCDABCDABCDULL,
			"1003",
			"333333",
			8,
			"白眉鹰王",
			0x03
		}
	};

	printf("\n>>> 服务器收到数据 <<<\n");

	printf("信令编号: 0x%.2x\n", s07wh->cmd);
	printf("商家 ID: %lu (0x%.16lx)\n",
		cvt64(s07wh->dept_id),
		cvt64(s07wh->dept_id));
	printf("最后更新时间 : 0x%.8x\n", ntohl(s07wh->last_update));

	printf("----- 服务器构造信息 -----\n");
	s07rh->cmd = 0x07;
	int num = sizeof(s07data) / sizeof(s07data[0]);
	s07rh->record_count = htons(num);
	s07rh->last_update = htonl(0x22220007);
	printf("信令编号: 0x%.2x\n", s07rh->cmd);
	printf("总记录数: %d\n", num);
	printf("最后更新时间 : 0x%.8x\n", ntohl(s07rh->last_update));
	len = 0;
	for (i=0; i<num; i++) {
		printf("\n--- 第 %d 条数据 ---\n", i+1);
		id = (void *)s07rh->data + len;
		printf("员工 ID: %lu (0x%.16lx)\n",
			s07data[i].id, s07data[i].id);
		*id = cvt64(s07data[i].id);
		len += 8;

		num_len = sizeof(s07data[i].num);
		printf("员工编号: ");
		print_str(s07data[i].num, num_len);
		memcpy(s07rh->data + len, s07data[i].num, num_len);
		len += num_len;

		pwd_len = sizeof(s07data[i].login_password);
		printf("员工密码: ");
		print_str(s07data[i].login_password, pwd_len);
		memcpy(s07rh->data + len,
		       s07data[i].login_password,
		       pwd_len);
		len += pwd_len;

		printf("员工姓名长度: %d\n", s07data[i].name_len);
		s07rh->data[len] = s07data[i].name_len;
		len++;

		printf("员工姓名: %s\n", s07data[i].name);
		memcpy(s07rh->data + len,
		       s07data[i].name,
		       s07data[i].name_len);
		len += s07data[i].name_len;

		printf("操作类型: 0x%.2x%s\n", s07data[i].oper_type,
			(s07data[i].oper_type > 0x03) ?
			oper_type[0] :
			oper_type[s07data[i].oper_type]);
		s07rh->data[len] = s07data[i].oper_type;
		len++;
	}

	len += sizeof(struct s_07_rrq);
	return len;
}

int
syj_08 (uint8_t *buf)
{
	uint64_t *id;
	int len, i, sys_dept_len;
	struct s_08_wrq *s08wh = (void *)buf;
	struct s_08_rrq *s08rh = (void *)buf;
	struct s_08_rrq_data s08data[] = {
		{
			0x1122334455667788ULL,	/* 员工 ID                              */
			15,			/* 营业执照号码长度，最大长度 15        */
			"123456789012345",	/* 营业执照号码                         */
			6,			/* 客户名称长度,最大长度 60             */
			"李云龙",		/* 客户名称                             */
			"A1234567",		/* 客户编码		                */
			0x01			/* 操作类型: 添加 0x02: 修改 0x03: 删除 */
		},
		{
			0x1357924680ABCDEULL,
			14,
			"12345678901234",
			4,
			"武松",
			"B1234567",
			0x02
		},
		{
			0xABCDABCDABCDABCDULL,
			13,
			"1234567890123",
			10,
			"白眉鹰王子",
			"C1234567",
			0x03
		}
	};

	printf("\n>>> 服务器收到数据 <<<\n");

	printf("信令编号: 0x%.2x\n", s08wh->cmd);
	printf("商家 ID: %lu (0x%.16lx)\n",
		cvt64(s08wh->dept_id),
		cvt64(s08wh->dept_id));
	printf("最后更新时间 : 0x%.8x\n", ntohl(s08wh->last_update));

	printf("----- 服务器构造信息 -----\n");
	s08rh->cmd = 0x08;
	int num = sizeof(s08data) / sizeof(s08data[0]);
	s08rh->record_count = htons(num);
	s08rh->last_update = htonl(0x22220008);
	printf("信令编号: 0x%.2x\n", s08rh->cmd);
	printf("总记录数: %d\n", num);
	printf("最后更新时间 : 0x%.8x\n", ntohl(s08rh->last_update));
	len = 0;
	for (i=0; i<num; i++) {
		printf("\n--- 第 %d 条数据 ---\n", i+1);
		id = (void *)s08rh->data + len;
		printf("客户 ID: %lu (0x%.16lx)\n",
			s08data[i].id, s08data[i].id);
		*id = cvt64(s08data[i].id);
		len += 8;

		printf("营业执照号码长度: %d\n", s08data[i].num_len);
		s08rh->data[len] = s08data[i].num_len;
		len++;

		printf("营业执照号码: \"%s\"\n", s08data[i].num);
		memcpy(s08rh->data + len,
		       s08data[i].num,
		       s08data[i].num_len);
		len += s08data[i].num_len;

		printf("客户名称长度: %d\n", s08data[i].name_len);
		s08rh->data[len] = s08data[i].name_len;
		len++;

		printf("客户名称: \"%s\"\n", s08data[i].name);
		memcpy(s08rh->data + len,
		       s08data[i].name,
		       s08data[i].name_len);
		len += s08data[i].name_len;

		printf("客户编码: ");
		sys_dept_len = sizeof(s08data[i].sys_dept_code);
		print_str(s08data[i].sys_dept_code, sys_dept_len);
		memcpy(s08rh->data + len,
		       s08data[i].sys_dept_code,
		       sys_dept_len);
		len += sys_dept_len;

		printf("操作类型: 0x%.2x%s\n", s08data[i].oper_type,
			(s08data[i].oper_type > 0x03) ?
			oper_type[0] :
			oper_type[s08data[i].oper_type]);
		s08rh->data[len] = s08data[i].oper_type;
		len++;
	}

	len += sizeof(struct s_08_rrq);
	return len;
}

int
syj_09 (uint8_t *buf)
{
	uint64_t *id;
	int len, i, sys_dept_len;
	struct s_09_wrq *s09wh = (void *)buf;
	struct s_09_rrq *s09rh = (void *)buf;
	struct s_09_rrq_data s09data[] = {
		{
			0x1122334455667788ULL,	/* 供应商 ID                            */
			15,			/* 营业执照号码长度，最大长度 15        */
			"123456789012345",	/* 营业执照号码                         */
			6,			/* 供应商名称长度,最大长度 60           */
			"周星驰",		/* 供应商名称                           */
			"a1234567",		/* 供应商编码		                */
			0x01			/* 操作类型: 添加 0x02: 修改 0x03: 删除 */
		},
		{
			0x1357924680ABCDEULL,
			14,
			"12345678901234",
			4,
			"成龙",
			"b1234567",
			0x02
		},
		{
			0xABCDABCDABCDABCDULL,
			13,
			"1234567890123",
			10,
			"川岛芳子君",
			"c1234567",
			0x03
		}
	};

	printf("\n>>> 服务器收到数据 <<<\n");

	printf("信令编号: 0x%.2x\n", s09wh->cmd);
	printf("商家 ID: %lu (0x%.16lx)\n",
		cvt64(s09wh->dept_id),
		cvt64(s09wh->dept_id));
	printf("最后更新时间 : 0x%.8x\n", ntohl(s09wh->last_update));

	printf("----- 服务器构造信息 -----\n");
	s09rh->cmd = 0x09;
	int num = sizeof(s09data) / sizeof(s09data[0]);
	s09rh->record_count = htons(num);
	s09rh->last_update = htonl(0x22220009);
	printf("信令编号: 0x%.2x\n", s09rh->cmd);
	printf("总记录数: %d\n", num);
	printf("最后更新时间 : 0x%.8x\n", ntohl(s09rh->last_update));
	len = 0;
	for (i=0; i<num; i++) {
		printf("\n--- 第 %d 条数据 ---\n", i+1);
		id = (void *)s09rh->data + len;
		printf("客户 ID: %lu (0x%.16lx)\n",
			s09data[i].id, s09data[i].id);
		*id = cvt64(s09data[i].id);
		len += 8;

		printf("营业执照号码长度: %d\n", s09data[i].num_len);
		s09rh->data[len] = s09data[i].num_len;
		len++;

		printf("营业执照号码: \"%s\"\n", s09data[i].num);
		memcpy(s09rh->data + len,
		       s09data[i].num,
		       s09data[i].num_len);
		len += s09data[i].num_len;

		printf("供应商称长度: %d\n", s09data[i].name_len);
		s09rh->data[len] = s09data[i].name_len;
		len++;

		printf("供应商名称: \"%s\"\n", s09data[i].name);
		memcpy(s09rh->data + len,
		       s09data[i].name,
		       s09data[i].name_len);
		len += s09data[i].name_len;

		printf("供应商编码: ");
		sys_dept_len = sizeof(s09data[i].sys_dept_code);
		print_str(s09rh->data + len, sys_dept_len);
		memcpy(s09rh->data + len,\
		       s09data[i].sys_dept_code,
		       sys_dept_len);
		len += sys_dept_len;

		printf("操作类型: 0x%.2x%s\n", s09data[i].oper_type,
			(s09data[i].oper_type > 0x03) ?
			oper_type[0] :
			oper_type[s09data[i].oper_type]);
		s09rh->data[len] = s09data[i].oper_type;
		len++;
	}

	len += sizeof(struct s_09_rrq);
	return len;
}

int
syj_0a (uint8_t *buf)
{
	uint64_t *id;
	int len, i;
	struct s_0a_wrq *s0awh = (void *)buf;
	struct s_0a_rrq *s0arh = (void *)buf;
	struct s_0a_rrq_data s0adata[] = {
		{
			0x112233445566ULL,	/* 商品 ID			*/
			13,			/* 商品条码长度			*/
			"0620110000015",	/* 商品条码			*/
			12,			/* 商品名称长度			*/
			"康师傅矿泉水",		/* 商品名称			*/
			2,			/* 基本单位名称长度		*/
			"瓶",			/* 基本单位名称			*/
			111222333ULL,		/* 商品类别 ID			*/
			8,			/* 规格长度			*/
			"500ml/瓶",		/* 规格				*/
			12345678ULL,		/* 基础商品 ID			*/
			1,			/* 是否公有			*/
			11112222ULL,		/* 商家 ID			*/
			10,			/* 证件编号长度			*/
			"ABCDEFG123",		/* 证件编号			*/
			0x01			/* 操作类型			*/
		},
		{
			0x112233445599ULL,
			12,
			"062011000014",
			8,
			"营养快线",
			2,
			"瓶",
			222333444ULL,		/* 商品类别 ID			*/
			7,
			"50ml/瓶",
			12345678ULL,		/* 基础商品 ID			*/
			1,			/* 是否公有			*/
			0,			/* 商家 ID（为空）		*/
			11,			/* 证件编号长度			*/
			"ABCDEFG1234",		/* 证件编号			*/
			0x02
		},
		{
			0xaabbccdd5599ULL,
			11,
			"06201100013",
			9,
			"营养快线2",
			2,
			"箱",
			333444555ULL,		/* 商品类别 ID			*/
			6,
			"50g/箱",
			12345678ULL,		/* 基础商品 ID			*/
			1,			/* 是否公有			*/
			0,			/* 商家 ID（为空）		*/
			12,			/* 证件编号长度			*/
			"ABCDEFG12345",		/* 证件编号			*/
			0x03
		},
		{
			0xAABBCCDD5599ULL,
			10,
			"0620110012",
			9,
			"营养快线3",
			2,
			"箱",
			444555666ULL,		/* 商品类别 ID			*/
			8,
			"5000g/箱",
			12345678ULL,		/* 基础商品 ID			*/
			0,			/* 是否公有			*/
			0,			/* 商家 ID（为空）		*/
			13,			/* 证件编号长度			*/
			"ABCDEFG123456",	/* 证件编号			*/
			0x00
		}
	};

	printf("\n>>> 服务器收到数据 <<<\n");

	printf("信令编号: 0x%.2x\n", s0awh->cmd);
	printf("商家 ID: %lu (0x%.16lx)\n",
		cvt64(s0awh->dept_id),
		cvt64(s0awh->dept_id));
	printf("最后更新时间 : 0x%.8x\n", ntohl(s0awh->last_update));

	printf("----- 服务器构造信息 -----\n");
	s0arh->cmd = 0x0a;
	int num = sizeof(s0adata) / sizeof(s0adata[0]);
	s0arh->record_count = htons(num);
	s0arh->last_update = htonl(0x2222000a);
	printf("信令编号: 0x%.2x\n", s0arh->cmd);
	printf("总记录数: %d\n", num);
	printf("最后更新时间 : 0x%.8x\n", ntohl(s0arh->last_update));
	len = 0;
	for (i=0; i<num; i++) {
		printf("\n--- 第 %d 条数据 ---\n", i+1);
		id = (void *)s0arh->data + len;
		printf("商品 ID: %lu (0x%.16lx)\n",
			s0adata[i].id, s0adata[i].id);
		*id = cvt64(s0adata[i].id);
		len += 8;

		printf("商品条码长度: %d\n", s0adata[i].code_len);
		s0arh->data[len] = s0adata[i].code_len;
		len++;

		printf("商品条码: %s\n", s0adata[i].code);
		memcpy(s0arh->data + len,
		       s0adata[i].code,
		       s0adata[i].code_len);
		len += s0adata[i].code_len;

		printf("商品名称长度: %d\n", s0adata[i].name_len);
		s0arh->data[len] = s0adata[i].name_len;
		len++;

		printf("商品名称: %s\n", s0adata[i].name);
		memcpy(s0arh->data + len,
		       s0adata[i].name,
		       s0adata[i].name_len);
		len += s0adata[i].name_len;

		printf("基本单位名称长度: %d\n",
			s0adata[i].base_unit_name_len);
		s0arh->data[len] = s0adata[i].base_unit_name_len;
		len++;

		printf("基本单位名称: %s\n", s0adata[i].base_unit_name);
		memcpy(s0arh->data + len,
		       s0adata[i].base_unit_name,
		       s0adata[i].base_unit_name_len);
		len += s0adata[i].base_unit_name_len;

		printf("商品类别 ID: %lu (0x%.16lx)\n",
			s0adata[i].category_id,
			s0adata[i].category_id);
		id = (void *)s0arh->data + len;
		*id = cvt64(s0adata[i].category_id);
		len += 8;

		printf("规格长度: %d\n", s0adata[i].spec_len);
		s0arh->data[len] = s0adata[i].spec_len;
		len++;

		printf("规格: %s\n", s0adata[i].spec);
		memcpy(s0arh->data + len,
		       s0adata[i].spec,
		       s0adata[i].spec_len);
		len += s0adata[i].spec_len;

		printf("基础商品 ID: %lu (0x%.16lx)\n",
			s0adata[i].commondity_id,
			s0adata[i].commondity_id);
		id = (void *)s0arh->data + len;
		*id = cvt64(s0adata[i].commondity_id);
		len += 8;

		printf("是否公有: 0x%.2x%s\n", s0adata[i].is_public,
			s0adata[i].is_public > 2 ?
			yes_no[0] :
			yes_no[s0adata[i].is_public]);
		s0arh->data[len] = s0adata[i].is_public;
		len++;

		printf("商家 ID: %lu (0x%.16lx)\n",
			s0adata[i].dept_id,
			s0adata[i].dept_id);
		id = (void *)s0arh->data + len;
		*id = cvt64(s0adata[i].dept_id);
		len += 8;

		printf("证件编号长度: %d\n", s0adata[i].cert_code_len);
		s0arh->data[len] = s0adata[i].cert_code_len;
		len++;

		printf("证件编号: \"%s\"\n", s0adata[i].cert_code);
		memcpy(s0arh->data + len,
		       s0adata[i].cert_code,
		       s0adata[i].cert_code_len);
		len += s0adata[i].cert_code_len;

		printf("操作类型: 0x%.2x%s\n", s0adata[i].oper_type,
			(s0adata[i].oper_type > 0x03) ?
			oper_type[0] :
			oper_type[s0adata[i].oper_type]);
		s0arh->data[len] = s0adata[i].oper_type;
		len++;
	}

	len += sizeof(struct s_0a_rrq);
	return len;
}

int
syj_0b (uint8_t *buf)
{
	uint64_t *id;
	uint16_t *uint16;
	int len, i, price_len = 10;
	struct s_0b_wrq *s0bwh = (void *)buf;
	struct s_0b_rrq *s0brh = (void *)buf;
	struct s_0b_rrq_data s0bdata[] = {
		{
			0x112233445566ULL,	/* 商品 ID			*/
			0x1122334455ULL,	/* 单位 ID			*/
			2,			/* 单位名称长度			*/
			"箱",			/* 单位名称			*/
			10,			/* 单位关系,网络字节序		*/
			20,			/* 条码长度			*/
			"06998877665544332211", /* 条码				*/
			"0000009900",		/* 零售价			*/
			"0000008888",		/* 会员价			*/
			"0000077000",		/* 经销商价			*/
			"0000010155",		/* 进货参考价			*/
			0x01,			/* 单位类别			*/
			0x01			/* 操作类型			*/
		},
		{
			0x112233445566ULL,	/* 商品 ID			*/
			0x1122334455ULL,	/* 单位 ID			*/
			2,			/* 单位名称长度			*/
			"条",			/* 单位名称			*/
			11,			/* 单位关系,网络字节序		*/
			20,			/* 条码长度			*/
			"06998877665544332210", /* 条码				*/
			"0000005550",		/* 零售价			*/
			"0000008800",		/* 预售价1			*/
			"0088000800",		/* 预售价2			*/
			"0000010166",		/* 进货参考价			*/
			0x02,			/* 单位类别			*/
			0x02			/* 操作类型			*/

		},
		{
			0x334455667799ULL,	/* 商品 ID			*/
			0x1122334455ULL,	/* 单位 ID			*/
			4,			/* 单位名称长度			*/
			"大包",			/* 单位名称			*/
			12,			/* 单位关系,网络字节序		*/
			20,			/* 条码长度			*/
			"06998877665544666666", /* 条码				*/
			"0000007777",		/* 零售价			*/
			"0000008800",		/* 预售价1			*/
			"0066008066",		/* 预售价2			*/
			"0000010177",		/* 进货参考价			*/
			0x02,			/* 单位类别			*/
			0x03			/* 操作类型			*/
		}
	};

	printf("\n>>> 服务器收到数据 <<<\n");

	printf("信令编号: 0x%.2x\n", s0bwh->cmd);
	printf("商家 ID: %lu (0x%.16lx)\n",
		cvt64(s0bwh->dept_id),
		cvt64(s0bwh->dept_id));
	printf("最后更新时间 : 0x%.8x\n", ntohl(s0bwh->last_update));

	printf("----- 服务器构造信息 -----\n");
	s0brh->cmd = 0x0b;
	int num = sizeof(s0bdata) / sizeof(s0bdata[0]);
	s0brh->record_count = htonl(num);
	s0brh->last_update = htonl(0x2222000b);
	printf("信令编号: 0x%.2x\n", s0brh->cmd);
	printf("总记录数: %d\n", num);
	printf("最后更新时间 : 0x%.8x\n", ntohl(s0brh->last_update));
	len = 0;
	for (i=0; i<num; i++) {
		printf("\n--- 第 %d 条数据 ---\n", i+1);
		id = (void *)s0brh->data + len;
		printf("商品 ID: %lu (0x%.16lx)\n",
			s0bdata[i].commodity_id,
			s0bdata[i].commodity_id);
		*id = cvt64(s0bdata[i].commodity_id);
		len += sizeof(*id);

		printf("单位 ID: %lu (0x%.16lx)\n",
			s0bdata[i].unit_id,
			s0bdata[i].unit_id);
		id = (void *)s0brh->data + len;
		*id = cvt64(s0bdata[i].unit_id);
		len += sizeof(*id);

		printf("单位名称长度: %d\n", s0bdata[i].unit_name_len);
		s0brh->data[len] = s0bdata[i].unit_name_len;
		len++;

		printf("单位名称: ");
		print_str(s0bdata[i].unit_name, s0bdata[i].unit_name_len);
		memcpy(s0brh->data + len,
		       s0bdata[i].unit_name,
		       s0bdata[i].unit_name_len);
		len += s0bdata[i].unit_name_len;

		uint16 = (void *)s0brh->data + len;
		printf("单位关系: %d\n", s0bdata[i].unit_relation);
		*uint16 = htons(s0bdata[i].unit_relation);
		len += 2;

		printf("条码长度: %d\n", s0bdata[i].code_len);
		s0brh->data[len] = s0bdata[i].code_len;
		len += sizeof(s0bdata[i].code_len);

		printf("条码: %s\n", s0bdata[i].code);
		memcpy(s0brh->data + len,
		       s0bdata[i].code,
		       s0bdata[i].code_len);
		len += s0bdata[i].code_len;

		printf("零售价: ");
		format_price(s0bdata[i].retail_price, price_len);
		memcpy(s0brh->data + len,
		       s0bdata[i].retail_price,
		       price_len);
		len += price_len;

		printf("会员价: ");
		format_price(s0bdata[i].vip_price, price_len);
		memcpy(s0brh->data + len,
		       s0bdata[i].vip_price,
		       price_len);
		len += price_len;

		printf("经销商价: ");
		format_price(s0bdata[i].deal_price, price_len);
		memcpy(s0brh->data + len,
		       s0bdata[i].deal_price,
		       price_len);
		len += price_len;

		printf("进货参考价: ");
		format_price(s0bdata[i].price, price_len);
		memcpy(s0brh->data + len,
		       s0bdata[i].price,
		       price_len);
		len += price_len;

		printf("单位类别: 0x%.2x%s\n", s0bdata[i].sort,
			(s0bdata[i].sort > 0x02) ?
			sorts[0] :
			sorts[s0bdata[i].sort]);
		s0brh->data[len] = s0bdata[i].sort;
		len += sizeof(s0bdata[i].sort);

		printf("操作类型: 0x%.2x%s\n", s0bdata[i].oper_type,
			(s0bdata[i].oper_type > 0x03) ?
			oper_type[0] :
			oper_type[s0bdata[i].oper_type]);
		s0brh->data[len] = s0bdata[i].oper_type;
		len += sizeof(s0bdata[i].oper_type);
	}

	len += sizeof(struct s_0b_rrq);
	return len;
}

int
syj_0c (uint8_t *buf)
{
	uint64_t *id;
	uint16_t *uint16;
	int len, i;
	struct s_0c_wrq *s0cwh = (void *)buf;
	struct s_0c_rrq *s0crh = (void *)buf;
	struct s_0c_rrq_data s0cdata[] = {
		{
			0x112233445566ULL,	/* 商品 ID		*/
			11,			/* 批次号长度		*/
			"01111122222",		/* 批次号		*/
			10,			/* 生产日期长度		*/
			"2011-02-22",		/* 生产日期		*/
			90,			/* 保质期		*/
			0x01,			/* 保质单位		*/
			10,			/* 过期日期长度		*/
			"2011-12-10",		/* 过期日期长		*/
			11223344ULL,		/* 仓库 ID		*/
			0x01			/* 操作类型		*/
		},
		{
			0xaabbccdd5566ULL,  /* 商品 ID              */
			12,                     /* 批次号长度           */
			"011111333333",         /* 批次号               */
			10,                     /* 生产日期长度         */
			"2012-12-15",           /* 生产日期             */
			9,                      /* 保质期               */
			0x02,                   /* 保质单位             */
			10,                     /* 过期日期长度         */
			"2012-12-26",           /* 过期日期		*/
			22334455ULL,		/* 仓库 ID		*/
			0x02                    /* 操作类型             */
		},
		{
			0x998877665544ULL,  /* 商品 ID              */
			13,                     /* 批次号长度           */
			"0111112222255",        /* 批次号               */
			10,                     /* 生产日期长度         */
			"2013-09-28",           /* 生产日期             */
			12,                     /* 保质期               */
			0x03,                   /* 保质单位             */
			10,                     /* 过期日期长度         */
			"2013-12-19",           /* 过期日期		*/
			33445566ULL,		/* 仓库 ID		*/
			0x03                    /* 操作类型             */
		}
	};

	printf("\n>>> 服务器收到数据 <<<\n");

	printf("信令编号: 0x%.2x\n", s0cwh->cmd);
	printf("商家 ID: %lu (0x%.16lx)\n",
		cvt64(s0cwh->dept_id),
		cvt64(s0cwh->dept_id));
	printf("最后更新时间 : 0x%.8x\n", ntohl(s0cwh->last_update));

	printf("----- 服务器构造信息 -----\n");
	s0crh->cmd = 0x0c;
	int num = sizeof(s0cdata) / sizeof(s0cdata[0]);
	s0crh->record_count = htonl(num);
	s0crh->last_update = htonl(0x2222000c);
	printf("信令编号: 0x%.2x\n", s0crh->cmd);
	printf("总记录数: %d\n", num);
	printf("最后更新时间 : 0x%.8x\n", ntohl(s0crh->last_update));
	len = 0;
	for (i=0; i<num; i++) {
		printf("\n--- 第 %d 条数据 ---\n", i+1);
		id = (void *)s0crh->data + len;
		printf("商品 ID: %lu (0x%.16lx)\n",
			s0cdata[i].commodity_id,
			s0cdata[i].commodity_id);
		*id = cvt64(s0cdata[i].commodity_id);
		len += sizeof(*id);

		printf("批次号长度: %d\n", s0cdata[i].batch_len);
		s0crh->data[len] = s0cdata[i].batch_len;
		len += sizeof(s0cdata[i].batch_len);
		printf("批次号: %s\n", s0cdata[i].batch);
		memcpy(s0crh->data + len,
		       s0cdata[i].batch,
		       s0cdata[i].batch_len);
		len += s0cdata[i].batch_len;

		printf("生产日期长度: %d\n", s0cdata[i].create_date_len);
		s0crh->data[len] = s0cdata[i].create_date_len;
		len += sizeof(s0cdata[i].create_date_len);
		printf("生产日期: %s\n", s0cdata[i].create_date);
		memcpy(s0crh->data + len,
		       s0cdata[i].create_date,
		       s0cdata[i].create_date_len);
		len += s0cdata[i].create_date_len;

		uint16 = (void *)s0crh->data + len;
		printf("保质期: %d\n", s0cdata[i].quality_data);
		*uint16 = htons(s0cdata[i].quality_data);
		len += 2;

		printf("保质单位: 0x%.2x%s\n", s0cdata[i].quality_unit,
			(s0cdata[i].quality_unit > 0x03) ?
			quality_units[0] :
			quality_units[s0cdata[i].quality_unit]);
		s0crh->data[len] = s0cdata[i].quality_unit;
		len += sizeof(s0cdata[i].quality_unit);

		printf("过期日期长度: %d\n", s0cdata[i].exceed_date_len);
		s0crh->data[len] = s0cdata[i].exceed_date_len;
		len += sizeof(s0cdata[i].exceed_date_len);
		printf("过期日期: %s\n", s0cdata[i].exceed_date);
		memcpy(s0crh->data + len,
		       s0cdata[i].exceed_date,
		       s0cdata[i].exceed_date_len);
		len += s0cdata[i].exceed_date_len;

		id = (void *)s0crh->data + len;
		printf("仓库 ID: %lu (0x%.16lx)\n",
			s0cdata[i].depot_id,
			s0cdata[i].depot_id);
		*id = cvt64(s0cdata[i].depot_id);
		len += sizeof(*id);

		printf("操作类型: 0x%.2x%s\n", s0cdata[i].oper_type,
			(s0cdata[i].oper_type > 0x03) ?
			oper_type[0] :
			oper_type[s0cdata[i].oper_type]);
		s0crh->data[len] = s0cdata[i].oper_type;
		len += sizeof(s0cdata[i].oper_type);
	}

	len += sizeof(struct s_0c_rrq);
	return len;
}

int
syj_0d (uint8_t *buf)
{
	uint64_t *id;
	uint16_t *content_len;
	int len, i;
	struct s_0d_wrq *s0dwh = (void *)buf;
	struct s_0d_rrq *s0drh = (void *)buf;
	struct s_0d_rrq_data s0ddata[] = {
		{
			0x112233445566ULL,	/* 任务 ID		*/
			12,			/* 任务标题长度		*/
			"关于数据同步",		/* 任务标题		*/
			10,			/* 任务内容长度		*/
			"明后天再说",		/* 任务内容		*/
			"2088-09-09 12:30:35",	/* 发送时间		*/
			0x01,			/* 任务类型		*/
			8,			/* 任务参数长度		*/
			"任务参数",		/* 任务参数长		*/
			0x01,			/* 处理状态		*/
			0x03			/* 操作类型		*/
		},
		{
			0xaabbccdd1122ULL,  /* 任务 ID              */
			18,                     /* 任务标题长度         */
			"收银机任务进度安排",   /* 任务标题             */
			32,                     /* 任务内容长度         */
			"3月1号完成模拟收银"
			"机22个业务功能",	/* 任务内容             */
			"2009-10-11 10:10:10",  /* 发送时间             */
			0x02,                   /* 任务类型             */
			9,                      /* 任务参数长度         */
			"任务参数2",            /* 任务参数长           */
			0x03,                   /* 处理状态             */
			0x01                    /* 操作类型             */
		},
		{
			0x998877665544ULL,  /* 任务 ID              */
			8,                      /* 任务标题长度         */
			"任务标题",	        /* 任务标题             */
			10,                     /* 任务内容长度         */
			"明后天再说",           /* 任务内容             */
			"2012-12-12 12:12:12",  /* 发送时间             */
			0x03,                   /* 任务类型             */
			9,                      /* 任务参数长度         */
			"任务参数3",            /* 任务参数长           */
			0x00,                   /* 处理状态             */
			0x02                    /* 操作类型             */
		}
	};

	printf("\n>>> 服务器收到数据 <<<\n");

	printf("信令编号: 0x%.2x\n", s0dwh->cmd);
	printf("商家 ID: %lu (0x%.16lx)\n",
		cvt64(s0dwh->dept_id),
		cvt64(s0dwh->dept_id));
	printf("最后更新时间 : 0x%.8x\n", ntohl(s0dwh->last_update));

	printf("----- 服务器构造信息 -----\n");
	s0drh->cmd = 0x0d;
	s0drh->record_count = sizeof(s0ddata) / sizeof(s0ddata[0]);
	s0drh->last_update = htonl(0x2222000d);
	printf("信令编号: 0x%.2x\n", s0drh->cmd);
	printf("总记录数: %d\n", s0drh->record_count);
	printf("最后更新时间 : 0x%.8x\n", ntohl(s0drh->last_update));
	len = 0;
	for (i=0; i<s0drh->record_count; i++) {
		printf("\n--- 第 %d 条数据 ---\n", i+1);
		id = (void *)s0drh->data + len;
		printf("任务 ID: %lu (0x%.16lx)\n",
			s0ddata[i].id, s0ddata[i].id);
		*id = cvt64(s0ddata[i].id);
		len += sizeof(*id);

		printf("任务标题长度: %d\n", s0ddata[i].title_len);
		s0drh->data[len] = s0ddata[i].title_len;
		len += sizeof(s0ddata[i].title_len);
		printf("任务标题: %s\n", s0ddata[i].title);
		memcpy(s0drh->data + len,
		       s0ddata[i].title,
		       s0ddata[i].title_len);
		len += s0ddata[i].title_len;

		printf("任务内容长度: %d\n", s0ddata[i].content_len);
		content_len = (void *)s0drh->data + len;
		*content_len = htons(s0ddata[i].content_len);
		len += sizeof(*content_len);
		printf("任务内容: %s\n", s0ddata[i].content);
		memcpy(s0drh->data + len,
		       s0ddata[i].content,
		       s0ddata[i].content_len);
		len += s0ddata[i].content_len;

		printf("发送时间: ");
		int stime_len = sizeof(s0ddata[i].send_time);
		print_str(s0ddata[i].send_time, stime_len);
		memcpy(s0drh->data + len,
		       s0ddata[i].send_time,
		       stime_len);
		len += stime_len;

		printf("任务类型: 0x%.2x%s\n", s0ddata[i].sort,
			(s0ddata[i].sort > 0x02) ?
			sort_type[0] :
			sort_type[s0ddata[i].sort]);
		s0drh->data[len] = s0ddata[i].sort;
		len += sizeof(s0ddata[i].sort);

		printf("任务参数长度: %d\n", s0ddata[i].pm_len);
		s0drh->data[len] = s0ddata[i].pm_len;
		len += sizeof(s0ddata[i].pm_len);
		printf("任务参数: %s\n", s0ddata[i].pm);
		memcpy(s0drh->data + len,
		       s0ddata[i].pm,
		       s0ddata[i].pm_len);
		len += s0ddata[i].pm_len;

		printf("处理状态: 0x%.2x%s\n", s0ddata[i].status,
			(s0ddata[i].status > 0x01) ?
			status[2] :
			status[s0ddata[i].status]);
		s0drh->data[len] = s0ddata[i].status;
		len += sizeof(s0ddata[i].status);

		printf("操作类型: 0x%.2x%s\n", s0ddata[i].oper_type,
			(s0ddata[i].oper_type > 0x03) ?
			oper_type[0] :
			oper_type[s0ddata[i].oper_type]);
		s0drh->data[len] = s0ddata[i].oper_type;
		len += sizeof(s0ddata[i].oper_type);
	}

	len += sizeof(struct s_0d_rrq);
	return len;
}

int
syj_0e (uint8_t *buf)
{

	uint64_t *id;
	uint16_t *content_len;
	int len, i;
	struct s_0e_wrq *s0ewh = (void *)buf;
	struct s_0e_rrq *s0erh = (void *)buf;
	struct s_0e_rrq_data s0edata[] = {
		{
			0x112233445566ULL,	/* 消息 ID              */
			6,			/* 消息标题长度         */
			"进货单",		/* 消息标题             */
			20,			/* 消息内容长度         */
			"自动生成进货单成功了",	/* 消息内容             */
			"2099-09-09 12:12:12",	/* 发送时间             */
			0x01,			/* 消息类型             */
			0x02			/* 操作类型             */
		},
		{
			0x112233445566ULL,  /* 消息 ID              */
			6,                      /* 消息标题长度         */
			"商家证",               /* 消息标题             */
			12,                     /* 消息内容长度         */
			"索商家证成功",		/* 消息内容             */
			"2099-09-09 13:13:13",  /* 发送时间             */
			0x02,                   /* 消息类型             */
			0x01                    /* 操作类型             */
		},
		{
			0x112233445566ULL,  /* 消息 ID              */
			6,                      /* 消息标题长度         */
			"产品证",               /* 消息标题             */
			14,                     /* 消息内容长度         */
			"索产品证成功了",	/* 消息内容             */
			"2099-09-09 15:15:15",  /* 发送时间             */
			0x03,                   /* 消息类型             */
			0x03                    /* 操作类型             */
		}
	};

	printf("\n>>> 服务器收到数据 <<<\n");

	printf("信令编号: 0x%.2x\n", s0ewh->cmd);
	printf("商家 ID: %lu (0x%.16lx)\n",
		cvt64(s0ewh->dept_id),
		cvt64(s0ewh->dept_id));
	printf("最后更新时间 : 0x%.8x\n", ntohl(s0ewh->last_update));

	printf("----- 服务器构造信息 -----\n");
	s0erh->cmd = 0x0e;
	s0erh->record_count = sizeof(s0edata) / sizeof(s0edata[0]);
	s0erh->last_update = htonl(0x2222000e);
	printf("信令编号: 0x%.2x\n", s0erh->cmd);
	printf("总记录数: %d\n", s0erh->record_count);
	printf("最后更新时间 : 0x%.8x\n", ntohl(s0erh->last_update));
	len = 0;
	for (i=0; i<s0erh->record_count; i++) {
		printf("\n--- 第 %d 条数据 ---\n", i+1);
		id = (void *)s0erh->data + len;
		printf("消息 ID: %lu (0x%.16lx)\n",
			s0edata[i].id, s0edata[i].id);
		*id = cvt64(s0edata[i].id);
		len += sizeof(*id);

		printf("消息标题长度: %d\n", s0edata[i].title_len);
		s0erh->data[len] = s0edata[i].title_len;
		len += sizeof(s0edata[i].title_len);
		printf("消息标题: %s\n", s0edata[i].title);
		memcpy(s0erh->data + len,
		       s0edata[i].title,
		       s0edata[i].title_len);
		len += s0edata[i].title_len;

		printf("消息内容长度: %d\n", s0edata[i].content_len);
		content_len = (void *)s0erh->data + len;
		*content_len = htons(s0edata[i].content_len);
		len += sizeof(*content_len);
		printf("消息内容: %s\n", s0edata[i].content);
		memcpy(s0erh->data + len,
		       s0edata[i].content,
		       s0edata[i].content_len);
		len += s0edata[i].content_len;

		printf("发送时间: ");
		int stime_len = sizeof(s0edata[i].send_time);
		print_str(s0edata[i].send_time, stime_len);
		memcpy(s0erh->data + len,
		       s0edata[i].send_time,
		       stime_len);
		len += stime_len;

		printf("消息类型: 0x%.2x%s%s\n", s0edata[i].sort,
			(s0edata[i].sort > 0x03) ?
			sort_type[0] :
			sort_type[s0edata[i].sort], "结果");
		s0erh->data[len] = s0edata[i].oper_type;
		len += sizeof(s0edata[i].oper_type);

		printf("操作类型: 0x%.2x%s\n", s0edata[i].oper_type,
			(s0edata[i].oper_type > 0x03) ?
			oper_type[0] :
			oper_type[s0edata[i].oper_type]);
		s0erh->data[len] = s0edata[i].oper_type;
		len += sizeof(s0edata[i].oper_type);
	}

	len += sizeof(struct s_0e_rrq);
	return len;
}

int
syj_0f (uint8_t *buf)
{
	uint8_t *p;
	int i;
	struct s_0f_wrq *s0fwh = (void *)buf;
	struct s_0f_rrq *s0frh = (void *)buf;
	struct s_0f_rrq_data s0fdata[] = {
		{
			0x112233445566ULL,	/* 类别 ID			*/
			1,			/* 类别编号长度，最大 4		*/
			"1",			/* 类别编号			*/
			16,			/* 类别名称长度，最大 30	*/
			"类别名称长度测试",	/* 类别名称			*/
			0x01,			/* 监管属性			*/
			8,			/* 编码长度，最大 20		*/
			"12345678",		/* 编码				*/
			0x02,			/* 操作类型			*/
		},
		{
			0x222233445566ULL,	/* 类别 ID			*/
			3,			/* 类别编号长度，最大 4		*/
			"333",			/* 类别编号			*/
			17,			/* 类别名称长度，最大 30	*/
			"类别名称长度测试2",	/* 类别名称			*/
			0x03,			/* 监管属性			*/
			10,			/* 编码长度，最大 20		*/
			"1234567890",		/* 编码				*/
			0x03,			/* 操作类型			*/
		},
		{
			0x0ULL,			/* 类别 ID			*/
			4,			/* 类别编号长度，最大 4		*/
			"1234",			/* 类别编号			*/
			12,			/* 类别名称长度，最大 30	*/
			"自增添加测试",		/* 类别名称			*/
			0x04,			/* 监管属性			*/
			7,			/* 编码长度，最大 20		*/
			"abcdefg",		/* 编码				*/
			0x01,			/* 操作类型			*/
		},
		{
			0x0ULL,			/* 类别 ID			*/
			0,			/* 类别编号长度，最大 4		*/
			"",			/* 类别编号			*/
			12,			/* 类别名称长度，最大 30	*/
			"异常数据测试",		/* 类别名称			*/
			0x00,			/* 监管属性			*/
			0,			/* 编码长度，最大 20		*/
			"",			/* 编码				*/
			0x05,			/* 操作类型			*/
		},
	};

	printf("\n>>> 服务器收到数据 <<<\n");

	printf("信令编号: 0x%.2x\n", s0fwh->cmd);
	printf("最后更新时间 : 0x%.8x\n", ntohl(s0fwh->last_update));

	printf("----- 服务器构造信息 -----\n");
	s0frh->cmd = 0x0f;
	s0frh->record_count = htons(sizeof(s0fdata) / sizeof(s0fdata[0]));
	s0frh->last_update = htonl(0x2222000f);
	printf("信令编号: 0x%.2x\n", s0frh->cmd);
	printf("总记录数: %d\n", ntohs(s0frh->record_count));
	printf("最后更新时间 : 0x%.8x\n", ntohl(s0frh->last_update));
	p = (void *)s0frh->data;
	for (i=0; i<ntohs(s0frh->record_count); i++) {
		printf("\n--- 第 %d 条数据 ---\n", i+1);
		printf("类别 ID: %lu (0x%.16lx)\n",
			s0fdata[i].id, s0fdata[i].id);
		*((uint64_t *)p) = cvt64(s0fdata[i].id);
		p += 8;

		printf("类别编号长度: %d\n", s0fdata[i].number_len);
		*p = s0fdata[i].number_len;
		p++;

		printf("类别编号: %s\n", s0fdata[i].number);
		memcpy(p, s0fdata[i].number, s0fdata[i].number_len);
		p += s0fdata[i].number_len;

		printf("类别名称长度: %d\n", s0fdata[i].name_len);
		*p = s0fdata[i].name_len;
		p++;

		printf("类别名称: %s\n", s0fdata[i].name);
		memcpy(p, s0fdata[i].name, s0fdata[i].name_len);
		p += s0fdata[i].name_len;

		printf("监管属性: 0x%.2x%s\n", s0fdata[i].is_circulate,
			s0fdata[i].is_circulate > 0x04 ?
			supervise[0] :
			supervise[s0fdata[i].is_circulate]);
		*p = s0fdata[i].is_circulate;
		p++;

		printf("编码长度: %d\n", s0fdata[i].code_len);
		*p = s0fdata[i].code_len;
		p++;

		printf("编码: %s\n", s0fdata[i].code);
		memcpy(p, s0fdata[i].code, s0fdata[i].code_len);
		p += s0fdata[i].code_len;

		printf("操作类型: 0x%.2x%s\n", s0fdata[i].oper_type,
			s0fdata[i].oper_type > 0x03 ?
			oper_type[0] :
			oper_type[s0fdata[i].oper_type]);
		*p = s0fdata[i].oper_type;
		p++;
	}

	return (void *)p - (void *)buf;
}

int
syj_10 (uint8_t *buf)
{
	uint8_t *p;
	int i;
	uint64_t *id;
	struct s_10_wrq *s10wh = (void *)buf;
	struct s_10_rrq *s10rh = (void *)buf;
	struct s_10_rrq_data s10data[] = {
		{
			0x112233445566ULL,	/* ID				*/
			8,			/* 名称长度，最大 20		*/
			"名称测试",		/* 名称				*/
			0x665544332211ULL,	/* 对应值			*/
			5,			/* 类别				*/
			3,			/* 显示顺序			*/
			0x02,			/* 操作类型			*/
		},
		{
			0x223344556677ULL,	/* ID				*/
			9,			/* 名称长度，最大 20		*/
			"名称测试2",		/* 名称				*/
			0x776655443322ULL,	/* 对应值			*/
			5,			/* 类别				*/
			2,			/* 显示顺序			*/
			0x03,			/* 操作类型			*/
		},
		{
			0,			/* ID				*/
			12,			/* 名称长度，最大 20		*/
			"名称再次测试",		/* 名称				*/
			0x887766554433ULL,	/* 对应值			*/
			5,			/* 类别				*/
			1,			/* 显示顺序			*/
			0x01,			/* 操作类型			*/
		},
	};

	printf("\n>>> 服务器收到数据 <<<\n");

	printf("信令编号: 0x%.2x\n", s10wh->cmd);
	printf("最后更新时间 : 0x%.8x\n", ntohl(s10wh->last_update));

	printf("----- 服务器构造信息 -----\n");
	s10rh->cmd = 0x10;
	s10rh->record_count = htons(sizeof(s10data) / sizeof(s10data[0]));
	s10rh->last_update = htonl(0x2222000f);
	printf("信令编号: 0x%.2x\n", s10rh->cmd);
	printf("总记录数: %d\n", ntohs(s10rh->record_count));
	printf("最后更新时间 : 0x%.8x\n", ntohl(s10rh->last_update));
	p = (void *)s10rh->data;
	for (i=0; i<ntohs(s10rh->record_count); i++) {
		printf("\n--- 第 %d 条数据 ---\n", i+1);
		id = (void *)p;
		*id = s10data[i].id;
		printf("ID: %lu (0x%.16lx)\n",
			cvt64(*id), cvt64(*id));
		p += 8;

		*p = s10data[i].name_len;
		printf("名称长度: %d\n", *p);
		p++;

		memcpy(p, s10data[i].name, s10data[i].name_len);
		printf("名称: ");
		print_str((char *)p, *(p-1));
		p += *(p-1);

		*p = s10data[i].val;
		id = (void *)p;
		printf("对应值: %lu (0x%.16lx)\n",
			cvt64(*id), cvt64(*id));
		p += 8;

		*p = s10data[i].sort;
		printf("类别: 0x%.2x%s\n", *p,
			*p == 0x05 ? handle_type[*p] : handle_type[0]);
		p++;

		*p = s10data[i].seq;
		printf("显示顺序: %d\n", *p);
		p++;

		*p = s10data[i].oper_type;
		printf("操作类型: 0x%.2x%s\n", *p,
			*p > 0x03 ? oper_type[0] : oper_type[*p]);
		p++;
	}

	return (void *)p - (void *)buf;
}

int
syj_11 (uint8_t *buf)
{
	uint8_t *p;
	int i;
	uint64_t *id;
	struct s_11_wrq *s11wh = (void *)buf;
	struct s_11_rrq *s11rh = (void *)buf;
	struct s_11_rrq_data s11data[] = {
		{
			0x112233445566ULL,	/* ID				*/
			"0001",			/* 类别编号			*/
			16,			/* 证件编号名称长度		*/
			"证件编号名称测试",	/* 证件编号名称			*/
			0x02,			/* 操作类型			*/
		},
		{
			0x223344556677ULL,	/* ID				*/
			"0002",			/* 类别编号			*/
			17,			/* 证件编号名称长度		*/
			"证件编号名称测试2",	/* 证件编号名称			*/
			0x03,			/* 操作类型			*/
		},
		{
			0,			/* ID				*/
			"0003",			/* 类别编号			*/
			20,			/* 证件编号名称长度		*/
			"证件编号名称再次测试",	/* 证件编号名称			*/
			0x01,			/* 操作类型			*/
		},
	};

	printf("\n>>> 服务器收到数据 <<<\n");

	printf("信令编号: 0x%.2x\n", s11wh->cmd);
	printf("最后更新时间 : 0x%.8x\n", ntohl(s11wh->last_update));

	printf("----- 服务器构造信息 -----\n");
	s11rh->cmd = 0x11;
	s11rh->record_count = htons(sizeof(s11data) / sizeof(s11data[0]));
	s11rh->last_update = htonl(0x2222000f);
	printf("信令编号: 0x%.2x\n", s11rh->cmd);
	printf("总记录数: %d\n", ntohs(s11rh->record_count));
	printf("最后更新时间 : 0x%.8x\n", ntohl(s11rh->last_update));
	p = (void *)s11rh->data;
	for (i=0; i<ntohs(s11rh->record_count); i++) {
		printf("\n--- 第 %d 条数据 ---\n", i+1);
		id = (void *)p;
		*id = s11data[i].id;
		printf("ID: %lu (0x%.16lx)\n",
			cvt64(*id), cvt64(*id));
		p += 8;

		memcpy(p, s11data[i].type_num, sizeof(s11data[i].type_num));
		printf("类别编号: ");
		print_str((char *)p, 4);
		p += 4;

		*p = s11data[i].license_len;
		printf("证件编号名称长度: %d\n", *p);
		p++;

		memcpy(p, s11data[i].license, s11data[i].license_len);
		printf("证件编号名称: ");
		print_str((char *)p, *(p-1));
		p += *(p-1);

		*p = s11data[i].oper_type;
		printf("操作类型: 0x%.2x%s\n", *p,
			*p > 0x03 ? oper_type[0] : oper_type[*p]);
		p++;
	}

	return (void *)p - (void *)buf;
}

int
syj_12 (uint8_t *buf)
{
	struct s_12_rrq *s12rh = (void *)buf;
	struct s_12_wrq *s12wh = (void *)buf;
	uint32_t *last_update;
	uint8_t *result_data;
	int len = 0;
	int i;
	char *update_data[] = {
		"[小票信息]",
		"[商家信息]",
		"[销售价格]",
		"[仓库信息]",
		"[员工信息]",
		"[客户信息]",
		"[供应商信息]",
		"[商品基本信息]",
		"[商品单位信息]",
		"[商品批次信息]",
		"[待办任务]",
		"[系统消息]",
		"[商品类别信息]",
		"[数据字典]",
		"[商品类别/证件编号名称对照]",
	};
	uint8_t result[] = {
		0x00,
		0x00,
		0x00,
		0x00,
		0x01,
		0x01,
		0x01,
		0x00,
		0x00,
		0x00,
		0x01,
		0x01,
		0x00,
		0x01,
		0x00,
	};

	printf("\n>>> 服务器收到数据 <<<\n");

	printf("信令编号: 0x%.2x\n", s12wh->cmd);
	printf("商家 ID: %lu (0x%.16lx)\n",
		cvt64(s12wh->dept_id),
		cvt64(s12wh->dept_id));
	last_update = &s12wh->last_update2;

	int num = sizeof(result) / sizeof(result[0]);
	for (i=0; i<num; i++) {
		printf("%s: 最后更新时间: 0x%.8x\n",
			update_data[i], ntohl(*last_update));
		last_update++;
	}

	printf("----- 服务器构造信息 -----\n");
	s12rh->cmd = 0x12;
	printf("信令编号: 0x%.2x\n", s12rh->cmd);
	result_data = &s12rh->result1;
	for (i=0; i<num; i++) {
		*result_data = result[i];
		printf("%s同步请求结果: 0x%.2x %s\n",
			update_data[i],
			result[i],
			result[i]? "（需要更新）" : "（无需更新）");
		result_data++;
	}

	len = num * sizeof(s12rh->result1) + sizeof(s12rh->cmd);
	return len;
}

int
syj_13 (uint8_t *buf)
{
	struct s_13_wrq *s13wh;
	uint8_t *p, sort;
	int i;
	s13wh = (void *)buf;

	printf("\n>>> 服务器收到数据 <<<\n");

	printf("信令编号: 0x%.2x\n", s13wh->cmd);
	printf("商家 ID: %lu (0x%.16lx)\n",
		cvt64(s13wh->dept_id),
		cvt64(s13wh->dept_id));
	printf("删除记录数: %u\n", ntohs(s13wh->del_count));
	printf("处理记录数: %u\n", ntohs(s13wh->deal_count));
	p = (void *)s13wh->data;

	for (i=0; i<ntohs(s13wh->del_count); i++) {
		printf("\n--- 第 %d 条删除记录 ---\n", i+1);
		printf("任务 ID: %lu (0x%.16lx)\n",
			cvt64(*((uint64_t *)p)),
			cvt64(*((uint64_t *)p)));
		p += 8;
	}

	for (i=0; i<ntohs(s13wh->deal_count); i++) {
		printf("\n--- 第 %d 条处理记录 ---\n", i+1);
		printf("任务 ID: %lu (0x%.16lx)\n",
			cvt64(*((uint64_t *)p)),
			cvt64(*((uint64_t *)p)));
		p += 8;

		printf("任务类型: 0x%.2x, %s\n", *p,
			*p > 0x03 ? sort_type[0] : sort_type[*p]);
		sort = *p;
		p++;

		printf("任务参数长度: %d\n", *p);
		p++;

		printf("任务参数: ");
		print_str((char *)p, *(p-1));
		p += *(p-1);

		switch (sort) {
			case 0x01:
				printf("进货单编号: ");
				print_str((char *)p, 22);
				p += 22;

				printf("入库仓库 ID: %lu (0x%.16lx)\n",
					cvt64(*((uint64_t *)p)),
					cvt64(*((uint64_t *)p)));
				p += 8;

				printf("入库仓库名称长度: %d\n", *p);
				p++;

				printf("入库仓库名称: ");
				print_str((char *)p, *(p-1));
				p += *(p-1);

				printf("登录人 ID: %lu (0x%.16lx)\n",
					cvt64(*((uint64_t *)p)),
					cvt64(*((uint64_t *)p)));
				p += 8;

				printf("登录人姓名长度: %d\n", *p);
				p++;

				printf("登录人姓名: ");
				print_str((char *)p, *(p-1));
				p += *(p-1);
				break;
			default:
				printf("错误的任务类型: 0x%.2x\n", sort);
				break;
		}
	}

	return 0;
}

int
syj_14 (uint8_t *buf)
{
	struct s_14_wrq *s14wh;
	s14wh = (void *)buf;

	printf("\n>>> 服务器收到数据 <<<\n");

	printf("信令编号: 0x%.2x\n", s14wh->cmd);
	printf("商家 ID: %lu (0x%.16lx)\n",
		cvt64(s14wh->dept_id),
		cvt64(s14wh->dept_id));
	printf("被索到商家证号码长度: %d\n", s14wh->num_len);
	printf("被索到商家证号码: ");
	print_str((void *)&s14wh->num, s14wh->num_len);

	return 0;
}

int
syj_15 (uint8_t *buf)
{
	struct s_15_wrq *s15wh;
	uint64_t *id;
	s15wh = (void *)buf;

	printf("\n>>> 服务器收到数据 <<<\n");

	printf("信令编号: 0x%.2x\n", s15wh->cmd);
	printf("商家 ID: %lu (0x%.16lx)\n",
		cvt64(s15wh->dept_id),
		cvt64(s15wh->dept_id));
	printf("被索产品证条码长度: %d\n", s15wh->code_len);
	printf("被索产品证条码: ");
	print_str((void *)&s15wh->code, s15wh->code_len);
	id = (void *)&s15wh->code + s15wh->code_len;
	printf("供应商 ID: %lu (0x%.16lx)\n",
		cvt64(*id), cvt64(*id));

	return 0;
}

int
syj_16 (uint8_t *buf)
{
	struct s_16_wrq *s16wh;
	struct s_16_wrq_data data;
	uint64_t *id = NULL;
	uint32_t *sum_count = NULL, *count = NULL;
	uint16_t *quality_date;
	int i, money_len, len = 0;

	printf("\n>>> 服务器接收到的数据 <<<\n");

	printf("信令编号: 0x%.2x\n", buf[0]);
	len++;

	id = (void *)buf + len;
	printf("商家 ID: %lu (0x%.16lx)\n",
		cvt64(*id), cvt64(*id));
	len += sizeof(*id);

	printf("单据编号: ");
	print_str((void *)buf + len, sizeof(s16wh->num));
	len += sizeof(s16wh->num);

	printf("商家名称长度: %d\n", buf[len]);
	printf("商家名称: ");
	print_str((void *)buf + 1 + len, buf[len]);
	len = len + 1 + buf[len];

	id = (void *)buf + len;
	printf("供应商 ID: %lu (0x%.16lx)\n",
		cvt64(*id), cvt64(*id));
	len += sizeof(*id);
	printf("供应商名称长度: %d\n", buf[len]);
	printf("供应商名称: ");
	print_str((void *)buf + 1 + len, buf[len]);
	len = len + 1 + buf[len];

	id = (void *)buf + len;
	printf("入库仓库 ID: %lu (0x%.16lx)\n",
		cvt64(*id), cvt64(*id));
	len += sizeof(*id);
	printf("入库仓库名称长度: %d\n", buf[len]);
	printf("入库仓库名称: ");
	print_str((void *)buf + 1 + len, buf[len]);
	len = len + 1 + buf[len];

	id = (void *)buf + len;
	printf("经手人 ID: %lu (0x%.16lx)\n",
		cvt64(*id), cvt64(*id));
	len += sizeof(*id);
	printf("经手人姓名长度: %d\n", buf[len]);
	printf("经手人姓名: ");
	print_str((void *)buf + 1 + len, buf[len]);
	len = len + 1 + buf[len];

	sum_count = (void *)buf + len;
	printf("数量合计: %d\n", ntohl(*sum_count));
	len += sizeof(*sum_count);

	printf("应付款金额: ");
	format_price((void *)buf + len, sizeof(s16wh->pay_money));
	len += sizeof(s16wh->pay_money);
	printf("实际付款金额: ");
	format_price((void *)buf + len, sizeof(s16wh->fact_money));
	len += sizeof(s16wh->fact_money);

	id = (void *)buf + len;
	printf("制单人 ID: %lu (0x%.16lx)\n",
		cvt64(*id), cvt64(*id));
	len += sizeof(*id);
	printf("制单人姓名长度: %d\n", buf[len]);
	printf("制单人姓名: ");
	print_str((void *)buf + 1 + len, buf[len]);
	len = len + 1 + buf[len];

	printf("制单日期: ");
	print_str((void *)buf + len, sizeof(s16wh->create_date));
	len += sizeof(s16wh->create_date);

	printf("优惠金额: ");
	format_price((void *)buf + len, sizeof(s16wh->fav_money));
	len += sizeof(s16wh->fav_money);

	printf("折前金额合计: ");
	format_price((void *)buf + len, sizeof(s16wh->rebate_front_money));
	len += sizeof(s16wh->rebate_front_money);

	printf("折后金额合计: ");
	format_price((void *)buf + len, sizeof(s16wh->rebate_back_money));
	len += sizeof(s16wh->rebate_back_money);

	int record_count;
	record_count = buf[len];
	printf("明细记录数: %d\n", buf[len]);
	len++;

	printf("--- 进货单明细 ---\n");
	for (i=0; i<record_count; i++) {
		printf("\n--- 第 %d 条数据 ---\n", i+1);
		id = (void *)buf +len;
		printf("商品 ID: %lu (0x%.16lx)\n",
			cvt64(*id), cvt64(*id));
		len += sizeof(*id);

		printf("是否赠品: 0x%.2x%s\n", buf[len], (buf[len] > 0x02) ?
			is_give[0] :is_give[(uint8_t)buf[len]]);
		len += sizeof(buf[len]);

		count = (void *)buf + len;
		printf("数量: %d\n", ntohl(*count));
		len += sizeof(*count);

		printf("单价: ");
		money_len = sizeof(data.price);
		format_price((void *)buf + len, money_len);
		len += money_len;

		id = (void *)buf + len;
		printf("单位 ID: %lu (0x%.16lx)\n",
			cvt64(*id), cvt64(*id));
		len += sizeof(*id);

		printf("保质单位: 0x%.2x%s\n", buf[len], (buf[len] > 0x03) ?
			quality_units[0] : quality_units[(uint8_t)buf[len]]);
		len += sizeof(data.quality_unit);

		quality_date = (void *)buf + len;
		printf("保质期: 0x%.2x\n", ntohs(*quality_date));
		len += sizeof(*quality_date);

		int create_len = buf[len];
		printf("生产日期长度: %d\n", create_len);
		printf("生产日期: ");
		print_str((void *)buf + len + 1, create_len);
		len = len + 1 + create_len;

		int exceed_len = buf[len];
		printf("过期日期长度: %d\n", exceed_len);
		printf("过期日期: ");
		print_str((void *)buf + len + 1, exceed_len);
		len = len + 1 + exceed_len;

		int batch_len = buf[len];
		printf("批次长度: %d\n", batch_len);
		printf("批次: ");
		print_str((void *)buf + len + 1, batch_len);
		len = len + 1 + batch_len;

		printf("折扣: %d\n", buf[len]);
		len += sizeof(buf[len]);
	}

	return 0;
}

int
syj_17 (uint8_t *buf)
{
	struct s_17_wrq *s17wh;
	struct s_17_wrq_data data;
	uint64_t *id = NULL;
	uint32_t *sum_count = NULL, *count = NULL;
	uint16_t *quality_date;
	int i, money_len, len = 0;

	printf("\n>>> 服务器接收到的数据 <<<\n");

	printf("信令编号: 0x%.2x\n", buf[0]);
	len++;

	id = (void *)buf + len;
	printf("商家 ID: %lu (0x%.16lx)\n",
		cvt64(*id), cvt64(*id));
	len += sizeof(*id);

	printf("单据编号: ");
	print_str((void *)buf + len, sizeof(s17wh->num));
	len += sizeof(s17wh->num);

	printf("商家名称长度: %d\n", buf[len]);
	printf("商家名称: ");
	print_str((void *)buf + 1 + len, buf[len]);
	len = len + 1 + buf[len];

	id = (void *)buf + len;
	printf("客户 ID: %lu (0x%.16lx)\n",
		cvt64(*id), cvt64(*id));
	len += sizeof(*id);

	printf("客户名称长度: %d\n", buf[len]);
	printf("客户名称: ");
	print_str((void *)buf + 1 + len, buf[len]);
	len = len + 1 + buf[len];

	id = (void *)buf + len;
	printf("入库仓库 ID: %lu (0x%.16lx)\n",
		cvt64(*id), cvt64(*id));
	len += sizeof(*id);

	printf("入库仓库名称长度: %d\n", buf[len]);
	printf("入库仓库名称: ");
	print_str((void *)buf + 1 + len, buf[len]);
	len = len + 1 + buf[len];

	id = (void *)buf + len;
	printf("经手人 ID: %lu (0x%.16lx)\n",
		cvt64(*id), cvt64(*id));
	len += sizeof(*id);

	printf("经手人姓名长度: %d\n", buf[len]);
	printf("经手人姓名: ");
	print_str((void *)buf + 1 + len, buf[len]);
	len = len + 1 + buf[len];

	sum_count = (void *)buf + len;
	printf("数量合计: %d\n", ntohl(*sum_count));
	len += sizeof(*sum_count);

	printf("应付款金额: ");
	format_price((void *)buf + len, sizeof(s17wh->pay_money));
	len += sizeof(s17wh->pay_money);

	printf("实际付款金额: ");
	format_price((void *)buf + len, sizeof(s17wh->fact_money));
	len += sizeof(s17wh->fact_money);

	id = (void *)buf + len;
	printf("制单人 ID: %lu (0x%.16lx)\n",
		cvt64(*id), cvt64(*id));
	len += sizeof(*id);

	printf("制单人姓名长度: %d\n", buf[len]);
	printf("制单人姓名: ");
	print_str((void *)buf + 1 + len, buf[len]);
	len = len + 1 + buf[len];

	printf("制单日期: ");
	print_str((void *)buf + len, sizeof(s17wh->create_date));
	len += sizeof(s17wh->create_date);

	printf("优惠金额: ");
	format_price((void *)buf + len, sizeof(s17wh->fav_money));
	len += sizeof(s17wh->fav_money);

	printf("折前金额合计: ");
	format_price((void *)buf + len, sizeof(s17wh->rebate_front_money));
	len += sizeof(s17wh->rebate_front_money);

	printf("折后金额合计: ");
	format_price((void *)buf + len, sizeof(s17wh->rebate_back_money));
	len += sizeof(s17wh->rebate_back_money);

	uint8_t record_count;
	record_count = buf[len];
	printf("明细记录数: %d\n", record_count);
	len++;

	printf("--- 销售单明细 ---\n");
	for (i=0; i<record_count; i++) {
		printf("\n--- 第 %d 条数据 ---\n", i+1);
		id = (void *)buf +len;
		printf("商品 ID: %lu (0x%.16lx)\n",
			cvt64(*id), cvt64(*id));
		len += sizeof(*id);

		printf("是否赠品: 0x%.2x%s\n", buf[len], (buf[len] > 0x02) ?
			is_give[0] :is_give[(uint8_t)buf[len]]);
		len += sizeof(buf[len]);

		count = (void *)buf + len;
		printf("数量: %d\n", ntohl(*count));
		len += sizeof(*count);

		printf("单价: ");
		money_len = sizeof(data.price);
		format_price((void *)buf + len, money_len);
		len += money_len;

		id = (void *)buf + len;
		printf("单位 ID: %lu (0x%.16lx)\n",
			cvt64(*id), cvt64(*id));
		len += sizeof(*id);
		printf("保质单位: 0x%.2x%s\n", buf[len], (buf[len] > 0x03) ?
			quality_units[0] : quality_units[(uint8_t)buf[len]]);
		len += sizeof(data.quality_unit);

		quality_date = (void *)buf + len;
		printf("保质期: 0x%.2x\n", ntohs(*quality_date));
		len += sizeof(*quality_date);

		int create_len = buf[len];
		printf("生产日期长度: %d\n", create_len);
		printf("生产日期: ");
		print_str((void *)buf + len + 1, create_len);
		len = len + 1 + create_len;

		int exceed_len = buf[len];
		printf("过期日期长度: %d\n", exceed_len);
		printf("过期日期: ");
		print_str((void *)buf + len + 1, exceed_len);
		len = len + 1 + exceed_len;

		int batch_len = buf[len];
		printf("批次长度: %d\n", batch_len);
		printf("批次: ");
		print_str((void *)buf + len + 1, batch_len);
		len = len + 1 + batch_len;

		printf("折扣: %d\n", buf[len]);
		len += sizeof(buf[len]);
	}

	return 0;
}

int
syj_18 (uint8_t *buf)
{
	struct s_18_wrq *s18wh;
	struct s_18_wrq_data data;
	uint64_t *id = NULL;
	uint32_t *sum_count = NULL, *count = NULL;
	uint16_t *quality_date;
	int i, len = 0;

	printf("\n>>> 服务器接收到的数据 <<<\n");

	printf("信令编号: 0x%.2x\n", buf[0]);
	len++;

	id = (void *)buf + len;
	printf("商家 ID: %lu (0x%.16lx)\n",
		cvt64(*id), cvt64(*id));
	len += sizeof(*id);

	printf("单据编号: ");
	print_str((void *)buf + len, sizeof(s18wh->num));
	len += sizeof(s18wh->num);

	printf("商家名称长度: %d\n", buf[len]);
	printf("商家名称: ");
	print_str((void *)buf + 1 + len, buf[len]);
	len = len + 1 + buf[len];

	id = (void *)buf + len;
	printf("出库仓库 ID: %lu (0x%.16lx)\n",
		cvt64(*id), cvt64(*id));
	len += sizeof(*id);

	printf("出库仓库名称长度: %d\n", buf[len]);
	printf("出库仓库名称: ");
	print_str((void *)buf + 1 + len, buf[len]);
	len = len + 1 + buf[len];

	id = (void *)buf + len;
	printf("入库仓库 ID: %lu (0x%.16lx)\n",
		cvt64(*id), cvt64(*id));
	len += sizeof(*id);

	printf("入库仓库名称长度: %d\n", buf[len]);
	printf("入库仓库名称: ");
	print_str((void *)buf + 1 + len, buf[len]);
	len = len + 1 + buf[len];

	id = (void *)buf + len;
	printf("经手人 ID: %lu (0x%.16lx)\n",
		cvt64(*id), cvt64(*id));
	len += sizeof(*id);

	printf("经手人姓名长度: %d\n", buf[len]);
	printf("经手人姓名: ");
	print_str((void *)buf + 1 + len, buf[len]);
	len = len + 1 + buf[len];

	sum_count = (void *)buf + len;
	printf("退市数量合计: %d\n", ntohl(*sum_count));
	len += sizeof(*sum_count);

	id = (void *)buf + len;
	printf("制单人 ID: %lu (0x%.16lx)\n",
		cvt64(*id), cvt64(*id));
	len += sizeof(*id);

	printf("制单人姓名长度: %d\n", buf[len]);
	printf("制单人姓名: ");
	print_str((void *)buf + 1 + len, buf[len]);
	len = len + 1 + buf[len];

	printf("制单日期: ");
	print_str((void *)buf + len, sizeof(s18wh->create_date));
	len += sizeof(s18wh->create_date);

	uint8_t record_count;
	record_count = buf[len];
	printf("明细记录数: %d\n", record_count);
	len++;

	printf("--- 退市单明细 ---\n");
	for (i=0; i<record_count; i++) {
		printf("\n--- 第 %d 条数据 ---\n", i+1);
		id = (void *)buf +len;
		printf("商品 ID: %lu (0x%.16lx)\n",
			cvt64(*id), cvt64(*id));
		len += sizeof(*id);

		count = (void *)buf + len;
		printf("退市数量: %d\n", ntohl(*count));
		len += sizeof(*count);

		printf("保质单位: 0x%.2x%s\n", buf[len], (buf[len] > 0x03) ?
			quality_units[0] : quality_units[(uint8_t)buf[len]]);
		len += sizeof(data.quality_unit);

		quality_date = (void *)buf + len;
		printf("保质期: %d\n", ntohs(*quality_date));
		len += sizeof(*quality_date);

		int create_len = buf[len];
		printf("生产日期长度: %d\n", create_len);
		printf("生产日期: ");
		print_str((void *)buf + len + 1, create_len);
		len = len + 1 + create_len;

		int exceed_len = buf[len];
		printf("过期日期长度: %d\n", exceed_len);
		printf("过期日期: ");
		print_str((void *)buf + len + 1, exceed_len);
		len = len + 1 + exceed_len;

		int batch_len = buf[len];
		printf("批次长度: %d\n", batch_len);
		printf("批次: ");
		print_str((void *)buf + len + 1, batch_len);
		len = len + 1 + batch_len;
	}

	return 0;
}

int
syj_19 (uint8_t *buf)
{
	struct s_19_wrq *s19wh;
	struct s_19_wrq_data data;
	uint64_t *id = NULL;
	uint32_t *sum_count = NULL, *count = NULL;
	uint16_t *quality_date;
	int i, len = 0;

	printf("\n>>> 服务器接收到的数据 <<<\n");

	printf("信令编号: 0x%.2x\n", buf[0]);
	len++;

	id = (void *)buf + len;
	printf("商家 ID: %lu (0x%.16lx)\n",
		cvt64(*id), cvt64(*id));
	len += sizeof(*id);

	printf("单据编号: ");
	print_str((void *)buf + len, sizeof(s19wh->num));
	len += sizeof(s19wh->num);

	printf("商家名称长度: %d\n", buf[len]);
	printf("商家名称: ");
	print_str((void *)buf + 1 + len, buf[len]);
	len = len + 1 + buf[len];

	id = (void *)buf + len;
	printf("禁售仓库 ID: %lu (0x%.16lx)\n",
		cvt64(*id), cvt64(*id));
	len += sizeof(*id);

	printf("禁售仓库名称长度: %d\n", buf[len]);
	printf("禁售仓库名称: ");
	print_str((void *)buf + 1 + len, buf[len]);
	len = len + 1 + buf[len];

	id = (void *)buf + len;
	printf("经手人 ID: %lu (0x%.16lx)\n",
		cvt64(*id), cvt64(*id));
	len += sizeof(*id);

	printf("经手人姓名长度: %d\n", buf[len]);
	printf("经手人姓名: ");
	print_str((void *)buf + 1 + len, buf[len]);
	len = len + 1 + buf[len];

	printf("处理方式: 0x%.2x%s\n", buf[len],
		(buf[len] > 0x05)?
		deal_way[0]:deal_way[(uint8_t)buf[len]]);
	len += sizeof(buf[len]);

	sum_count = (void *)buf + len;
	printf("退市数量合计: %d\n", ntohl(*sum_count));
	len += sizeof(*sum_count);

	id = (void *)buf + len;
	printf("制单人 ID: %lu (0x%.16lx)\n",
		cvt64(*id), cvt64(*id));
	len += sizeof(*id);

	printf("制单人姓名长度: %d\n", buf[len]);
	printf("制单人姓名: ");
	print_str((void *)buf + 1 + len, buf[len]);
	len = len + 1 + buf[len];

	printf("制单日期: ");
	print_str((void *)buf + len, sizeof(s19wh->create_date));
	len += sizeof(s19wh->create_date);

	printf("处理方式描述长度: %d\n", buf[len]);
	printf("处理方式描述: ");
	print_str((void *)buf + 1 + len, buf[len]);
	len = len + 1 + buf[len];

	uint8_t record_count;
	record_count = buf[len];
	printf("明细记录数: %d\n", record_count);
	len++;

	printf("--- 退市单明细 ---\n");
	for (i=0; i<record_count; i++) {
		printf("\n--- 第 %d 条数据 ---\n", i+1);
		id = (void *)buf +len;
		printf("商品 ID: %lu (0x%.16lx)\n",
			cvt64(*id), cvt64(*id));
		len += sizeof(*id);

		count = (void *)buf + len;
		printf("处理数量: %d\n", ntohl(*count));
		len += sizeof(*count);

		printf("保质单位: 0x%.2x%s\n", buf[len], (buf[len] > 0x03) ?
			quality_units[0] : quality_units[(uint8_t)buf[len]]);
		len += sizeof(data.quality_unit);

		quality_date = (void *)buf + len;
		printf("保质期: %d\n", ntohs(*quality_date));
		len += sizeof(*quality_date);

		int create_len = buf[len];
		printf("生产日期长度: %d\n", create_len);
		printf("生产日期: ");
		print_str((void *)buf + len + 1, create_len);
		len = len + 1 + create_len;

		int exceed_len = buf[len];
		printf("过期日期长度: %d\n", exceed_len);
		printf("过期日期: ");
		print_str((void *)buf + len + 1, exceed_len);
		len = len + 1 + exceed_len;

		int batch_len = buf[len];
		printf("批次长度: %d\n", batch_len);
		printf("批次: ");
		print_str((void *)buf + len + 1, batch_len);
		len = len + 1 + batch_len;
	}

	return 0;
}

int
syj_1a (uint8_t *buf)
{
	struct s_1a_wrq *s1awh;
	int i;
	char *p;

	printf("\n>>> 服务器接收到的数据 <<<\n");

	s1awh = (void *)buf;
	printf("信令编号: 0x%.2x\n", s1awh->cmd);
	printf("商家 ID: %lu (0x%.16lx)\n",
		cvt64(s1awh->dept_id),
		cvt64(s1awh->dept_id));
	printf("总记录数: %u\n", ntohs(s1awh->record_count));
	p = (void *)s1awh->data;

	for (i=0; i<ntohs(s1awh->record_count); i++) {
		printf("\n--- 第 %d 条数据 ---\n", i+1);
		printf("商品 ID: %lu (0x%.16lx)\n",
			cvt64(*((uint64_t *)p)),
			cvt64(*((uint64_t *)p)));
		p += 8;

		printf("基础商品 ID: %lu (0x%.16lx)\n",
			cvt64(*((uint64_t *)p)),
			cvt64(*((uint64_t *)p)));
		p += 8;

		printf("商品条码长度: %d\n", *p);
		p++;

		printf("商品条码: ");
		print_str(p, *(p-1));
		p += *(p-1);

		printf("商品名称长度: %d\n", *p);
		p++;

		printf("商品名称: ");
		print_str(p, *(p-1));
		p += *(p-1);

		printf("基本单位名称长度: %d\n", *p);
		p++;

		printf("基本单位名称: ");
		print_str(p, *(p-1));
		p += *(p-1);

		printf("商品类别 ID: %lu (0x%.16lx)\n",
			cvt64(*((uint64_t *)p)),
			cvt64(*((uint64_t *)p)));
		p += 8;

		printf("类别编码长度: %d\n", *p);
		p++;

		printf("类别编码: ");
		print_str(p, *(p-1));
		p += *(p-1);

		printf("规格长度: %d\n", *p);
		p++;

		printf("规格: ");
		print_str(p, *(p-1));
		p += *(p-1);

		printf("进货参考价: ");
		format_price(p, 10);
		p += 10;

		printf("零售价: ");
		format_price(p, 10);
		p += 10;

		printf("会员价: ");
		format_price(p, 10);
		p += 10;

		printf("经销商价: ");
		format_price(p, 10);
		p += 10;

		printf("证件编号长度: %d\n", *p);
		p++;

		printf("证件编号: ");
		print_str(p, *(p-1));
		p += *(p-1);

		printf("操作类型: 0x%.2x%s\n", *p,
			(*p > 0x03) ?
			oper_type[0] :
			oper_type[(uint8_t)*p]);
		p++;
	}

	return 0;
}

int
syj_1b (uint8_t *buf)
{
	struct s_1b_wrq *s1bwh;
	int i;
	char *p;

	printf("\n>>> 服务器接收到的数据 <<<\n");

	s1bwh = (void *)buf;
	printf("信令编号: 0x%.2x\n", s1bwh->cmd);
	printf("商家 ID: %lu (0x%.16lx)\n",
		cvt64(s1bwh->dept_id),
		cvt64(s1bwh->dept_id));
	printf("总记录数: %u\n", ntohs(s1bwh->record_count));
	p = (void *)s1bwh->data;

	for (i=0; i<ntohs(s1bwh->record_count); i++) {
		printf("\n--- 第 %d 条数据 ---\n", i+1);
		printf("商品 ID: %lu (0x%.16lx)\n",
			cvt64(*((uint64_t *)p)),
			cvt64(*((uint64_t *)p)));
		p += 8;

		printf("条码长度: %d\n", *p);
		p++;

		printf("条码: ");
		print_str(p, *(p-1));
		p += *(p-1);

		printf("单位名称长度: %d\n", *p);
		p++;

		printf("单位名称: ");
		print_str(p, *(p-1));
		p += *(p-1);

		printf("单位关系: %u (0x%.4x)\n",
			ntohs(*((uint16_t *)p)),
			ntohs(*((uint16_t *)p)));
		p += 2;

		printf("进货参考价: ");
		format_price(p, 10);
		p += 10;

		printf("零售价: ");
		format_price(p, 10);
		p += 10;

		printf("会员价: ");
		format_price(p, 10);
		p += 10;

		printf("经销商价: ");
		format_price(p, 10);
		p += 10;

		printf("操作类型: 0x%.2x%s\n", *p,
			(*p > 0x03) ?
			oper_type[0] :
			oper_type[(uint8_t)*p]);
		p++;
	}

	return 0;
}

int
syj_1c (uint8_t *buf)
{
	struct s_1c_rrq *s1crh;
	struct s_1c_wrq *s1cwh;
	char *p;

	printf("\n>>> 服务器接收到的数据 <<<\n");

	s1crh = (void *)buf;
	s1cwh = (void *)buf;
	printf("信令编号: 0x%.2x\n", s1cwh->cmd);
	printf("商家 ID: %lu (0x%.16lx)\n",
		cvt64(s1cwh->dept_id),
		cvt64(s1cwh->dept_id));
	p = (void *)&s1cwh->code_len;
	printf("商品条码长度: %d\n", *p);
	p++;
	printf("商品条码: ");
	print_str(p, *(p-1));

	printf("----- 服务器构造信息 -----\n");
	s1crh->cmd = 0x1c;
	s1crh->record_count = 1;
	printf("信令编号: 0x%.2x\n", s1crh->cmd);
	printf("总记录数: %d\n", s1crh->record_count);
	p = (void *)&s1crh->code_len;

	*p = 8;					/* 商品条码长度		*/
	printf("商品条码长度: %d\n", *p);
	p++;

	memcpy(p, "A1S2D3F4", *(p-1));		/* 商品条码		*/
	printf("商品条码: ");
	print_str(p, *(p-1));
	p += *(p-1);

	*p = 12;				/* 商品名称长度		*/
	printf("商品名称长度: %d\n", *p);
	p++;

	memcpy(p, "商品名称测试", *(p-1));	/* 商品名称		*/
	printf("商品名称: ");
	print_str(p, *(p-1));
	p += *(p-1);

	*p = 2;					/* 基本单位名称长度	*/
	printf("基本单位名称长度: %d\n", *p);
	p++;

	memcpy(p, "吨", *(p-1));		/* 基本单位名称		*/
	printf("基本单位名称: ");
	print_str(p, *(p-1));
	p += *(p-1);

	*((uint64_t *)p) = cvt64(12345);	/* 商品类别 ID		*/
	printf("商品类别 ID: %lu (0x%.16lx)\n",
		cvt64(*((uint64_t *)p)),
		cvt64(*((uint64_t *)p)));
	p += 8;

	*p = 11;				/* 规格长度		*/
	printf("规格长度: %d\n", *p);
	p++;

	memcpy(p, "Cross-rapid", *(p-1));	/* 规格			*/
	printf("规格: ");
	print_str(p, *(p-1));
	p += *(p-1);

	*p = 8;					/* 证件编号长度		*/
	printf("证件编号长度: %d\n", *p);
	p++;

	memcpy(p, "ABCD1234", *(p-1));		/* 证件编号		*/
	printf("证件编号: ");
	print_str(p, *(p-1));
	p += *(p-1);

	return (void *)p - (void *)buf;
}

int
syj_1d (uint8_t *buf)
{
	uint8_t *p;
	int i;
	uint32_t *uint32;
	struct s_1d_wrq *s1dwh = (void *)buf;
	struct s_1d_rrq *s1drh = (void *)buf;
	struct s_1d_rrq_data s1ddata[] = {
		{
			6,			/* 商品名称长度			*/
			"食用盐",		/* 商品名称			*/
			2,			/* 单位名称长度			*/
			"袋",			/* 单位名称			*/
			1234,			/* 数量				*/
		},
		{
			6,			/* 商品名称长度			*/
			"食用油",		/* 商品名称			*/
			2,			/* 单位名称长度			*/
			"桶",			/* 单位名称			*/
			255,			/* 数量				*/
		},
		{
			4,			/* 商品名称长度			*/
			"糖果",		/* 商品名称			*/
			4,			/* 单位名称长度			*/
			"千克",			/* 单位名称			*/
			10,			/* 数量				*/
		},
	};

	printf("\n>>> 服务器收到数据 <<<\n");
	printf("信令编号: 0x%.2x\n", s1dwh->cmd);
	printf("商家 ID: %lu (0x%.16lx)\n",
		cvt64(s1dwh->dept_id),
		cvt64(s1dwh->dept_id));
	printf("销售 UUID: ");
	print_str(s1dwh->sale_uuid, sizeof(s1dwh->sale_uuid));

	printf("----- 服务器构造信息 -----\n");
	s1drh->cmd = 0x1d;
	s1drh->record_count = htons(sizeof(s1ddata) / sizeof(s1ddata[0]));
	printf("信令编号: 0x%.2x\n", s1drh->cmd);
	printf("总记录数: %d\n", ntohs(s1drh->record_count));
	p = (void *)s1drh->data;
	for (i=0; i<ntohs(s1drh->record_count); i++) {
		printf("\n--- 第 %d 条数据 ---\n", i+1);
		*p = s1ddata[i].name_len;
		printf("商品名称长度: %d\n", *p);
		p++;

		memcpy(p, s1ddata[i].name, s1ddata[i].name_len);
		printf("商品名称: ");
		print_str((char *)p, *(p-1));
		p += *(p-1);

		*p = s1ddata[i].unit_len;
		printf("单位名称长度: %d\n", *p);
		p++;

		memcpy(p, s1ddata[i].unit, s1ddata[i].unit_len);
		printf("单位名称: ");
		print_str((char *)p, *(p-1));
		p += *(p-1);

		uint32 = (void *)p;
		*uint32 = htonl(s1ddata[i].count);
		printf("数量: %u (0x%.8x)\n", ntohl(*uint32), ntohl(*uint32));
		p += 4;
	}

	return (void *)p - (void *)buf;
}

int
syj_1e (uint8_t *buf)
{
	struct s_1e_wrq *s1ewh = (void *)buf;
	struct s_1e_rrq *s1erh = (void *)buf;

	printf("\n>>> 服务器收到数据 <<<\n");
	printf("信令编号: 0x%.2x\n", s1ewh->cmd);
	printf("当前硬件版本号: ");
	print_ver(s1ewh->hard_version);
	printf("当前软件版本号: ");
	print_ver(s1ewh->soft_version);

	printf("----- 服务器构造信息 -----\n");
	s1erh->cmd = 0x1e;
	memcpy(s1erh->last_version, "101010", 6);
	s1erh->filesize = htonl(262144);
	s1erh->need_update = 0x01;
	printf("信令编号: 0x%.2x\n", s1erh->cmd);
	printf("最新软件版本号: ");
	print_ver(s1erh->last_version);
	printf("升级包大小: %u\n", ntohl(s1erh->filesize));
	printf("是否需要升级: 0x%.2x%s\n", s1erh->need_update,
		s1erh->need_update > 1 ?
		yes_no[0] :
		yes_no[s1erh->need_update]);

	return sizeof(struct s_1e_rrq);
}

int
syj_1f (uint8_t *buf)
{
	struct s_1f_wrq *s1fwh = (void *)buf;
	struct s_1f_rrq *s1frh = (void *)buf;
	uint8_t *p;
	int i;

	printf("\n>>> 服务器收到数据 <<<\n");
	printf("信令编号: 0x%.2x\n", s1fwh->cmd);
	printf("商家 ID: %lu (0x%.16lx)\n",
		cvt64(s1fwh->dept_id),
		cvt64(s1fwh->dept_id));
	printf("当前硬件版本号: ");
	print_ver(s1fwh->hard_version);
	printf("当前软件版本号: ");
	print_ver(s1fwh->cursoft_version);
	printf("请求软件版本号: ");
	print_ver(s1fwh->reqsoft_version);

	printf("----- 服务器构造信息 -----\n");
	s1frh->cmd = 0x1f;
	s1frh->filesize = htonl(324096);
	printf("信令编号: 0x%.2x\n", s1frh->cmd);
	printf("升级包大小: %u\n", ntohl(s1frh->filesize));
	p = (void *)s1frh->filecontent;
	for (i=0; i<ntohl(s1frh->filesize); i++)
		p[i] = (uint8_t)i;
	printf("升级包内容:\n");
	hexdump(s1frh->filecontent, ntohl(s1frh->filesize));
	p += ntohl(s1frh->filesize);

	return (void *)p - (void *)buf;
}

int
syj_20 (uint8_t *buf)
{
	struct s_20_wrq *s20wh;
	int i;
	uint8_t *p;

	printf("\n>>> 服务器接收到的数据 <<<\n");

	s20wh = (void *)buf;
	printf("信令编号: 0x%.2x\n", s20wh->cmd);
	printf("商家 ID: %lu (0x%.16lx)\n",
		cvt64(s20wh->dept_id),
		cvt64(s20wh->dept_id));
	printf("总记录数: %u\n", ntohs(s20wh->record_count));
	p = (void *)s20wh->data;

	for (i=0; i<ntohs(s20wh->record_count); i++) {
		printf("\n--- 第 %d 条数据 ---\n", i+1);
		printf("员工 ID: %lu (0x%.16lx)\n",
			cvt64(*((uint64_t *)p)),
			cvt64(*((uint64_t *)p)));
		p += 8;

		printf("员工编号: ");
		print_str((char *)p, 4);
		p += 4;

		printf("员工密码: ");
		print_str((char *)p, 6);
		p += 6;

		printf("员工姓名长度: %d\n", *p);
		p++;

		printf("员工姓名: ");
		print_str((char *)p, *(p-1));
		p += *(p-1);

		printf("操作类型: 0x%.2x%s\n", *p,
			(*p > 0x03) ? oper_type[0] : oper_type[*p]);
		p++;
	}

	return 0;
}

int
syj_21 (uint8_t *buf)
{
	struct s_21_wrq *s21wh;
	int i;
	uint8_t *p;

	printf("\n>>> 服务器接收到的数据 <<<\n");

	s21wh = (void *)buf;
	printf("信令编号: 0x%.2x\n", s21wh->cmd);
	printf("商家 ID: %lu (0x%.16lx)\n",
		cvt64(s21wh->dept_id),
		cvt64(s21wh->dept_id));
	printf("总记录数: %u\n", ntohs(s21wh->record_count));
	p = (void *)s21wh->data;

	for (i=0; i<ntohs(s21wh->record_count); i++) {
		printf("\n--- 第 %d 条数据 ---\n", i+1);
		printf("供应商 ID: %lu (0x%.16lx)\n",
			*((uint64_t *)p),
			*((uint64_t *)p));
		p += 8;

		printf("营业执照号码长度: %d\n", *p);
		p++;

		printf("营业执照号码: ");
		print_str((char *)p, *(p-1));
		p += *(p-1);

		printf("供应商名称长度: %d\n", *p);
		p++;

		printf("供应商名称: ");
		print_str((char *)p, *(p-1));
		p += *(p-1);

		printf("操作类型: 0x%.2x%s\n", *p,
			(*p > 0x03) ? oper_type[0] : oper_type[*p]);
		p++;
	}

	return 0;
}

int
syj_22 (uint8_t *buf)
{
	struct s_22_wrq *s22wh;
	int i;
	uint8_t *p;

	printf("\n>>> 服务器接收到的数据 <<<\n");

	s22wh = (void *)buf;
	printf("信令编号: 0x%.2x\n", s22wh->cmd);
	printf("商家 ID: %lu (0x%.16lx)\n",
		cvt64(s22wh->dept_id),
		cvt64(s22wh->dept_id));
	printf("总记录数: %u\n", ntohs(s22wh->record_count));
	p = (void *)s22wh->data;

	for (i=0; i<ntohs(s22wh->record_count); i++) {
		printf("\n--- 第 %d 条数据 ---\n", i+1);
		printf("客户 ID: %lu (0x%.16lx)\n",
			*((uint64_t *)p),
			*((uint64_t *)p));
		p += 8;

		printf("营业执照号码长度: %d\n", *p);
		p++;

		printf("营业执照号码: ");
		print_str((char *)p, *(p-1));
		p += *(p-1);

		printf("客户名称长度: %d\n", *p);
		p++;

		printf("客户名称: ");
		print_str((char *)p, *(p-1));
		p += *(p-1);

		printf("操作类型: 0x%.2x%s\n", *p,
			(*p > 0x03) ? oper_type[0] : oper_type[*p]);
		p++;
	}

	return 0;
}

int
main (void)
{
	int socketfd;
	struct sockaddr_in sn;
	socklen_t snsize;
	int bind_rc;
	int ret;
	uint8_t recvbuf[sizeof(struct rxhdr) + SEGSIZE];
	struct rxhdr *rxh = (void *)recvbuf;
	int total_len, payload_len, len = 0;
	int ready = 0;

	printf("\n收银机模拟器（服务端）版本: %s\n\n", SYJ_VERSION);

	bzero(&sn, sizeof(sn));
	sn.sin_family = AF_INET;
	sn.sin_addr.s_addr = htonl(INADDR_ANY);
	sn.sin_port = htons(RXUDP_PORT);
	snsize = sizeof(sn);

	socketfd = socket(AF_INET, SOCK_DGRAM, 0);
	if (socketfd == -1) {
		perror("socket call failed");
		exit(errno);
	}

	bind_rc = bind(socketfd, (struct sockaddr *)&sn, snsize);
	if (bind_rc == -1) {
		perror("bind failed");
		exit(errno);
	}

	rxudp_setsim("FEDCBA987654321");
	for (;;) {
		ret = rxudp_get_packet(socketfd, &sn, TIMEOUT, &total_len, rxh);
		if ((ret <= GET_TIMEOUT) || (total_len != sizeof(struct rxhdr)))
			continue;

		switch (ret) {
			case GET_WRQ:;
				     ret = rxudp_server_recv(socketfd, &sn, buf, &payload_len);
				     if (ret == ERR)
					     continue;

				     printf("\n------服务器收到信息-----\n");
				     printf("接收数据长度: %d\n", payload_len);

				     switch (buf[0]) {
					     case 0x01:;
						       len = syj_01(buf);
						       ready = 1;
						       break;
					     case 0x02:;
						       len = syj_02(buf);
						       ready = 1;
						       break;
					     case 0x03:;
						       len = syj_03(buf);
						       ready = 1;
						       break;
					     case 0x04:;
						       len = syj_04(buf);
						       ready = 1;
						       break;
					     case 0x05:;
						       len = syj_05(buf);
						       ready = 1;
						       break;
					     case 0x06:;
						       len = syj_06(buf);
						       ready = 1;
						       break;
					     case 0x07:;
						       len = syj_07(buf);
						       ready = 1;
						       break;
					     case 0x08:;
						       len = syj_08(buf);
						       ready = 1;
						       break;
					     case 0x09:;
						       len = syj_09(buf);
						       ready = 1;
						       break;
					     case 0x0a:;
						       len = syj_0a(buf);
						       ready = 1;
						       break;
					     case 0x0b:;
						       len = syj_0b(buf);
						       ready = 1;
						       break;
					     case 0x0c:;
						       len = syj_0c(buf);
						       ready = 1;
						       break;
					     case 0x0d:;
						       len = syj_0d(buf);
						       ready = 1;
						       break;
					     case 0x0e:;
						       len = syj_0e(buf);
						       ready = 1;
						       break;
					     case 0x0f:;
						       len = syj_0f(buf);
						       ready = 1;
						       break;
					     case 0x10:;
						       len = syj_10(buf);
						       ready = 1;
						       break;
					     case 0x11:;
						       len = syj_11(buf);
						       ready = 1;
						       break;
					     case 0x12:;
						       len = syj_12(buf);
						       ready = 1;
						       break;
					     case 0x13:;
						       syj_13(buf);
						       ready = 0;
						       break;
					     case 0x14:;
						       syj_14(buf);
						       ready = 0;
						       break;
					     case 0x15:;
						       syj_15(buf);
						       ready = 0;
						       break;
					     case 0x16:;
						       syj_16(buf);
						       ready = 0;
						       break;
					     case 0x17:;
						       syj_17(buf);
						       ready = 0;
						       break;
					     case 0x18:;
						       syj_18(buf);
						       ready = 0;
						       break;
					     case 0x19:;
						       syj_19(buf);
						       ready = 0;
						       break;
					     case 0x1a:;
						       syj_1a(buf);
						       ready = 0;
						       break;
					     case 0x1b:;
						       syj_1b(buf);
						       ready = 0;
						       break;
					     case 0x1c:;
						       len = syj_1c(buf);
						       ready = 1;
						       break;
					     case 0x1d:;
						       len = syj_1d(buf);
						       ready = 1;
						       break;
					     case 0x1e:;
						       len = syj_1e(buf);
						       ready = 1;
						       break;
					     case 0x1f:;
						       len = syj_1f(buf);
						       ready = 1;
						       break;
					     case 0x20:;
						       syj_20(buf);
						       ready = 0;
						       break;
					     case 0x21:;
						       syj_21(buf);
						       ready = 0;
						       break;
					     case 0x22:;
						       syj_22(buf);
						       ready = 0;
						       break;
					     default:;
						       printf("错误的信令编号: 0x%.2x\n", buf[0]);
						       break;
				     }
				     break;
			case GET_RRQ:;
				     fflush(stdout);
				     if (!ready)
					     break;
				     ret = rxudp_server_send(socketfd, &sn, buf, len);
				     if (ret == ERR) {
					printf("continue\n");
					continue;
				     }
				     printf("发送数据长度: %d\n", len);
				     ready = 0;
				     break;
		}
	}

	return 0;
}
