CC	:= gcc
CFLG	:= -Wall
OPT	:= -O3 -std=gnu99
CLIB	:=
TARGET	+= syj.o
TARGET	+= flashmem.o
TARGET	+= menu.o
TARGET	+= rxudp.o
DEBUG	:= -g
PROG	:= syj


all: $(TARGET)
	$(CC) $(CFLG) $(OPT) $(DEBUG) $(CLIB) -o $(PROG) $(TARGET)
	$(CC) $(CFLG) $(OPT) $(DEBUG) $(CLIB) -o syj_server syj_server.c rxudp.o
	$(CC) $(CFLG) $(OPT) $(DEBUG) $(CLIB) -o client_recv client_recv.c rxudp.o
	$(CC) $(CFLG) $(OPT) $(DEBUG) $(CLIB) -o server_send server_send.c rxudp.o
	strip $(PROG) syj_server

syj.o: syj.c syj.h
	$(CC) $(CFLG) $(OPT) $(DEBUG) -c syj.c

menu.o: menu.c menu.h global.h syj_struct.h
	$(CC) $(CFLG) $(OPT) $(DEBUG) -c menu.c

rxudp.o: rxudp.c rxudp.h
	$(CC) $(CFLG) $(OPT) $(DEBUG) -c rxudp.c

flashmem.o: flashmem.c flashmem.h
	$(CC) $(CFLG) $(OPT) $(DEBUG) -c flashmem.c

.PHONY	: clean

clean:
	rm -f $(TARGET) $(PROG) syj_server
