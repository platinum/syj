#include "flashmem.h"

int
flash_init (void)
{
	printf("Initializing flash . . . ");
	fflush(stdout);
	flash = malloc(FLASH_SIZE);
	if (flash == NULL) {
		printf("ERROR\n");
		fflush(stdout);
		return -1;
	}
	memset(flash, 0x00, FLASH_SIZE);
	printf("OK\n");
	fflush(stdout);

	return 0;
}

int
flash_deinit (void)
{
	printf("Flushing flash . . . ");
	fflush(stdout);
	free(flash);
	flash = NULL;
	printf("OK\n");
	fflush(stdout);

	return 0;
}

int
flash_create (void)
{
	int fd;
	int ret = 0;

	fd = open(FLASH_NAME, O_RDWR | O_CREAT, 0644);
	if (fd == -1)
		return -1;
	if (write(fd, flash, FLASH_SIZE) == -1)
		ret = -1;
	close(fd);

	return ret;
}

int
flash_load (void)
{
	struct stat buf;
	int fd;
	int ret = 0;

	memset(&buf, 0, sizeof(buf));
	printf("Loading flash to memory . . . ");
	fflush(stdout);
	if (stat(FLASH_NAME, &buf) < 0 || buf.st_size != FLASH_SIZE) {
		if (flash_create() == -1) {
			printf("ERROR\n");
			fflush(stdout);
			return -1;
		}
	}

	fd = open(FLASH_NAME, O_RDWR | O_CREAT, 0644);
	if (fd == -1) {
		printf("ERROR\n");
		fflush(stdout);
		return -1;
	}
	if (read(fd, flash, FLASH_SIZE) == -1) {
		printf("ERROR\n");
		fflush(stdout);
		close(fd);
		ret = -1;
	} else {
		printf("OK\n");
		fflush(stdout);
	}

	close(fd);

	if (DEPT_ID == 0)
		DEPT_ID = 12345678ULL;

	return ret;
}

int
flash_save (void)
{
	int fd;
	int ret = 0;

	printf("Saving memory to flash . . . ");
	fflush(stdout);

	fd = open(FLASH_NAME, O_RDWR | O_CREAT, O_SYNC);
	if (fd == -1) {
		printf("ERROR\n");
		fflush(stdout);
		return -1;
	}
	if (write(fd, flash, FLASH_SIZE) == -1) {
		printf("ERROR\n");
		fflush(stdout);
		ret = -1;
	} else {
		printf("OK\n");
		fflush(stdout);
	}

	close(fd);

	return ret;
}
