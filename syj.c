#include "syj.h"
#include "flashmem.h"
#include "menu.h"

int timestamp_flag = 0;

void
strfmt (char *buf)
{
	int len = strlen(buf);
	int i;

	for (i=0; i<len; i++) {
		if (buf[i] < ' ')
			buf[i] = 0x00;
	}
}

void
sig_int (int signo)
{
	longjmp(go_out, -1);
}

int
main (void)
{
	char buf[MAXCHAR];
	int i, j, k;
	int (*fun)(void);

	/* 截获 CTRL + C */
	signal(SIGINT, sig_int);

	if (flash_init() == -1)
		return -1;

	if (flash_load() == -1) {
		flash_deinit();
		return -1;
	}

	for (;;) {
		i = menu_show();
		printf("操作指令：");

		if (setjmp(go_out) == -1) {
			printf("\n");
			flash_save();
			flash_deinit();
			return 0;
		}

		while (fgets(buf, MAXCHAR, stdin) == NULL);

		buf[MAXCHAR - 1] = '\0';
		strfmt(buf);
		j = atoi(buf);

		if (j < 1 || j > i)
			continue;

		/* 判断最后是否有 " 0" 字样 */
		k = strlen(buf);
		if (k >= 3 && !strcmp(buf + k - 2, " 0"))
			timestamp_flag = 1;
		else
			timestamp_flag = 0;

		fun = menu_getfun(j);
		fun();

		continue;
	}

	return 0;
}
