/*
 * RXUDP 协议参考 tftp 协议标准实现（RFC783），略有改动
 * @Author: 白金 Rexen(Beijing)
 * @Date: 2011.01.24
 *
 * rxudp.h	库中所需要的基本定义
 * rxudp.c	RXUDP 的一些封装函数
 *
 * 使用方法：
 * 	服务端：
 * 		监听 UDP 端口，调用 rxudp_get_packet 接收数据
 * 		收到数据后做合法性验证，区分是 WRQ 还是 RRQ 请求
 * 		WRQ
 * 			调用 rxudp_server_recv 接收数据流
 * 		RRQ
 * 			调用 rxudp_server_send 发送数据流
 * 		反复循环继续等待请求
 * 	客户端：
 * 		建立套接字（服务器 IP、端口）
 *		给服务器发数据
 *			调用 rxudp_client_send
 *		从服务器读数据
 *			调用 rxudp_client_recv
 *
 * 无线收银机业务逻辑
 * 	rxudp_client_send: 发送请求信令
 * 	rxudp_client_recv: 等待服务器的数据返回
 *
 * 注意：
 * 	1、接收方收到最后的数据块后，返回的 ACK 发送方未必能收到
 * 	   为了保证尽量收到，再次重发 10 次 ACK
 * 	   如果发送方还不能收到最后数据块的 ACK，发送方会认为发送失败，但接收方则视为成功
 * 	2、此测试用例的服务端仅支持单线程处理
 */

#define DEBUG	1
#if DEBUG
#define DEBUGP printf
#else
#define DEBUGP(format, args...)
#endif

#include "rxudp.h"

uint8_t simid[15] = {0};

void
rxudp_setsim (char *sim)
{
	memcpy(simid, sim, 15);
}

/*
 * 读取数据包并返回状态
 * timeout: 设置超时时间（秒）
 * total_len: 返回读到的包长度，调用时设置成 NULL 则不返回
 * rxh: 读到数据后放在 rxh 的位置
 * RETURN: 数据包类型
 */
uint8_t
rxudp_get_packet (int socketfd, struct sockaddr_in *sn,
		  int timeout, int *total_len, struct rxhdr *rxh)
{
	struct timeval tv;
	fd_set fds;
	int ret;
	socklen_t snsize = sizeof(*sn);

	/* 设置收包非阻塞时长 */
	tv.tv_sec = timeout;
	tv.tv_usec = 0;
	FD_ZERO(&fds);
	FD_SET(socketfd, &fds);
	ret = select(FD_SETSIZE, &fds, NULL, NULL, &tv);

	switch (ret) {
		case -1:
			return ERR;
			break;
		case 0:
			return GET_TIMEOUT;
		default:
			if (FD_ISSET(socketfd, &fds)) {
				snsize = sizeof(*sn);
				ret = recvfrom(socketfd, rxh,
						sizeof(struct rxhdr) + SEGSIZE,
						0, (struct sockaddr *)sn, &snsize);
				if (ret <= 0)
					return ERR;

				/* 验证报文合法性 */
				if (memcmp(rxh->rexen, "RX", 2))
					return GET_DISCARD;

				/* 返回读到的数据长度 */
				if (total_len)
					*total_len = ret;

				/* 返回状态 */
				switch (rxh->opcode) {
					case RRQ:
						return GET_RRQ;
					case WRQ:
						return GET_WRQ;
					case ACK:
						return GET_ACK;
					case ERROR:
						return GET_ERROR;
					case DATA:
						return GET_DATA;
					default:
						return GET_DISCARD;
				}
			}
	}
	return ERR;
}

/*
 * 发送请求包
 * opcode: WRQ/RRQ
 * RETURN: 成功：OK、失败：ERR
 */
int
rxudp_send_request (int socketfd, struct sockaddr_in *sn, uint8_t opcode)
{
	int ret;
	struct rxhdr rxh;
	int i = 10;

	memcpy(rxh.rexen, "RX", 2);
	rxh.opcode = opcode;
	rxh.u.block = 0;
	memcpy(rxh.simid, simid, sizeof(rxh.simid));

	/* 由于有时套接字有问题，所以当发现问题时重试 10 次 */
	do {
		ret = sendto(socketfd, &rxh, sizeof(struct rxhdr), 0, (struct sockaddr *)sn, sizeof(*sn));
	} while ((ret < 0) && (i-- != 0));

	if (ret < 0)
		return ERR;

	return OK;
}

/*
 * 发送 ACK 应答包
 * block: ACK 的 block 序号（主机字节序）
 * RETURN: 成功：OK、失败：ERR
 */
int
rxudp_send_ack (int socketfd, struct sockaddr_in *sn, uint8_t block)
{
	int ret;
	struct rxhdr rxh;

	memcpy(rxh.rexen, "RX", 2);
	rxh.opcode = ACK;
	rxh.u.block = block;
	memcpy(rxh.simid, simid, sizeof(rxh.simid));
	ret = sendto(socketfd, &rxh, sizeof(struct rxhdr), 0, (struct sockaddr *)sn, sizeof(*sn));
	if (ret < 0)
		return ERR;

	return OK;
}

/*
 * 发送 ERROR 包
 * errcode: 错误代码
 * RETURN: 成功：OK、失败：ERR
 */
int
rxudp_send_error (int socketfd, struct sockaddr_in *sn, uint16_t errcode)
{
	int ret;
	struct rxhdr rxh;

	memcpy(rxh.rexen, "RX", 2);
	rxh.opcode = ERROR;
	rxh.u.errcode = errcode;
	memcpy(rxh.simid, simid, sizeof(rxh.simid));
	ret = sendto(socketfd, &rxh, sizeof(struct rxhdr), 0, (struct sockaddr *)sn, sizeof(*sn));
	if (ret < 0)
		return ERR;

	return OK;
}

/*
 * 发送数据块
 * block: 数据块编号
 * rxh: 全部待发送数据的头指针
 * total_len: 待发送数据总长度
 * RETURN: 成功：OK、失败：ERR
 */
int
rxudp_send_data (int socketfd, struct sockaddr_in *sn, uint8_t block,
		 struct rxhdr *rxh, int total_len)
{
	int ret;

	memcpy(rxh->rexen, "RX", 2);
	rxh->opcode = DATA;
	rxh->u.block = block;
	memcpy(rxh->simid, simid, sizeof(rxh->simid));
	ret = sendto(socketfd, rxh, total_len, 0, (struct sockaddr *)sn, sizeof(*sn));
	if (ret < 0)
		return ERR;

	return OK;
}

/*
 * 客户端发送数据流状态机
 * 1、发送请求 (WRQ/RRQ)
 * 2、等待收包
 * 	- 是 ACK 包，goto 3
 * 	- 是 ERROR 包，ABORT
 * 	- TIMEOUT，回到上一个状态
 * 3、发送数据，goto 2
 *
 * *data: 发送缓冲区指针
 * len: 发送数据长度
 * RETURN: 成功：OK、失败：ERR
 */
int
rxudp_client_send (int socketfd, struct sockaddr_in *sn, uint8_t *data, int len)
{
	int state = S_SEND_REQ;		/* 初始状态 */
	int timeout_state = state;	/* 初始超时状态 */
	int timeout_times = 0;		/* 重试次数 */
	uint8_t send_block = 0;		/* 最后发送的数据块编号 */
	uint8_t recv_block = 0;		/* 接收到的数据块编号 */
	int ret;
	int offset = 0;			/* 发送缓冲区偏移量 */
	int total_len = 0;		/* 数据总长 */
	int payload_len;		/* 数据块长度 */
	int finish = 0;			/* 传输结束标记 */
	uint8_t *send_buf;
	uint8_t *recv_buf;
	struct rxhdr *send_rxh;
	struct rxhdr *recv_rxh;

	send_buf = malloc(sizeof(struct rxhdr) + MAXSIZE);
	if (send_buf == NULL)
		return ERR;

	recv_buf = malloc(sizeof(struct rxhdr) + MAXSIZE);
	if (recv_buf == NULL) {
		free(send_buf);
		return ERR;
	}

	send_rxh = (void *)send_buf;
	recv_rxh = (void *)recv_buf;

	memcpy(send_rxh->simid, simid, sizeof(send_rxh->simid));
	for (;;) {
		switch (state) {
			case S_SEND_REQ:
				DEBUGP("S_SEND_REQ\n");
				timeout_state = S_SEND_REQ;
				if (rxudp_send_request(socketfd, sn, WRQ) == ERR)
					state = S_ABORT;
				else
					state = S_WAIT_PACKET;
				break;
			case S_SEND_DATA:
				DEBUGP("S_SEND_DATA\n");
				timeout_state = S_SEND_DATA;
				if (recv_block == send_block) {
					/* 准备新的数据 */
					payload_len = (len - offset > SEGSIZE) ? SEGSIZE : len - offset;
					total_len = payload_len + sizeof(struct rxhdr);
					memcpy(send_rxh->data, data + offset, payload_len);
					offset += payload_len;
					send_block++;
					/* 如果是最后一块数据，则设置结束标记 */
					if (payload_len < SEGSIZE)
						finish = 1;
				}
				DEBUGP("block: %d\n\n", send_block);
				rxudp_send_data(socketfd, sn, send_block, send_rxh, total_len);
				state = S_WAIT_PACKET;
				break;
			case S_WAIT_PACKET:
				DEBUGP("S_WAIT_PACKET\n");
				ret = rxudp_get_packet(socketfd, sn, TIMEOUT, NULL, recv_rxh);
				switch (ret) {
					case GET_TIMEOUT:
						DEBUGP("GET_TIMEOUT\n");
						timeout_times++;
						if (timeout_times > TIMEOUT_TIMES)
							state = S_ABORT;
						else {
							state = timeout_state;
							DEBUGP("Retry: %d . . .\n", timeout_times);
						}
						break;
					case GET_ACK:
						DEBUGP("GET_ACK\n");
						timeout_times = 0;
						recv_block = recv_rxh->u.block;
						DEBUGP("block: %d\n\n", recv_block);
						/* 如果接收到了最后一个数据块的 ACK，则传输结束 */
						if (finish && (send_block == recv_block)) {
							state = S_END;
							break;
						}
						state = S_SEND_DATA;
						break;
					case GET_ERROR:
						DEBUGP("GET_ERROR\n");
						state = S_ABORT;
						break;
					case GET_DISCARD:
						DEBUGP("GET_DISCARD\n");
						break;
					case ERR:
						DEBUGP("ERR\n");
						state = S_ABORT;
						break;
					default:
						DEBUGP("S_WAIT_PACKET: ret = %d\n", ret);
				}
				break;
			case S_ABORT:
				perror("S_ABORT");
				free(send_buf);
				free(recv_buf);
				return ERR;
			case S_END:
				DEBUGP("S_END\n");
				free(send_buf);
				free(recv_buf);
				return OK;
			default:
				perror("ERR");
				free(send_buf);
				free(recv_buf);
				return ERR;
		}
	}
}

/*
 * 客户端接收数据流状态机
 * 1、发送请求 (WRQ/RRQ)
 * 2、等待收包
 * 	- 是 ACK 包，goto 3
 * 	- 是 ERROR 包，ABORT
 * 	- TIMEOUT，回到上一个状态
 * 3、发送数据，goto 2
 *
 * *data: 接收缓冲区指针
 * *len: 接收完成后保存接收的数据长度
 * RETURN: 成功：OK、失败：ERR
 */
int
rxudp_client_recv (int socketfd, struct sockaddr_in *sn, uint8_t *data, int *len)
{
	int state = S_SEND_REQ;
	int timeout_state = state;
	int timeout_times = 0;
	uint8_t send_block = 0;
	uint8_t recv_block = 0;
	int ret;
	int offset = 0;
	int total_len = 0;
	int payload_len;
	int finish = 0;
	uint8_t *buf;
	struct rxhdr *rxh;

	buf = malloc(sizeof(struct rxhdr) + MAXSIZE);
	if (buf == NULL)
		return ERR;

	rxh = (void *)buf;
	memcpy(rxh->simid, simid, sizeof(rxh->simid));
	for (;;) {
		switch (state) {
			case S_SEND_REQ:
				DEBUGP("S_SEND_REQ\n");
				timeout_state = S_SEND_REQ;
				if (rxudp_send_request(socketfd, sn, RRQ) == ERR)
					state = S_ABORT;
				else
					state = S_WAIT_PACKET;
				break;
			case S_SEND_ACK:
				DEBUGP("S_SEND_ACK\n");
				timeout_state = S_SEND_ACK;
				DEBUGP("block: %d\n\n", send_block);
				if (rxudp_send_ack(socketfd, sn, send_block) == ERR) {
					state = S_ABORT;
					break;
				}
				if (finish && (send_block == recv_block)) {
					state = S_END;
#if 0
					for (int i=1; i<=10; i++) {
						DEBUGP("last ACK send again: %d\n", i);
						rxudp_send_ack(socketfd, sn, send_block);
					}
#endif
				} else
					state = S_WAIT_PACKET;
				break;
			case S_WAIT_PACKET:
				DEBUGP("S_WAIT_PACKET\n");
				ret = rxudp_get_packet(socketfd, sn, TIMEOUT, &total_len, rxh);
				switch (ret) {
					case GET_TIMEOUT:
						DEBUGP("GET_TIMEOUT\n");
						timeout_times++;
						if (timeout_times > TIMEOUT_TIMES)
							state = S_ABORT;
						else {
							state = timeout_state;
							DEBUGP("Retry: %d . . .\n", timeout_times);
						}
						break;
					case GET_ERROR:
						DEBUGP("GET_ERROR\n");
						state = S_ABORT;
						break;
					case GET_DATA:
						timeout_times = 0;
						state = S_DATA_RECEIVED;
						break;
					case GET_DISCARD:
						DEBUGP("GET_DISCARD\n");
						break;
					case ERR:
						DEBUGP("ERR\n");
						state = S_ABORT;
						break;
					default:
						DEBUGP("S_WAIT_PACKET: ret = %d\n", ret);
				}
				break;
			case S_DATA_RECEIVED:
				recv_block = rxh->u.block;
				/* 利用 block_after 宏解决数据越界循环问题 */
				if (block_after(recv_block, send_block)) {
					payload_len = total_len - sizeof(struct rxhdr);
					memcpy(data + offset, rxh->data, payload_len);
					offset += payload_len;
					send_block = recv_block;
					if (payload_len < SEGSIZE)
						finish = 1;
				}
				state = S_SEND_ACK;
				timeout_state = state;
				break;
			case S_END:
				DEBUGP("S_END\n");
				*len = offset;
				free(buf);
				return OK;
			case S_ABORT:
				DEBUGP("S_ABORT\n");
				free(buf);
				return ERR;
			default:
				free(buf);
				return ERR;
		}
	}

	free(buf);
	return ERR;
}

/*
 * 服务端发送数据流状态机
 * 1、发送数据
 * 2、等待收包
 * 	- 是 ACK 包，goto 3
 * 	- 是 ERROR 包，ABORT
 * 	- TIMEOUT，回到上一个状态
 * 3、发送数据，goto 2
 *
 * *data: 发送缓冲区指针
 * len: 发送数据长度
 * RETURN: 成功：OK、失败：ERR
 */
int
rxudp_server_send (int socketfd, struct sockaddr_in *sn, uint8_t *data, int len)
{
	int state = S_BEGIN;
	int timeout_state = state;
	int timeout_times = 0;
	uint8_t send_block = 0;
	uint8_t recv_block = 0;
	int ret;
	int offset = 0;
	int total_len = 0;
	int payload_len;
	int finish = 0;
	uint8_t *send_buf;
	uint8_t *recv_buf;
	struct rxhdr *send_rxh;
	struct rxhdr *recv_rxh;

	send_buf = malloc(sizeof(struct rxhdr) + MAXSIZE);
	if (send_buf == NULL)
		return ERR;

	recv_buf = malloc(sizeof(struct rxhdr) + MAXSIZE);
	if (recv_buf == NULL) {
		free(send_buf);
		return ERR;
	}

	send_rxh = (void *)send_buf;
	recv_rxh = (void *)recv_buf;

	memcpy(send_rxh->simid, simid, sizeof(send_rxh->simid));
	for (;;) {
		switch (state) {
			case S_BEGIN:
				DEBUGP("S_BEGIN\n");
				state = S_SEND_DATA;
				break;
			case S_SEND_DATA:
				DEBUGP("S_SEND_DATA\n");
				timeout_state = state;
				if (recv_block == send_block) {
					/* 准备新数据 */
					payload_len = (len - offset > SEGSIZE) ? SEGSIZE : len - offset;
					total_len = payload_len + sizeof(struct rxhdr);
					memcpy(send_rxh->data, data + offset, payload_len);
					offset += payload_len;
					send_block++;
					/* 如果是最后一块数据，则设置结束标记 */
					if (payload_len < SEGSIZE)
						finish = 1;
				}
				DEBUGP("block: %d\n\n", send_block);
				rxudp_send_data(socketfd, sn, send_block, send_rxh, total_len);
				state = S_WAIT_PACKET;
				break;
			case S_WAIT_PACKET:
				DEBUGP("S_WAIT_PACKET\n");
				ret = rxudp_get_packet(socketfd, sn, TIMEOUT, NULL, recv_rxh);
				switch (ret) {
					case GET_TIMEOUT:
						DEBUGP("GET_TIMEOUT\n");
						timeout_times++;
						if (timeout_times > TIMEOUT_TIMES)
							state = S_ABORT;
						else {
							state = timeout_state;
							DEBUGP("Retry: %d . . .\n", timeout_times);
						}
						break;
					case GET_ACK:
						DEBUGP("GET_ACK\n");
						timeout_times = 0;
						recv_block = recv_rxh->u.block;
						DEBUGP("block: %d\n\n", recv_block);
						/* 如果接收到了最后一个数据块的 ACK，则传输结束 */
						if (finish && (recv_block == send_block)) {
							state = S_END;
							break;
						}
						state = S_SEND_DATA;
						break;
					case GET_ERROR:
						DEBUGP("GET_ERROR\n");
						state = S_ABORT;
						break;
					case GET_DISCARD:
						DEBUGP("GET_DISCARD\n");
						break;
					case ERR:
						DEBUGP("ERR\n");
						state = S_ABORT;
						break;
					case GET_RRQ:
						DEBUGP("RRQ\n");
						state = S_SEND_DATA;
						break;
					default:
						DEBUGP("S_WAIT_PACKET: ret = %d\n", ret);
				}
				break;
			case S_ABORT:
				DEBUGP("S_ABORT\n");
				free(send_buf);
				free(recv_buf);
				return ERR;
			case S_END:
				DEBUGP("S_END\n");
				free(send_buf);
				free(recv_buf);
				return OK;
			default:
				free(send_buf);
				free(recv_buf);
				return ERR;
		}
	}
}

/*
 * 服务端接收数据流状态机
 * 1、发送 ACK 包
 * 2、等待收包
 * 	- 是 DATA 包，读到接收缓存中，goto 1
 * 	- 是 ERROR 包，ABORT
 * 	- TIMEOUT，回到上一个状态
 *
 * *data: 接收缓冲区指针
 * *len: 接收完成后保存接收的数据长度
 * RETURN: 成功：OK、失败：ERR
 */
int
rxudp_server_recv (int socketfd, struct sockaddr_in *sn, uint8_t *data, int *len)
{
	int state = S_BEGIN;
	int timeout_state = state;
	int timeout_times = 0;
	uint8_t send_block = 0;
	uint8_t recv_block = 0;
	int ret;
	int offset = 0;
	int total_len = 0;
	int payload_len;
	int finish = 0;
	uint8_t *buf;
	struct rxhdr *rxh;

	buf = malloc(sizeof(struct rxhdr) + MAXSIZE);
	if (buf == NULL)
		return ERR;

	rxh = (void *)buf;
	memcpy(rxh->simid, simid, sizeof(rxh->simid));
	*len = 0;
	for (;;) {
		switch (state) {
			case S_BEGIN:
				DEBUGP("S_BEGIN\n");
				state = S_SEND_ACK;
				break;
			case S_SEND_ACK:
				DEBUGP("S_SEND_ACK\n");
				timeout_state = state;
				DEBUGP("block: %d\n\n", send_block);
				ret = rxudp_send_ack(socketfd, sn, send_block);
				if (finish) {
					state = S_END;
#if 0
					for (int i=1; i<=10; i++) {
						DEBUGP("last ACK send again: %d\n", i);
						rxudp_send_ack(socketfd, sn, send_block);
					}
#endif
				} else
					state = S_WAIT_PACKET;
				break;
			case S_WAIT_PACKET:
				DEBUGP("S_WAIT_PACKET\n");
				ret = rxudp_get_packet(socketfd, sn, TIMEOUT, &total_len, rxh);
				switch (ret) {
					case GET_TIMEOUT:
						DEBUGP("GET_TIMEOUT\n");
						timeout_times++;
						if (timeout_times > TIMEOUT_TIMES)
							state = S_ABORT;
						else {
							state = timeout_state;
							DEBUGP("Retry: %d . . .\n", timeout_times);
						}
						break;
					case GET_ERROR:
						DEBUGP("GET_ERROR\n");
						state = S_ABORT;
						break;
					case GET_DATA:
						timeout_times = 0;
						state = S_DATA_RECEIVED;
						break;
					case GET_DISCARD:
						DEBUGP("GET_DISCARD\n");
						break;
					case ERR:
						DEBUGP("ERR\n");
						state = S_ABORT;
						break;
					case GET_WRQ:
						DEBUGP("WRQ\n");
						state = S_SEND_ACK;
						break;
					default:
						DEBUGP("S_WAIT_PACKET: ret = %d\n", ret);
						state = S_ABORT;
				}
				break;
			case S_DATA_RECEIVED:
				DEBUGP("S_DATA_RECEIVED\n");
				recv_block = rxh->u.block;
				DEBUGP("block: %d\n\n", recv_block);
				/* 利用 block_after 宏解决数据越界循环问题 */
				if (block_after(recv_block, send_block)) {
					payload_len = total_len - sizeof(struct rxhdr);
					memcpy(data + offset, rxh->data, payload_len);
					offset += payload_len;
					send_block = recv_block;
					if (payload_len < SEGSIZE)
						finish = 1;
				}
				state = S_SEND_ACK;
				break;
			case S_END:
				DEBUGP("S_END\n");
				*len = offset;
				free(buf);
				return OK;
			case S_ABORT:
				DEBUGP("S_ABORT\n");
				free(buf);
				return ERR;
			default:
				free(buf);
				return ERR;
		}
	}

	free(buf);
	return ERR;
}
