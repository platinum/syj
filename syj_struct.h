#ifndef __SYJ_STRUCT_H
#define __SYJ_STRUCT_H

/* 定义客户类型 */
char *client_type[] = {
	"（散户）",
	"（批发商）",
	"（会员）",
};

/* 定义价格类型 */
char *price_type[] = {
	"（零售价）",
	"（售价1）",
	"（售价2）",
};

/* 定义操作类型 */
char *oper_type[] = {
	"（未知）",		/* 真正的编号从 0x01 开始 */
	"（添加）",
	"（修改）",
	"（删除）",
};

/* 类别 */
char *handle_type[] = {
	"（未知）",
	"（未知）",
	"（未知）",
	"（未知）",
	"（未知）",
	"（处理方式）",
};

/* 定义仓库类型 */
char *cangku[] = {
	"（未知）",		/* 真正的编号从 0x01 开始 */
	"（存货仓库）",
	"（禁售仓库）",
	"（在售货架）",
};

/* 定义任务类型 */
char *sort_type[] = {
	"（未知）",		/* 真正的编号从 0x01 开始 */
	"（自动生成 [进货单] 确认请求）",
	"（索取 [商家证] 确认请求）",
	"（索取 [产品证] 确认请求）",
};

/* 定义监管属性	*/
char *supervise[] = {
	"（未知）",		/* 真正的编号从 0x01 开始 */
	"（不监管）",
	"（监管生产日期保质期）",
	"（监管批次）",
	"（监管生产日期保质期和批次）"
};

/* 定义单位类别 */
char *sorts[] = {
	"（未知）",		/* 真正的编号从 0x01 开始 */
	"（基本单位）",
	"（辅助单位）"
};

/* 定义保质单位	*/
char *quality_units[] = {
	"（未知）",		/* 真正的编号从 0x01 开始 */
	"（天）",
	"（月）",
	"（年）"
};

/* 定义处理状态	*/
char *status[] = {
	"（未处理）",		/* 真正的编号从 0x00 开始 */
	"（已处理）",
	"（未知）"
};

/* 定义是否是赠品 */
char *is_give[] = {
	"（未知）",		/* 真正的编号从 0x00 开始 */
	"（是）",
	"（否）"
};

char *deal_way[] = {		/* 真正的编号从 0x00 开始 */
	"（未知）",
	"（销毁）",
	"（反厂）",
	"（低价出售）",
	"（员工内部解决）",
	"（其它）",
};

/* 是否启用 */
char *yes_no[] = {
	"（未知）",
	"（是）",
	"（否）",
};

/* 排列顺序 */
char *order[] = {
	"未知",
	"升序",
	"降序",
};

/* 信息验证: WRQ, 0x01 */
struct s_01_wrq {
	uint8_t cmd;		/* 信令编号		*/
	char sim[15];		/* SIM 卡唯一标识	*/
	char hardware[20];	/* 硬件串号		*/
} __attribute__((packed));

/* 信息验证: RRQ, 0x01 */
struct s_01_rrq {
	uint8_t cmd;		/* 信令编号		*/
	uint8_t result;		/* 验证结果: 通过 0x01，不通过 0x00 */
	uint64_t dept_id; 	/* 商家 ID		*/
	char num[2];		/* 收银机编号		*/
} __attribute__((packed));

/* 全局参数: WRQ, 0x02 */
struct s_02_wrq {
	uint8_t cmd;		/* 信令编号		*/
	uint64_t dept_id;	/* 商家 ID		*/
	uint32_t last_update;	/* 最后更新时间		*/
} __attribute__((packed));

/* 全局参数: RRQ, 0x02 */
struct s_02_rrq {
	uint8_t cmd;		/* 信令编号		*/
	uint8_t use_shelf;	/* 是否启用在售货架	*/
	uint8_t direct_out;	/* 是否直接出库		*/
	uint8_t direct_in;	/* 是否直接入库		*/
	uint8_t batch;		/* 是否选择批次		*/
	uint8_t batch_order;	/* 批次号排序规则	*/
	uint8_t if_auto;	/* 是否自动生成进货单	*/
	uint32_t last_update;	/* 最后更新时间		*/
} __attribute__((packed));

/* 小票信息: WRQ, 0x03 */
struct s_03_wrq {
	uint8_t cmd;		/* 信令编号		*/
	uint32_t last_update;	/* 最后更新时间		*/
} __attribute__((packed));

/* 小票信息: RRQ, 0x03 */
struct s_03_rrq {
	uint8_t cmd;		/* 信令编号		*/
	uint8_t head1_len;	/* 小票头部第 1 行长度	*/
	char *head1;		/* 小票头部第 1 行	*/
	uint8_t head2_len;	/* 小票头部第 2 行长度	*/
	char *head2;		/* 小票头部第 2 行	*/
	uint8_t head3_len;	/* 小票头部第 3 行长度	*/
	char *head3;		/* 小票头部第 3 行	*/
	uint8_t head4_len;	/* 小票头部第 4 行长度	*/
	char *head4;		/* 小票头部第 4 行	*/
	uint8_t head5_len;	/* 小票头部第 5 行长度	*/
	char *head5;		/* 小票头部第 5 行	*/
	uint8_t tail1_len;	/* 小票尾部第 1 行长度	*/
	char *tail1;		/* 小票尾部第 1 行	*/
	uint8_t tail2_len;	/* 小票尾部第 2 行长度	*/
	char *tail2;		/* 小票尾部第 2 行	*/
	uint8_t tail3_len;	/* 小票尾部第 3 行长度	*/
	char *tail3;		/* 小票尾部第 3 行	*/
	uint8_t head1_flag;	/* 小票头部第 1 行双高标记 */
	uint8_t head2_flag;	/* 小票头部第 2 行双高标记 */
	uint8_t head3_flag;	/* 小票头部第 3 行双高标记 */
	uint8_t head4_flag;	/* 小票头部第 4 行双高标记 */
	uint8_t head5_flag;	/* 小票头部第 5 行双高标记 */
	uint8_t tail1_flag;	/* 小票尾部第 1 行双高标记 */
	uint8_t tail2_flag;	/* 小票尾部第 2 行双高标记 */
	uint8_t tail3_flag;	/* 小票尾部第 3 行双高标记 */
	uint32_t last_update;	/* 最后更新时间		*/
} __attribute__((packed));

/* 商家信息: WRQ, 0x04 */
struct s_04_wrq {
	uint8_t cmd;		/* 信令编号		*/
	uint64_t dept_id;	/* 商家 ID		*/
	uint32_t last_update;	/* 最后更新时间		*/
} __attribute__((packed));

/* 商家信息: RRQ, 0x04 */
struct s_04_rrq {
	uint8_t cmd;		/* 信令编号		*/
	uint64_t id;		/* 商家 ID		*/
	uint64_t pid;		/* 商家父 ID		*/
	uint8_t num_len;	/* 营业执照号码长度	*/
	char *num;		/* 营业执照号码		*/
	uint64_t code;		/* 商家编码		*/
	uint8_t name_len;	/* 商家名称长度		*/
	char *name;		/* 商家名称		*/
	uint8_t tel_len;	/* 商家电话长度		*/
	char *tel;		/* 商家电话		*/
	uint8_t address_len;	/* 商家地址长度		*/
	char *address;		/* 商家地址		*/
	uint16_t province;	/* 省份			*/
	uint16_t city;		/* 城市			*/
	uint32_t last_update;	/* 最后更新时间		*/
} __attribute__((packed));

/* 销售价格定制信息: WRQ, 0x05 */
struct s_05_wrq {
	uint8_t cmd;		/* 信令编号		*/
	uint64_t dept_id;	/* 商家 ID		*/
	uint32_t last_update;	/* 最后更新时间		*/
} __attribute__((packed));

/* 销售价格定制信息: RRQ, 0x05 */
struct s_05_rrq {
	uint8_t cmd;		/* 信令编号		*/
	uint8_t record_count;	/* 记录数量		*/
	uint32_t last_update;	/* 最后更新时间		*/
	char data[0];		/* 销售价格定制信息明细	*/
} __attribute__((packed));

/* 销售价格定制信息明细 */
struct s_05_rrq_data {
	uint8_t client_type;	/* 客户类型: 批发商 0x01, 会员 0x02, 散户 0x03 */
	uint8_t price_type;	/* 销售价格类型: 零售价 0x00, 售价1 0x01 */
				/* 售价2 0x02, 售价3 0x03 */
} __attribute__((packed));

/* 仓库信息: WRQ, 0x06 */
struct s_06_wrq {
	uint8_t cmd;		/* 信令编号		*/
	uint64_t dept_id;	/* 商家 ID		*/
	uint32_t last_update; 	/* 最后更新时间		*/
} __attribute__((packed));

/* 仓库信息: RRQ, 0x06 */
struct s_06_rrq {
	uint8_t cmd;		/* 信令编号		*/
	uint8_t record_count;	/* 记录数量		*/
	uint32_t last_update;	/* 最后更新时间		*/
	char data[0];		/* 仓库信息明细		*/
} __attribute__((packed));

/* 仓库信息明细 */
struct s_06_rrq_data {
	uint64_t id;		/* 仓库 ID		*/
	uint8_t name_len;	/* 仓库名称长度		*/
	char *name;		/* 仓库名称		*/
	uint8_t code_len;	/* 仓库编码长度		*/
	char *code;		/* 仓库编码		*/
	uint8_t type;		/* 仓库类型: 存货仓库 0x01, 禁售仓库 0x02, 在售货架 0x03 */
	uint64_t userid;	/* 库管员 ID		*/
	uint8_t user_name_len;	/* 库管员名字长度	*/
	char *user_name;	/* 库管员名字		*/
	uint8_t is_default;	/* 是否默认		*/
	uint8_t oper_type;	/* 操作类型		*/
} __attribute__((packed));

/* 员工信息: WRQ, 0x07 */
struct s_07_wrq {
	uint8_t cmd;		/* 信令编号		*/
	uint64_t dept_id;	/* 商家 ID		*/
	uint32_t last_update;	/* 最后更新时间		*/
} __attribute__((packed));

/* 员工信息: RRQ, 0x07 */
struct s_07_rrq {
	uint8_t cmd;		/* 信令编号		*/
	uint16_t record_count;	/* 记录数量		*/
	uint32_t last_update;	/* 最后更新时间		*/
	char data[0];		/* 员工信息明细		*/
} __attribute__((packed));

/* 员工信息明细 */
struct s_07_rrq_data {
	uint64_t id;		/* 员工 ID		*/
	char num[4];		/* 员工编号		*/
	char login_password[6];	/* 员工密码		*/
	uint8_t name_len;	/* 员工姓名长度		*/
	char *name;		/* 员工姓名		*/
	uint8_t oper_type;	/* 操作类型		*/
} __attribute__((packed));

/* 客户信息: WRQ, 0x08 */
struct s_08_wrq {
	uint8_t cmd;		/* 信令编号		*/
	uint64_t dept_id;	/* 商家 ID		*/
	uint32_t last_update;	/* 最后更新时间		*/
} __attribute__((packed));

/* 客户信息: RRQ, 0x08 */
struct s_08_rrq {
	uint8_t cmd;		/* 信令编号		*/
	uint16_t record_count;	/* 记录数量		*/
	uint32_t last_update;	/* 最后更新时间		*/
	char data[0];		/* 客户信息明细		*/
} __attribute__((packed));

/* 客户信息明细 */
struct s_08_rrq_data {
	uint64_t id;		/* 客户 ID			*/
	uint8_t num_len; 	/* 营业执照号码长度，最大 15	*/
	char *num;		/* 营业执照号码			*/
	uint8_t name_len;	/* 客户名称长度，最大 60	*/
	char *name;		/* 客户名称			*/
	char sys_dept_code[8];	/* 客户编码			*/
	uint8_t oper_type;	/* 操作类型			*/
} __attribute__((packed));

/* 供应商信息: WRQ, 0x09 */
struct s_09_wrq {
	uint8_t cmd;		/* 信令编号		*/
	uint64_t dept_id;	/* 商家 ID		*/
	uint32_t last_update;	/* 最后更新时间		*/
} __attribute__((packed));

/* 供应商信息: RRQ, 0x09 */
struct s_09_rrq {
	uint8_t cmd;		/* 信令编号		*/
	uint16_t record_count;	/* 记录数量		*/
	uint32_t last_update;	/* 最后更新时间		*/
	char data[0];		/* 供应商信息明细	*/
} __attribute__((packed));

/* 供应商信息明细 */
struct s_09_rrq_data {
	uint64_t id;		/* 供应商 ID			*/
	uint8_t num_len;	/* 营业执照号码长度，最大 15	*/
	char *num;		/* 营业执照号码			*/
	uint8_t name_len;	/* 供应商名称长度，最大 60	*/
	char *name;		/* 供应商名称			*/
	char sys_dept_code[8];	/* 供应商编码			*/
	uint8_t oper_type;	/* 操作类型 			*/
} __attribute__((packed));

/* 商品基本信息: WRQ, 0x0a */
struct s_0a_wrq {
	uint8_t cmd;		/* 信令编号		*/
	uint64_t dept_id;	/* 商家 ID		*/
	uint32_t last_update;	/* 最后更新时间		*/
} __attribute__((packed));

/* 商品基本信息: RRQ, 0x0a */
struct s_0a_rrq {
	uint8_t cmd;		/* 信令编号		*/
	uint16_t record_count;	/* 记录数量		*/
	uint32_t last_update;	/* 最后更新时间		*/
	char data[0];		/* 商品基本信息明细	*/
} __attribute__((packed));

/* 商品基本信息明细 */
struct s_0a_rrq_data {
	uint64_t id;			/* 商品 ID		*/
	uint8_t code_len;		/* 商品条码长度, 13	*/
	char *code;			/* 商品条码		*/
	uint8_t name_len;		/* 商品长称长度, 30	*/
	char *name;			/* 商品名称		*/
	uint8_t base_unit_name_len;	/* 基本单位名称长度, 10	*/
	char *base_unit_name;		/* 基本单位名称		*/
	uint64_t category_id;		/* 商品类别 ID		*/
	uint8_t spec_len;		/* 规格长度, 10		*/
	char *spec;			/* 规格			*/
	uint64_t commondity_id;		/* 基础商品 ID		*/
	uint8_t is_public;		/* 是否公有		*/
	uint64_t dept_id;		/* 商家 ID		*/
	uint8_t cert_code_len;		/* 证件编号长度, 30	*/
	char *cert_code;		/* 证件编号		*/
	uint8_t oper_type;		/* 操作类型		*/
} __attribute__((packed));

/* 商品单位信息: WRQ, 0x0b */
struct s_0b_wrq {
	uint8_t cmd;		/* 信令编号		*/
	uint64_t dept_id;	/* 商家 ID		*/
	uint32_t last_update;	/* 最后更新时间		*/
} __attribute__((packed));

/* 商品单位信息: RRQ, 0x0b */
struct s_0b_rrq {
	uint8_t cmd;		/* 信令编号		*/
	uint32_t record_count;	/* 记录数量		*/
	uint32_t last_update;	/* 最后更新时间		*/
	char data[0];		/* 商品单位信息明细	*/
} __attribute__((packed));

/* 商品单位信息明细 */
struct s_0b_rrq_data {
	uint64_t commodity_id;	/* 商品 ID		*/
	uint64_t unit_id;	/* 单位 ID		*/
	uint8_t unit_name_len;	/* 单位名称长度		*/
	char *unit_name;	/* 单位名称		*/
	uint16_t unit_relation;	/* 单位关系		*/
	uint8_t code_len;	/* 条码长度		*/
	char *code;		/* 条码			*/
	char retail_price[10];	/* 零售价		*/
	char vip_price[10];	/* 会员价		*/
	char deal_price[10];	/* 经销商价		*/
	char price[10];		/* 参考进货价		*/
	uint8_t sort;		/* 单位类别: 基本单位 0x01, 辅助单位 0x02 */
	uint8_t oper_type;	/* 操作类型		*/
} __attribute__((packed));

/* 商品批次信息: WRQ, 0x0c */
struct s_0c_wrq {
	uint8_t cmd;		/* 信令编号		*/
	uint64_t dept_id;	/* 商家 ID		*/
	uint32_t last_update;	/* 最后更新时间		*/
} __attribute__((packed));

/* 商品批次信息: RRQ, 0x0c */
struct s_0c_rrq {
	uint8_t cmd;		/* 信令编号		*/
	uint32_t record_count;	/* 记录数量		*/
	uint32_t last_update;	/* 最后更新时间		*/
	char data[0];		/* 商品批次信息明细	*/
} __attribute__((packed));

/* 商品批次信息明细 */
struct s_0c_rrq_data {
	uint64_t commodity_id;	/* 商品 ID		*/
	uint8_t batch_len;	/* 批次长度		*/
	char *batch;		/* 批次号		*/
	uint8_t create_date_len;/* 生产日期长度		*/
	char *create_date;	/* 生产日期		*/
	uint16_t quality_data;	/* 保质期		*/
	uint8_t quality_unit;	/* 保质单位		*/
	uint8_t exceed_date_len;/* 过期日期长度		*/
	char *exceed_date;	/* 过期日期		*/
	uint64_t depot_id;	/* 仓库 ID		*/
	uint8_t oper_type;	/* 操作类型		*/
} __attribute__((packed));

/* 待办任务信息: WRQ, 0x0d */
struct s_0d_wrq {
	uint8_t cmd;		/* 信令编号		*/
	uint64_t dept_id;	/* 商家 ID		*/
	uint32_t last_update;	/* 最后更新时间		*/
}__attribute__((packed));

/* 待办任务信息: RRQ, 0x0d */
struct s_0d_rrq {
	uint8_t cmd;		/* 信令编号		*/
	uint8_t record_count;	/* 总记录数		*/
	uint32_t last_update;	/* 最后更新时间		*/
	char data[0];		/* 待办任务信息明细	*/
}__attribute__((packed));

/* 待办任务信息明细 */
struct s_0d_rrq_data {
	uint64_t id;		/* 任务 ID			*/
	uint8_t title_len; 	/* 任务标题长度，最大 100	*/
	char *title;		/* 任务标题			*/
	uint16_t content_len;	/* 任务内容长度，最大 400	*/
	char *content;		/* 任务内容			*/
	char send_time[19];	/* 发送时间			*/
	uint8_t sort;		/* 任务类型			*/
	uint8_t pm_len;		/* 任务参数长度，最大 100	*/
	char *pm;		/* 任务参数			*/
	uint8_t status;		/* 处理状态			*/
	uint8_t oper_type;	/* 操作类型			*/
}__attribute__((packed));

/* 系统消息: WRQ, 0x0e */
struct s_0e_wrq {
	uint8_t cmd;		/* 信令编号		*/
	uint64_t dept_id;	/* 商家 ID		*/
	uint32_t last_update;	/* 最后更新时间		*/
}__attribute((packed));

/* 系统消息: RRQ, 0x0e */
struct s_0e_rrq {
	uint8_t cmd;		/* 信令编号		*/
	uint8_t record_count;	/* 记录数量		*/
	uint32_t last_update;	/* 最后更新时间		*/
	char data[0];		/* 系统消息明细		*/
}__attribute__((packed));

/* 系统消息明细 */
struct s_0e_rrq_data {
	uint64_t id;		/* 消息 ID		*/
	uint8_t	title_len;	/* 消息标题长度		*/
	char *title;		/* 消息标题		*/
	uint16_t content_len;	/* 消息内容长度		*/
	char *content;		/* 消息内容		*/
	char send_time[19];	/* 发送时间		*/
	uint8_t sort;		/* 消息类型		*/
	uint8_t oper_type;	/* 操作类型		*/
}__attribute__((packed));

/* 商品类别信息: WRQ, 0x0f */
struct s_0f_wrq {
	uint8_t cmd;		/* 信令编号		*/
	uint32_t last_update;	/* 最后更新时间		*/
}__attribute__((packed));

/* 商品类别信息: RRQ, 0x0f */
struct s_0f_rrq {
	uint8_t cmd;		/* 信令编号		*/
	uint16_t record_count;	/* 记录数量		*/
	uint32_t last_update;	/* 最后更新时间		*/
	char data[0];		/* 系统消息明细		*/
}__attribute__((packed));

/* 商品类别信息明细 */
struct s_0f_rrq_data {
	uint64_t id;		/* 类别 ID			*/
	uint8_t number_len;	/* 类别编号长度, 最大 4 	*/
	char *number;		/* 类别编号			*/
	uint8_t name_len;	/* 类别名称长度，最大 30	*/
	char *name;		/* 类别名称			*/
	uint8_t is_circulate;	/* 监管属性			*/
	uint8_t code_len;	/* 编码长度，最大 20		*/
	char *code;		/* 编码				*/
	uint8_t oper_type;	/* 操作类型			*/
}__attribute__((packed));

/* 数据字典: WRQ, 0x10 */
struct s_10_wrq {
	uint8_t cmd;		/* 信令编号		*/
	uint32_t last_update;	/* 最后更新时间		*/
}__attribute__((packed));

/* 数据字典: RRQ, 0x10 */
struct s_10_rrq {
	uint8_t cmd;		/* 信令编号		*/
	uint16_t record_count;	/* 记录数量		*/
	uint32_t last_update;	/* 最后更新时间		*/
	char data[0];		/* 系统消息明细		*/
}__attribute__((packed));

/* 数据字典明细 */
struct s_10_rrq_data {
	uint64_t id;		/* ID				*/
	uint8_t name_len;	/* 名称长度，最大 20		*/
	char *name;		/* 类别名称			*/
	uint64_t val;		/* 对应值			*/
	uint8_t sort;		/* 类别 ID: 5			*/
	uint8_t seq;		/* 显示顺序			*/
	uint8_t oper_type;	/* 操作类型			*/
}__attribute__((packed));

/* 商品类别/证件编号名称对照: WRQ, 0x11 */
struct s_11_wrq {
	uint8_t cmd;		/* 信令编号		*/
	uint32_t last_update;	/* 最后更新时间		*/
}__attribute__((packed));

/* 商品类别/证件编号名称对照: RRQ, 0x11 */
struct s_11_rrq {
	uint8_t cmd;		/* 信令编号		*/
	uint16_t record_count;	/* 记录数量		*/
	uint32_t last_update;	/* 最后更新时间		*/
	char data[0];		/* 系统消息明细		*/
}__attribute__((packed));

/* 商品类别/证件编号名称对照明细 */
struct s_11_rrq_data {
	uint64_t id;		/* ID				*/
	char type_num[4];	/* 类别编号			*/
	uint8_t license_len;	/* 证件编号名称长度，最大 20	*/
	char *license;		/* 证件编号名称			*/
	uint8_t oper_type;	/* 操作类型			*/
}__attribute__((packed));

/* 差异化同步请求 WRQ, 0x12 */
struct s_12_wrq {
	uint8_t cmd;		/* 信令编号			*/
	uint64_t dept_id;	/* 商家 ID			*/
	uint32_t last_update2;	/* 小票信息最后更新时间		*/
	uint32_t last_update3;	/* 商家信息最后更新时间		*/
	uint32_t last_update4;	/* 销售价格定制信息最后更新时间	*/
	uint32_t last_update5;	/* 仓库信息最后更新时间		*/
	uint32_t last_update6;	/* 员工信息最后更新时间		*/
	uint32_t last_update7;	/* 客户信息最后更新时间		*/
	uint32_t last_update8;	/* 供应商信息最后更新时间	*/
	uint32_t last_update9;	/* 商品基本信息最后更新时间	*/
	uint32_t last_update10;	/* 商品单位信息最后更新时间	*/
	uint32_t last_update11;	/* 商品批次信息最后更新时间	*/
	uint32_t last_update12;	/* 代办任务最后更新时间		*/
	uint32_t last_update13;	/* 系统消息最后更新时间		*/
	uint32_t last_update14;	/* 商品类别信息最后更新时间	*/
	uint32_t last_update15;	/* 数据字典同步请求最后更新时间	*/
	uint32_t last_update16; /* 商品类别/证件编号更新时间	*/
}__attribute__((packed));

/* 差异化同步请求 RRQ, 0x12 */
struct s_12_rrq {
	uint8_t cmd;		/* 信令编号			*/
	uint8_t result1;	/* 全局参数同步请求结果		*/
	uint8_t result2;	/* 小票信息同步请求结果		*/
	uint8_t result3;	/* 商家信息同步请求结果		*/
	uint8_t result4;	/* 销售价格定制信息同步请求结果	*/
	uint8_t result5;	/* 仓库信息同步请求结果		*/
	uint8_t result6;	/* 员工信息同步请求结果		*/
	uint8_t result7;	/* 客户信息同步请求结果		*/
	uint8_t result8;	/* 供应商信息同步请求结果	*/
	uint8_t result9;	/* 商品基本信息同步请求结果	*/
	uint8_t result10;	/* 商品单位信息同步请求结果	*/
	uint8_t result11;	/* 商品批次信息同步请求结果	*/
	uint8_t result12;	/* 办任务同步请求结果		*/
	uint8_t result13;	/* 系统消息同步请求结果		*/
	uint8_t result14;	/* 商品类别信息同步请求结果	*/
	uint8_t result15;	/* 数据字典同步请求结果		*/
	uint8_t result16; 	/* 商品类别/证件编号更新结果	*/
}__attribute__((packed));

/* 待办任务信息 WRQ, 0x13 */
struct s_13_wrq {
	uint8_t cmd;		/* 信令编号			*/
	uint64_t dept_id;	/* 商家 ID			*/
	uint16_t del_count;	/* 删除记录数			*/
	uint16_t deal_count;	/* 处理记录数			*/
	char data[0];		/* 待办任务信息明细		*/
}__attribute__((packed));

/* 待办任务处理明细 */
struct s_13_wrq_dealdata {
	uint64_t id;		/* 任务 ID			*/
	uint8_t sort;		/* 任务类型			*/
	uint8_t pm_len;		/* 任务参数长度，最大 100	*/
	char *pm;		/* 任务参数			*/
	char num[22];		/* 进货单编号			*/
	uint64_t depot_id;	/* 入库仓库 ID，可能为空	*/
	uint8_t depot_name_len;	/* 入库仓库名称长度，最大 10	*/
	char *depot_name;	/* 入库仓库名称			*/
	uint64_t login_user_id;	/* 登录人 ID			*/
	uint8_t name_len;	/* 登录人姓名长度，最大 8	*/
	char *name;		/* 登录人姓名			*/
}__attribute__((packed));

/* 索取商家证请求 WRQ, 0x14 */
struct s_14_wrq {
	uint8_t cmd;		/* 信令编号			*/
	uint64_t dept_id;	/* 商家 ID			*/
	uint8_t num_len;	/* 被索证商家营业执照号码长度	*/
	char *num;		/* 被索证商家营业执照号码	*/
}__attribute__((packed));

/* 索取产品证请求 WRQ, 0x15 */
struct s_15_wrq {
	uint8_t cmd;		/* 信令编号			*/
	uint64_t dept_id;	/* 商家 ID			*/
	uint8_t code_len;	/* 被索证产品条码长度		*/
	char *code;		/* 被索证产品条码		*/
	uint64_t num;		/* 供应商 ID 			*/
}__attribute__((packed));

/* 进货单信息 WRQ, 0x16 */
struct s_16_wrq {
	uint8_t cmd;			/* 信令编号			*/
	uint64_t dept_id;		/* 商家 ID			*/
	char num[22];			/* 单据编号			*/
	uint8_t dept_name_len;		/* 商家名称长度，最大 60	*/
	char *dept_name;		/* 商家名称			*/
	uint64_t client_id;		/* 供应商 ID			*/
	uint8_t client_name_len;	/* 供应商名称长度，最大 60	*/
	char *client_name;		/* 供应商名称			*/
	uint64_t depot_id;		/* 入库仓库 ID			*/
					/* 数据库中可能为空，如果为空传	*/
					/* 递参数的值为零 		*/
	uint8_t depot_name_len;		/* 入库仓库名称长度，最大 10	*/
	char *depot_name;		/* 入库仓库名称			*/
	uint64_t deal_user_id;		/* 经手人 ID			*/
	uint8_t deal_user_name_len;	/* 经手人姓名长度，最大 8	*/
	char *deal_user_name;		/* 经手人姓名			*/
	uint32_t sum_count;		/* 数量合计			*/
	char pay_money[10];		/* 应付款金额			*/
	char fact_money[10];		/* 实际付款金额			*/
	uint64_t create_user_id;	/* 制单人 ID			*/
	uint8_t create_user_name_len;	/* 制单人姓名长度，最大 8	*/
	char *create_user_name;		/* 制单人姓名			*/
	char create_date[10];		/* 制单日期			*/
	char fav_money[10];		/* 优惠金额			*/
	char rebate_front_money[10];	/* 折前金额合计			*/
	char rebate_back_money[10];	/* 折后金额合计			*/
	uint8_t record_count;		/* 明细记录数			*/
	char data[0];			/* 进货单明细			*/
}__attribute__((packed));

/* 进货单明细 */
struct s_16_wrq_data {
	uint64_t commodity_id;		/* 商品 ID			*/
	uint8_t is_give;		/* 是否赠品			*/
	uint32_t count;			/* 数量				*/
	char price[10];			/* 单价				*/
	uint64_t unit_id;		/* 单位 ID			*/
	uint8_t quality_unit;		/* 保质单位			*/
	uint16_t quality_date;		/* 保质期			*/
	uint8_t create_date_len;	/* 生产日期长度，最大 10	*/
	char *create_date;		/* 生产日期			*/
	uint8_t exceed_date_len;	/* 过期日期长度，最大 10	*/
	char *exceed_date;		/* 过期日期			*/
	uint8_t batch_len;		/* 批次长度，最大 30		*/
	char *batch;			/* 批次				*/
	uint8_t rebate;			/* 折扣				*/
}__attribute__((packed));

/* 销售单信息 WRQ, 0x17 */
struct s_17_wrq {
	uint8_t cmd;			/* 信令编号			*/
	uint64_t dept_id;		/* 商家 ID			*/
	char num[22];			/* 单据编号			*/
	uint8_t dept_name_len;		/* 商家名称长度，最大 60	*/
	char *dept_name;		/* 商家名称			*/
	uint64_t client_id;		/* 客户 ID			*/
	uint8_t client_name_len;	/* 客户名称长度，最大 60	*/
	char *client_name;		/* 客户名称			*/
	uint64_t depot_id;		/* 发货仓库 ID			*/
	uint8_t depot_name_len;		/* 发货仓库名称长度，最大 10	*/
	char *depot_name;		/* 发货仓库名称			*/
	uint64_t deal_user_id;		/* 经手人 ID			*/
	uint8_t deal_user_name_len;	/* 经手人姓名长度，最大 8	*/
	char *deal_user_name;		/* 经手人姓名			*/
	uint32_t sum_count;		/* 数量合计			*/
	char pay_money[10];		/* 应收款金额			*/
	char fact_money[10];		/* 实际收款金额			*/
	uint64_t create_user_id;	/* 制单人 ID			*/
	uint8_t create_user_name_len;	/* 制单人姓名长度，最大 8	*/
	char *create_user_name;		/* 制单人姓名			*/
	char create_date[10];		/* 制单日期			*/
	char fav_money[10];		/* 优惠金额			*/
	char rebate_front_money[10];	/* 折前金额合计			*/
	char rebate_back_money[10];	/* 折后金额合计			*/
	uint8_t record_count;		/* 明细记录数			*/
	char data[0];			/* 销售单明细			*/
}__attribute__((packed));

/* 销售单明细 */
struct s_17_wrq_data {
	uint64_t commodity_id;		/* 商品 ID			*/
	uint8_t is_give;		/* 是否赠品			*/
	uint32_t count;			/* 数量				*/
	char price[10];			/* 单价				*/
	uint64_t unit_id;		/* 单位 ID			*/
	uint8_t quality_unit;		/* 保质单位			*/
	uint16_t quality_date;		/* 保质期			*/
	uint8_t create_date_len;	/* 生产日期长度，最大 10	*/
	char *create_date;		/* 生产日期			*/
	uint8_t exceed_date_len;	/* 过期日期长度，最大 10	*/
	char *exceed_date;		/* 过期日期			*/
	uint8_t batch_len;		/* 批次长度，最大 30		*/
	char *batch;			/* 批次				*/
	uint8_t rebate;			/* 折扣				*/
}__attribute__((packed));

/* 退市单信息 WRQ, 0x18 */
struct s_18_wrq {
	uint8_t cmd;			/* 信令编号			*/
	uint64_t dept_id;		/* 商家 ID			*/
	char num[22];			/* 单据编号			*/
	uint8_t dept_name_len;		/* 商家名称长度			*/
	char *dept_name;		/* 商家名称			*/
	uint64_t out_depot_id;		/* 出库仓库 ID			*/
	uint8_t out_depot_name_len;	/* 出库仓库名称长度		*/
	char *out_depot_name;		/* 出库仓库名称			*/
	uint64_t in_depot_id;		/* 入库仓库 ID			*/
	uint8_t in_depot_name_len;	/* 入库仓库名称长度		*/
	char *in_depot_name;		/* 入库仓库名称长		*/
	uint64_t deal_user_id;		/* 经手人 ID 			*/
	uint8_t deal_user_name_len;	/* 经手人姓名长度		*/
	char *deal_user_name;		/* 经手人姓名			*/
	uint32_t sum_count;		/* 退市数量合计			*/
	uint64_t create_user_id;	/* 制单人id			*/
	uint8_t create_user_name_len;	/* 制单人姓名长度		*/
	char *create_user_name;		/* 制单人姓名			*/
	char create_date[10];		/* 制单日期			*/
	uint8_t record_count;		/* 明细记录数			*/
	char data[0];			/* 退市单明细			*/
}__attribute__((packed));

/* 退市单明细 */
struct s_18_wrq_data {
	uint64_t commodity_id;		/* 商品 ID			*/
	uint32_t count;			/* 退市数量			*/
	uint8_t quality_unit;		/* 保质单位			*/
	uint16_t quality_date;		/* 保质期			*/
	uint8_t create_date_len;	/* 生产日期长度			*/
	char *create_date;		/* 生产日期			*/
	uint8_t exceed_date_len;	/* 过期日期长度			*/
	char *exceed_date;		/* 过期日期			*/
	uint8_t batch_len;		/* 批次长度			*/
	char *batch;			/* 批次				*/
}__attribute__((packed));

/* 退市处理单信息 WRQ, 0x19 */
struct s_19_wrq {
	uint8_t cmd;			/* 信令编号			*/
	uint64_t dept_id;		/* 商家 ID			*/
	char num[22];			/* 单据编号			*/
	uint8_t dept_name_len;		/* 商家名称长度			*/
	char *dept_name;		/* 商家名称			*/
	uint64_t depot_id;		/* 禁售仓库 ID			*/
	uint8_t depot_name_len;		/* 禁售仓库名称长度		*/
	char *depot_name;		/* 禁售仓库名称			*/
	uint64_t deal_user_id;		/* 经手人 ID 			*/
	uint8_t deal_user_name_len;	/* 经手人姓名长度		*/
	char *deal_user_name;		/* 经手人姓名			*/
	uint8_t deal_way;		/* 处理方式			*/
	uint32_t sum_count;		/* 处理数量合计			*/
	uint64_t create_user_id;	/* 制单人id			*/
	uint8_t create_user_name_len;	/* 制单人姓名长度		*/
	char *create_user_name;		/* 制单人姓名			*/
	char create_date[10];		/* 制单日期			*/
	uint8_t describe_len;		/* 处理方式描述长度，最大 40	*/
	char *describe;			/* 处理方式描述			*/
	uint8_t record_count;		/* 明细记录数			*/
}__attribute__((packed));

/* 退市处理单明细 */
struct s_19_wrq_data {
	uint64_t commodity_id;		/* 商品 ID			*/
	uint32_t deal_count;		/* 处理数量			*/
	uint8_t quality_unit;		/* 保质单位			*/
	uint16_t quality_date;		/* 保质期			*/
	uint8_t create_date_len;	/* 生产日期长度			*/
	char *create_date;		/* 生产日期			*/
	uint8_t exceed_date_len;	/* 过期日期长度			*/
	char *exceed_date;		/* 过期日期			*/
	uint8_t batch_len;		/* 批次长度			*/
	char *batch;			/* 批次				*/
}__attribute__((packed));

/* 商品管理请求 WRQ, 0x1a */
struct s_1a_wrq {
	uint8_t cmd;			/* 信令编号			*/
	uint64_t dept_id;		/* 商家 ID			*/
	uint16_t record_count;		/* 总记录数			*/
	char data[0];			/* 商品管理请求明细		*/
}__attribute__((packed));

/* 商品管理请求明细 */
struct s_1a_wrq_data {
	uint64_t id;			/* 商品 ID			*/
	uint64_t commondity_id;		/* 基础商品 ID			*/
	uint8_t code_len;		/* 商品条码长度，最大 13	*/
	char *code;			/* 商品条码			*/
	uint8_t name_len;		/* 商品名称长度，最大 30	*/
	char *name;			/* 商品名称			*/
	uint8_t base_unit_name_len;	/* 基本单位名称长度，最大 10	*/
	char *base_unit_name;		/* 基本单位名称			*/
	uint64_t category_id;		/* 商品类别 ID			*/
	uint8_t category_len;		/* 类别编码长度，最大 20	*/
	char *category_code;		/* 类别编码			*/
	uint8_t spec_len;		/* 规格长度，最大 10		*/
	char *spec;			/* 规格				*/
	char price[10];			/* 进货参考价			*/
	char retail_price[10];		/* 零售价			*/
	char vip_price[10];		/* 会员价			*/
	char dealer_price[10];		/* 经销商价			*/
	uint8_t cert_code_len;		/* 证件编号长度，最大 30	*/
	char *cert_code;		/* 证件编号			*/
	uint8_t oper_type;		/* 操作类型			*/
}__attribute__((packed));

/* 商品单位信息管理请求 WRQ, 0x1b */
struct s_1b_wrq {
	uint8_t cmd;			/* 信令编号			*/
	uint64_t dept_id;		/* 商家 ID			*/
	uint16_t record_count;		/* 总记录数			*/
	char data[0];			/* 商品单位管理请求明细		*/
}__attribute__((packed));

/* 商品单位管理请求明细 */
struct s_1b_wrq_data {
	uint64_t id;			/* 商品 ID			*/
	uint8_t code_len;		/* 条码长度，最大 13		*/
	char *code;			/* 条码				*/
	uint8_t name_len;		/* 单位名称长度，最大 10	*/
	char *name;			/* 单位名称			*/
	uint16_t uint_relation;		/* 单位关系			*/
	char price[10];			/* 进货参考价			*/
	char retail_price[10];		/* 零售价			*/
	char vip_price[10];		/* 会员价			*/
	char dealer_price[10];		/* 经销商价			*/
	uint8_t oper_type;		/* 操作类型			*/
}__attribute__((packed));

/* 商品档案信息请求 WRQ, 0x1c */
struct s_1c_wrq {
	uint8_t cmd;			/* 信令编号			*/
	uint64_t dept_id;		/* 商家 ID			*/
	uint8_t code_len;		/* 商品条码长度，最大 13	*/
	char *code;			/* 商品条码			*/
}__attribute__((packed));

/* 商品档案信息请求 RRQ, 0x1c */
struct s_1c_rrq {
	uint8_t cmd;			/* 信令编号			*/
	uint8_t record_count;		/* 总记录数			*/
	uint8_t code_len;		/* 商品条码长度，最大 13	*/
	char *code;			/* 商品条码			*/
	uint8_t name_len;		/* 商品名称长度，最大 30	*/
	char *name;			/* 商品名称			*/
	uint8_t base_unit_name_len;	/* 基本单位名称长度，最大 10	*/
	char *base_unit_name;		/* 基本单位名称			*/
	uint64_t category_id;		/* 商品类别 ID			*/
	uint8_t spec_len;		/* 规格长度，最大 10		*/
	char *spec;			/* 规格				*/
	char *cert_code;		/* 证件编号			*/
	uint8_t oper_type;		/* 操作类型			*/
}__attribute__((packed));

/* 获取销售单商品信息请求: WRQ, 0x1d */
struct s_1d_wrq {
	uint8_t cmd;		/* 信令编号		*/
	uint64_t dept_id;	/* 商家 ID		*/
	char sale_uuid[32];	/* 销售 UUID		*/
}__attribute__((packed));

/* 获取销售单商品信息请求: RRQ, 0x1d */
struct s_1d_rrq {
	uint8_t cmd;		/* 信令编号		*/
	uint16_t record_count;	/* 记录数量		*/
	char data[0];		/* 系统消息明细		*/
}__attribute__((packed));

/* 获取销售单商品信息请求明细 */
struct s_1d_rrq_data {
	uint8_t name_len;	/* 商品名称长度，最大 30	*/
	char *name;		/* 商品名称			*/
	uint8_t unit_len;	/* 单位名称长度，最大 10	*/
	char *unit;		/* 单位名称			*/
	uint32_t count;		/* 数量				*/
}__attribute__((packed));

/* 获取升级包信息请求: WRQ, 0x1e */
struct s_1e_wrq {
	uint8_t cmd;		/* 信令编号		*/
	char hard_version[6];	/* 硬件版本号		*/
	char soft_version[6];	/* 软件版本号		*/
}__attribute__((packed));

/* 获取升级包信息请求: RRQ, 0x1e */
struct s_1e_rrq {
	uint8_t cmd;		/* 信令编号		*/
	char last_version[6];	/* 最新版本号		*/
	uint32_t filesize;	/* 升级包大小		*/
	uint8_t need_update;	/* 是否需要升级		*/
}__attribute__((packed));

/* 软件升级请求: WRQ, 0x1f */
struct s_1f_wrq {
	uint8_t cmd;		/* 信令编号		*/
	uint64_t dept_id;	/* 商家 ID		*/
	char hard_version[6];	/* 当前硬件版本号	*/
	char cursoft_version[6];/* 当前软件版本号	*/
	char reqsoft_version[6];/* 请求软件版本号	*/
}__attribute__((packed));

/* 软件升级请求: RRQ, 0x1f */
struct s_1f_rrq {
	uint8_t cmd;		/* 信令编号		*/
	uint32_t filesize;	/* 升级包大小		*/
	char filecontent[0];	/* 升级包内容		*/
}__attribute__((packed));

/* 员工管理请求 WRQ, 0x20 */
struct s_20_wrq {
	uint8_t cmd;			/* 信令编号			*/
	uint64_t dept_id;		/* 商家 ID			*/
	uint16_t record_count;		/* 总记录数			*/
	char data[0];			/* 员工管理请求明细		*/
}__attribute__((packed));

/* 员工管理请求明细 WRQ */
struct s_20_wrq_data {
	uint64_t id;			/* 员工 ID，若全 0 则服务器自增	*/
	char num[4];			/* 员工编号			*/
	char password[6];		/* 员工密码			*/
	uint8_t name_len;		/* 员工姓名长度，最大 8		*/
	char *name;			/* 员工姓名			*/
	uint8_t oper_type;		/* 操作类型			*/
}__attribute__((packed));

/* 供应商管理请求 WRQ, 0x21 */
struct s_21_wrq {
	uint8_t cmd;			/* 信令编号			*/
	uint64_t dept_id;		/* 商家 ID			*/
	uint16_t record_count;		/* 总记录数			*/
	char data[0];			/* 供应商管理请求明细		*/
}__attribute__((packed));

/* 供应商管理请求明细 WRQ */
struct s_21_wrq_data {
	uint64_t id;			/* 供应商 ID，若全 0 则自增	*/
	uint8_t num_len;		/* 营业执照号码长度，最大 15	*/
	char *num;			/* 营业执照号码			*/
	uint8_t name_len;		/* 供应商名称长度，最大 60	*/
	char *name;			/* 供应商名称			*/
	uint8_t oper_type;		/* 操作类型			*/
}__attribute__((packed));

/* 客户管理请求 WRQ, 0x22 */
struct s_22_wrq {
	uint8_t cmd;			/* 信令编号			*/
	uint64_t dept_id;		/* 商家 ID			*/
	uint16_t record_count;		/* 总记录数			*/
	char data[0];			/* 客户管理请求明细		*/
}__attribute__((packed));

/* 客户管理请求明细 WRQ */
struct s_22_wrq_data {
	uint64_t id;			/* 客户 ID，若全 0 则自增	*/
	uint8_t num_len;		/* 营业执照号码长度，最大 15	*/
	char *num;			/* 营业执照号码			*/
	uint8_t name_len;		/* 客户名称长度，最大 60	*/
	char *name;			/* 客户名称			*/
	uint8_t oper_type;		/* 操作类型			*/
}__attribute__((packed));

#endif /* __SYJ_STRUCT_H */
